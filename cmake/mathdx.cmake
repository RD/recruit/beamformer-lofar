set(MATHDX_VERSION "25.01.0")
set(MATHDX_COMPONENTS "cufftdx")

if(NOT DEFINED ${CMAKE_CUDA_ARCHITECTURES})
  set(CMAKE_CUDA_ARCHITECTURES 70)
endif()

FetchContent_Declare(
  mathdx
  GIT_REPOSITORY https://git.astron.nl/RD/mathdx
  GIT_TAG "main"
)
FetchContent_MakeAvailable(mathdx)
