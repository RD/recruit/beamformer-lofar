#ifndef LOFAR_GPUPROC_SPAN_H
#define LOFAR_GPUPROC_SPAN_H

#include <cstdlib>

#include <xtensor/xadapt.hpp>

namespace LOFAR::Cobalt {

template <typename InputType, size_t Dimensions>
using Span = decltype(::xt::adapt(std::add_pointer_t<InputType>{},
                                  std::array<size_t, Dimensions>{}));

template <typename InputType, size_t Dimensions>
[[nodiscard]] Span<InputType, Dimensions>
CreateSpan(InputType *pointer, const std::array<size_t, Dimensions> &shape) {
  // std::forward ensures that xt::adapt receives a pointer and not a
  // reference to a pointer.
  return ::xt::adapt(std::forward<InputType *>(pointer), shape);
}

} // namespace LOFAR::Cobalt

#endif