#!/usr/bin/env python3
from common import run
from BeamformerTuner import BeamformerTuner

if __name__ == "__main__":
    tuner = BeamformerTuner()
    run(tuner)
