from collections import OrderedDict

import numpy as np


class BeamformerTuner:
    def __init__(self):
        self._set_tune_params()
        self._set_constants()
        self._set_block_sizes()
        self._set_arguments()

    def get_kernel_name(self):
        return "beamFormer"

    def _set_tune_params(self):
        self.tune_params = OrderedDict()
        self.tune_params["NR_STATIONS"] = list(range(16,129))
        self.tune_params["NR_INPUT_BITS"] = [16, 32]
        self.tune_params["NR_TABS"] = [64, 128, 256, 512]

    def get_tune_params(self):
        return self.tune_params

    def _set_constants(self):
        self.constants = OrderedDict()
        self.constants["NR_CHANNELS"] = 256
        self.constants["NR_SAMPLES_PER_CHANNEL"] = 768
        self.constants["NR_POLARIZATIONS"] = 2
        self.constants["NR_TIME_PER_PASS"] = 32
        self.constants["NR_STATIONS_PER_PASS"] = 32
        # weight computation is disabled by setting NR_DELAYS=0:
        self.constants["NR_DELAYS"] = 0

    def get_constants(self):
        return self.constants

    def _set_block_sizes(self):
        self.tune_params["block_size_x"] = [self.constants["NR_POLARIZATIONS"]]
        self.tune_params["block_size_y"] = [32]

    def _set_arguments(self):
        nr_channels = self.constants["NR_CHANNELS"]
        nr_samples_per_channel = self.constants["NR_SAMPLES_PER_CHANNEL"]
        nr_polarizations = self.constants["NR_POLARIZATIONS"]
        max_nr_stations = max(self.tune_params["NR_STATIONS"])
        max_nr_tabs = max(self.tune_params["NR_TABS"])
        input_size = (
            nr_channels,
            nr_samples_per_channel,
            nr_polarizations,
            max_nr_stations,
        )
        output_size = (
            nr_channels,
            nr_samples_per_channel,
            nr_polarizations,
            max_nr_tabs,
        )
        weights_size = (max_nr_tabs, nr_channels, max_nr_stations)
        input = np.zeros(np.prod(input_size)).astype(np.complex64)
        output = np.zeros(np.prod(output_size)).astype(np.complex64)
        weights = np.zeros(np.prod(weights_size)).astype(np.complex64)
        station_indices = np.arange(max_nr_stations, dtype=np.int32)
        delay_indices = np.arange(max_nr_stations, dtype=np.int32)
        subband_frequency = np.int32(1.5e8)
        self.arguments = [
            output,
            input,
            station_indices,
            weights,
            delay_indices,
            subband_frequency,
        ]

    def get_arguments(self):
        return self.arguments

    def compute_flops(self, p):
        return (
            self.constants["NR_CHANNELS"]
            * p["NR_TABS"]
            * self.constants["NR_POLARIZATIONS"]
            * p["NR_STATIONS"]
            * self.constants["NR_SAMPLES_PER_CHANNEL"]
            * 8
        )

    def get_problem_size(self):
        return (self.constants["NR_CHANNELS"], "NR_TABS")

    def get_compiler_options(self):
        return [f"-D{k}={v}" for k, v in self.constants.items()]

    def get_restrictions(self, p):
        return True

    def get_grid_div(self):
        return [None, None, None]
