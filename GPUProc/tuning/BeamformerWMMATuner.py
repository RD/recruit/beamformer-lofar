from collections import OrderedDict

import numpy as np


class BeamformerWMMATuner:
    def __init__(self):
        self._set_tune_params()
        self._set_constants()
        self._set_block_sizes()
        self._set_arguments()

    def get_kernel_name(self):
        return "wmma_gemm"

    def _set_tune_params(self):
        self.tune_params = OrderedDict()
        self.tune_params["NR_STATIONS"] = list(range(16,129))
        self.tune_params["NR_INPUT_BITS"] = [16, 32]
        self.tune_params["NR_BEAMS"] = [64, 128, 256, 512]
        self.tune_params["block_size_x"] = [32]  # warpsize, fixed
        self.tune_params["block_size_y"] = [2]  # number of warps in x
        self.tune_params["block_size_z"] = [4]  # number of warps in y
        self.tune_params["WMMA_K"] = [8, 16]

    def get_tune_params(self):
        return self.tune_params

    def _set_constants(self):
        self.constants = OrderedDict()
        self.constants["NR_CHANNELS"] = 256
        self.constants["NR_SAMPLES"] = 768
        self.constants["NR_POLARIZATIONS"] = 2
        self.constants["WARP_SIZE"] = 32
        self.constants["WMMA_M"] = 16
        self.constants["WMMA_N"] = 16
        self.constants["BLOCK_ROW_WARPS"] = 2
        self.constants["BLOCK_COL_WARPS"] = 4
        self.constants["WARP_ROW_TILES"] = 2
        self.constants["WARP_COL_TILES"] = 1

    def get_constants(self):
        return self.constants

    def _set_block_sizes(self):
        self.tune_params["block_size_x"] = [self.constants["WARP_SIZE"]]
        self.tune_params["block_size_y"] = [self.constants["BLOCK_ROW_WARPS"]]
        self.tune_params["block_size_z"] = [self.constants["BLOCK_COL_WARPS"]]

    def _set_arguments(self):
        nr_channels = self.constants["NR_CHANNELS"]
        nr_samples = self.constants["NR_SAMPLES"]
        nr_polarizations = self.constants["NR_POLARIZATIONS"]
        max_nr_stations = max(self.tune_params["NR_STATIONS"])
        max_nr_beams = max(self.tune_params["NR_BEAMS"])
        input_size = (
            nr_channels,
            nr_samples,
            nr_polarizations,
            max_nr_stations,
        )
        output_size = (
            nr_channels,
            nr_samples,
            nr_polarizations,
            max_nr_beams,
        )
        weights_size = (max_nr_beams, nr_channels, max_nr_stations)
        input = np.zeros(np.prod(input_size)).astype(np.complex64)
        output = np.zeros(np.prod(output_size)).astype(np.complex64)
        weights = np.zeros(np.prod(weights_size)).astype(np.complex64)
        self.arguments = [weights, input, output]

    def get_arguments(self):
        return self.arguments

    def compute_flops(self, p):
        return (
            self.constants["NR_CHANNELS"]
            * p["NR_BEAMS"]
            * self.constants["NR_POLARIZATIONS"]
            * p["NR_STATIONS"]
            * self.constants["NR_SAMPLES"]
            * 8
        )

    def get_problem_size(self):
        return (
            "NR_BEAMS",
            self.constants["NR_SAMPLES"],
            self.constants["NR_CHANNELS"] * self.constants["NR_POLARIZATIONS"],
        )

    def get_compiler_options(self):
        return [f"-D{k}={v}" for k, v in self.constants.items()]

    def get_restrictions(self, p):
        if p["NR_INPUT_BITS"] == 16:
            return p["WMMA_K"] == 16
        else:
            return p["WMMA_K"] == 8

    def get_grid_div(self):
        beams_per_block = (
            self.constants["WMMA_M"]
            * self.constants["BLOCK_ROW_WARPS"]
            * self.constants["WARP_ROW_TILES"]
        )
        samples_per_block = (
            self.constants["WMMA_N"]
            * self.constants["BLOCK_COL_WARPS"]
            * self.constants["WARP_COL_TILES"]
        )
        channels_polarizations_per_block = 1
        grid_div_x = str(beams_per_block)
        grid_div_y = str(samples_per_block)
        grid_div_z = str(channels_polarizations_per_block)
        return ([grid_div_x], [grid_div_y], [grid_div_z])
