import argparse
import json
import os
import re

from collections import OrderedDict

import kernel_tuner
import pycuda.driver as drv

drv.init()


def get_device_name(device):
    return drv.Device(device).name().replace(" ", "_")


def get_default_parser():
    parser = argparse.ArgumentParser(description="Tune kernel")
    parser.add_argument("-d", dest="device", nargs="?", default=0, help="GPU ID to use")
    parser.add_argument(
        "-f",
        dest="overwrite",
        action="store_true",
        help="Overwrite any existing .json files",
    )
    parser.add_argument("--suffix", help="Suffix to append to output file names")
    parser.add_argument("--file", required=True, help="Path to kernel source file")
    return parser


def setup_output_files(filename, overwrite=False):
    filename_cache = f"{filename}_cache.json"
    filename_output = f"{filename}_output.json"
    filename_env = f"{filename}_env.json"

    if overwrite:
        for filename in [filename_cache, filename_output, filename_env]:
            if os.path.exists(filename):
                os.remove(filename)

    return (filename_cache, filename_output, filename_env)


def write_json(filename, data):
    with open(filename, "w") as fh:
        json.dump(data, fh)


def get_kernel_string(source_path, processed_files=None):
    source_dir = os.path.abspath(os.path.dirname(source_path))
    source_file = os.path.basename(source_path)

    if processed_files is None:
        processed_files = set()

    source_path = os.path.join(source_dir, source_file)

    with open(source_path, "r") as src_file:
        source_content = src_file.read()

        # Find all included header files
        header_matches = re.findall(r'#include "(.*?)"', source_content)

        for header in header_matches:
            header_path = os.path.join(source_dir, header)

            # Check if the header file exists and hasn't been processed before
            if os.path.exists(header_path) and header_path not in processed_files:
                processed_files.add(header_path)

                # Include the header's content recursively
                header_content = get_kernel_string(
                    f"{source_dir}/{header}", processed_files
                )

                # Replace the include statement with the header's content
                source_content = source_content.replace(
                    f'#include "{header}"', header_content
                )

    return source_content


def get_nvml_observer():
    return kernel_tuner.observers.nvml.NVMLObserver(
        [
            "nvml_power",
            "nvml_energy",
            "core_freq",
            "mem_freq",
            "temperature",
        ]
    )


def report_most_efficient(results, tune_params, metrics):
    energy_metric = "J"
    results = list(filter(lambda x: energy_metric in x, results))
    if energy_metric in metrics:
        best_config = min(results, key=lambda x: x[energy_metric])
        print("most efficient configuration:")
        kernel_tuner.util.print_config_output(
            tune_params, best_config, quiet=False, metrics=metrics, units=None
        )


def run(tuner):
    parser = get_default_parser()
    args = parser.parse_args()

    tune_params = tuner.get_tune_params()
    compiler_options = tuner.get_compiler_options()
    arguments = tuner.get_arguments()
    problem_size = tuner.get_problem_size()
    restrictions = tuner.get_restrictions
    grid_div = tuner.get_grid_div()

    # the device to use
    device = args.device
    device_name = get_device_name(device)

    # the kernel to tune
    kernel_string = get_kernel_string(args.file)
    kernel_name = tuner.get_kernel_name()
    kernel_source = f"temp_{kernel_name}.cu"
    with open(kernel_source, "w") as f:
        f.write(kernel_string)

    # name for the output cache file
    filename = f"{kernel_name}_{device_name}"
    if args.suffix:
        filename = f"{filename}_{args.suffix}"

    # setup metrics
    metrics = OrderedDict()
    metrics["GFLOP/s"] = lambda p: (tuner.compute_flops(p) / 1.0e9) / (
        p["time"] / 1.0e3
    )
    metrics["J"] = lambda p: p["nvml_energy"]
    metrics["W"] = lambda p: p["nvml_power"]
    nvmlobserver = get_nvml_observer()

    # setup output files
    (filename_cache, filename_output, filename_env) = setup_output_files(
        filename, args.overwrite
    )

    # start tuning
    results, env = kernel_tuner.tune_kernel(
        kernel_name,
        kernel_string,
        problem_size=problem_size,
        arguments=arguments,
        tune_params=tune_params,
        verbose=True,
        metrics=metrics,
        iterations=1,
        cache=filename_cache,
        observers=[nvmlobserver],
        compiler_options=compiler_options,
        restrictions=restrictions,
        grid_div_x=grid_div[0],
        grid_div_y=grid_div[1],
        grid_div_z=grid_div[2],
    )

    report_most_efficient(results, tune_params, metrics)

    write_json(filename_output, results)
    write_json(filename_env, env)
