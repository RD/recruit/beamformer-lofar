#!/usr/bin/env python3
from common import run
from BeamformerWMMATuner import BeamformerWMMATuner

if __name__ == "__main__":
    tuner = BeamformerWMMATuner()
    run(tuner)
