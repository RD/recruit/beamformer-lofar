#ifndef LOFAR_GPUPROC_CUDA_KERNELFACTORY_H
#define LOFAR_GPUPROC_CUDA_KERNELFACTORY_H

#include <GPUProc/Kernels/Kernel.h>
#include <GPUProc/gpu_utils.h>
#include <string>

namespace LOFAR::Cobalt {
// Abstract base class of the templated KernelFactory class.
class KernelFactoryBase {
public:
  // Pure virtual destructor, because this is an abstract base class.
  virtual ~KernelFactoryBase() = 0;

protected:
  // Return compile definitions to use when creating PTX code for any
  // Kernel.
  CompileDefinitions compileDefinitions(const Kernel::Parameters &param) const;

  // Return compile flags to use when creating PTX code for any Kernel.
  CompileFlags compileFlags(const Kernel::Parameters &param) const;
};

// Declaration of a generic factory class. For each concrete Kernel class
// (e.g. FIR_FilterKernel), a specialization must exist of the constructor
// and of the bufferSize() method.
template <typename T> class KernelFactory : public KernelFactoryBase {
public:
  typedef typename T::BufferType BufferType;

  // Construct a factory for creating Kernel objects of type \c T, using the
  // settings provided by \a params.
  KernelFactory(cu::Device &device, const typename T::Parameters &params)
      : itsDevice(device), itsParameters(params), itsPTX(_createPTX(device)) {}

  // Create a new Kernel object of type \c T.
  T *create(const cu::Context &context, cu::Stream &stream) const {
    return new T(itsDevice, stream,
                 createModule(context, T::theirSourceFile, itsPTX),
                 itsParameters);
  }

  // Return required buffer size for \a bufferType
  size_t bufferSize(BufferType bufferType) const {
    return itsParameters.bufferSize(bufferType);
  }

private:
  // Used by the constructors to construct the PTX from the other
  // members.
  std::string _createPTX(cu::Device &device) const {
    return createPTX(device, T::theirSourceFile, compileDefinitions(),
                     compileFlags());
  }

  // Return compile definitions to use when creating PTX code for kernels of
  // type \c T, using the parameters stored in \c itsParameters.
  CompileDefinitions compileDefinitions() const {
    return KernelFactoryBase::compileDefinitions(itsParameters);
  }

  // Return compile flags to use when creating PTX code for kernels of type
  // \c T.
  CompileFlags compileFlags() const {
    return KernelFactoryBase::compileFlags(itsParameters);
  }

  // The device to compiler kernels for
  cu::Device &itsDevice;

  // Additional parameters needed to create a Kernel object of type \c T.
  typename T::Parameters itsParameters;

  // PTX code, generated for kernels of type \c T, using information in the
  // Parset that was passed to the constructor.
  std::string itsPTX;
};

} // namespace LOFAR::Cobalt

#endif
