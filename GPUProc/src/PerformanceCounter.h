#ifndef LOFAR_GPUPROC_CUDA_PERFORMANCECOUNTER_H
#define LOFAR_GPUPROC_CUDA_PERFORMANCECOUNTER_H

#include <deque>

#include <CoInterface/RunningStatistics.h>
#include <cudawrappers/cu.hpp>

namespace LOFAR::Cobalt {
class PerformanceCounter {
public:
  PerformanceCounter(const std::string &name);
  ~PerformanceCounter();

  void recordStart(cu::Stream &stream);
  void recordStop(cu::Stream &stream);

  // Warning: user must make sure that the counter is not running!
  RunningStatistics getStats() {
    logTime();
    return stats;
  }

  static void disablePrint() { print = false; }

private:
  const std::string name;

  // Pairs of events to be inserted in a stream
  std::deque<std::pair<cu::Event, cu::Event>> events;

  // Whether to print the statistics upon destruction
  static bool print;

  RunningStatistics stats;

  void logTime();
};
} // namespace LOFAR::Cobalt

#endif
