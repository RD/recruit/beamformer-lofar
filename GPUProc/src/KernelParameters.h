#ifndef LOFAR_GPUPROC_KERNEL_PARAMETERS
#define LOFAR_GPUPROC_KERNEL_PARAMETERS

#include <vector>

#include <CoInterface/Parset.h>

namespace LOFAR::Cobalt {
struct KernelParameters {
public:
  // Components
  bool correlatorEnabled;
  bool preprocessingEnabled;
  bool beamFormerCoherentStokesEnabled;
  bool beamFormerIncoherentStokesEnabled;

  // Observation settings
  struct Observation {
    // Constructor
    Observation(const ObservationSettings &obsSettings);

    // Settings
    unsigned observationID;
    unsigned nrSAPs;
    unsigned nrStations;
    unsigned nrBitsPerSample;
    unsigned blockSize;
    double subbandWidth;
    std::vector<ObservationSettings::Subband> subbands;
    unsigned clockMHz;
  };

  Observation observation;

  // Correlator settings
  using Correlator = ObservationSettings::Correlator;
  Correlator correlator;

  // Preprocessing settings
  struct Preprocessor {
    // Constructor
    Preprocessor(const std::vector<ObservationSettings::AntennaFieldName>
                     &obsAntennaFieldNames,
                 const std::vector<ObservationSettings::AntennaFieldName>
                     &bfAntennaFieldNames,
                 bool delayCompensationEnabled, bool bandPassCorrectionEnabled,
                 unsigned nrDelayCompensationSettings, bool inputPPF);

    // Settings
    std::vector<unsigned> obsStationIndices;
    bool delayCompensationEnabled;
    bool bandPassCorrectionEnabled;
    unsigned nrDelayCompensationChannels;
    bool inputPPF;
  };

  Preprocessor preprocessing;

  // BeamFormer settings
  struct BeamFormer {
    // Constructor
    BeamFormer(const std::vector<ObservationSettings::AntennaFieldName>
                   &obsAntennaFieldNames,
               const std::vector<ObservationSettings::AntennaFieldName>
                   &preAntennaFieldNames,
               const ObservationSettings::BeamFormer::Pipeline &bfSettings);

    // The observation has a set of stations specified, ordered by name and
    // numbered from 0 to ps.settings.obsAntennaFieldNames.size() For the
    // beamFormer, there are two sets of indices for the (subset of) stations
    // being processed: obsStationIndices: the indices of the stations in the
    // global list of stations
    //                    use these indices when indexing the tabDelays
    // preStationIndices: the indices of the stations in the list of stations in
    // the preprocessor
    //                    use these indices when indexing the preprocssed input
    //                    data
    // Note that stationIndices must be a subset of delayIndices
    std::vector<unsigned> preStationIndices;
    std::vector<unsigned> obsStationIndices;

    // Settings
    bool doFlysEye;
    unsigned int nrSAPs;
    unsigned int maxNrCoherentTABsPerSAP;
    unsigned int maxNrIncoherentTABsPerSAP;
    ObservationSettings::BeamFormer::StokesSettings coherentSettings;
    ObservationSettings::BeamFormer::StokesSettings incoherentSettings;
  };

  BeamFormer beamFormer;

  // Cobalt settings
  using Cobalt = ObservationSettings::Cobalt;
  Cobalt cobalt;
};
} // end namespace Cobalt
} // end namespace LOFAR

#endif
