#include "FIR_FilterKernel.h"

#include <CoInterface/BlockID.h>
#include <GPUProc/FilterBank.h>
#include <GPUProc/gpu_utils.h>

#include <complex>

#include <mathdx-helper.hpp>

namespace LOFAR::Cobalt {
std::string FIR_FilterKernel::theirSourceFile = "FIR_Filter.cu";
std::string FIR_FilterKernel::theirFunction = "FIR_filter";

FIR_FilterKernel::Parameters::Parameters(
    unsigned nrTABs_, unsigned nrBitsPerSample_, unsigned nrChannels_,
    unsigned nrSamplesPerChannel_, float scaleFactor_, bool enableFFT_,
    const bool dumpBuffers_, std::string dumpFilePrefix_)
    : Kernel::Parameters(theirFunction), nrTABs(nrTABs_),
      nrBitsPerSample(nrBitsPerSample_), nrChannels(nrChannels_),
      nrSamplesPerChannel(nrSamplesPerChannel_), scaleFactor(scaleFactor_),
      enableFFT(enableFFT_) {
  dumpBuffers = dumpBuffers_;
  dumpFilePrefix = dumpFilePrefix_;
}

const unsigned FIR_FilterKernel::Parameters::nrTaps;

unsigned FIR_FilterKernel::Parameters::nrSamplesPerSubband() const {
  return nrChannels * nrSamplesPerChannel;
}

unsigned FIR_FilterKernel::Parameters::nrBytesPerComplexSample() const {
  return 2 * nrBitsPerSample / 8;
}

unsigned FIR_FilterKernel::Parameters::nrHistorySamples() const {
  return (nrTaps - 1) * nrChannels;
}

size_t FIR_FilterKernel::Parameters::bufferSize(BufferType bufferType) const {
  switch (bufferType) {
  case FIR_FilterKernel::INPUT_DATA:
    return (size_t)nrSamplesPerSubband() * nrTABs * kNrPolarizations *
           nrBytesPerComplexSample();
  case FIR_FilterKernel::OUTPUT_DATA:
    return (size_t)nrSamplesPerSubband() * nrTABs * kNrPolarizations *
           nrBytesPerComplexSample();
  case FIR_FilterKernel::FILTER_WEIGHTS:
    return (size_t)nrChannels * nrTaps * nrBitsPerSample / 8;
  case FIR_FilterKernel::HISTORY_DATA:
    // History is split over 2 bytes in 4-bit mode, to avoid unnecessary
    // packing/unpacking
    return (size_t)nrHistorySamples() * nrTABs * kNrPolarizations *
           (nrBitsPerSample == 4 ? 2U : nrBytesPerComplexSample());
  default:
    std::stringstream message;
    message << "Invalid bufferType (" << bufferType << ")";
    throw std::runtime_error(message.str());
  }
}

FIR_FilterKernel::FIR_FilterKernel(cu::Device &device, cu::Stream &stream,
                                   const cu::Module &module,
                                   const Parameters &params)
    : CompiledKernel(device, stream,
                     cu::Function(module, theirFunction.c_str()), params),
      h_filterWeights(params.bufferSize(FILTER_WEIGHTS)),
      d_filterWeights(params.bufferSize(FILTER_WEIGHTS)),
      d_historySamples(params.bufferSize(HISTORY_DATA)) {
  setEnqueueWorkSizes(device, Grid(kNrPolarizations, params.nrTABs),
                      Block(128));

  unsigned nrSamples = params.nrTABs * params.nrChannels * kNrPolarizations;

  nrOperations =
      (size_t)nrSamples * params.nrSamplesPerChannel * params.nrTaps * 2 * 2;

  nrBytesRead = (size_t)nrSamples *
                (params.nrTaps - 1 + params.nrSamplesPerChannel) *
                params.nrBytesPerComplexSample();

  nrBytesWritten = (size_t)nrSamples * params.nrSamplesPerChannel *
                   params.nrBytesPerComplexSample();

  if (params.scaleFactor) {
    FilterBank filterBank(true, params.nrTaps, params.nrChannels, KAISER);
    filterBank.negateWeights();
    filterBank.scaleWeights(params.scaleFactor);

    setWeights(filterBank.getWeights().data());
  }

  // set all history samples to 0, to prevent adding uninitialised data
  // to the stream
  d_historySamples.zero(d_historySamples.size());
}

void FIR_FilterKernel::setWeights(const void *weights) {
  std::memcpy(h_filterWeights, weights, h_filterWeights.size());
  itsStream.memcpyHtoDAsync(d_filterWeights, h_filterWeights,
                            h_filterWeights.size());
}

void FIR_FilterKernel::enqueue(const BlockID &blockId,
                               const cu::DeviceMemory &input,
                               cu::DeviceMemory &output) {
  setArg(0, output);
  setArg(1, input);
  setArg(2, d_filterWeights);
  setArg(3, d_historySamples);
  Kernel::enqueue(blockId, input, output);
}

//--------  Template specializations for KernelFactory  --------//

template <>
CompileDefinitions KernelFactory<FIR_FilterKernel>::compileDefinitions() const {
  CompileDefinitions defs =
      KernelFactoryBase::compileDefinitions(itsParameters);

  defs["COMPLEX"] = std::to_string(FIR_FilterKernel::kComplex);
  defs["NR_POLARIZATIONS"] = std::to_string(FIR_FilterKernel::kNrPolarizations);

  defs["NR_TABS"] = std::to_string(itsParameters.nrTABs);
  defs["NR_BITS_PER_SAMPLE"] = std::to_string(itsParameters.nrBitsPerSample);

  defs["NR_CHANNELS"] = std::to_string(itsParameters.nrChannels);
  defs["NR_SAMPLES_PER_CHANNEL"] =
      std::to_string(itsParameters.nrSamplesPerChannel);

  defs["NR_TAPS"] = std::to_string(itsParameters.nrTaps);

  if (itsParameters.enableFFT) {
    defs["ENABLE_FFT"] = std::to_string(1);
  }

  return defs;
}

template <> CompileFlags KernelFactory<FIR_FilterKernel>::compileFlags() const {
  CompileFlags flags = KernelFactoryBase::compileFlags(itsParameters);

  if (itsParameters.enableFFT) {
    const std::vector<std::string> include_dirs = mathdx::get_include_dirs();
    std::copy(include_dirs.begin(), include_dirs.end(),
              std::inserter(flags, flags.end()));
    flags.insert("--device-as-default-execution-space");
  }

  return flags;
}

} // namespace LOFAR::Cobalt
