#include "FlysEyeKernel.h"

#include <CoInterface/BlockID.h>
#include <GPUProc/gpu_utils.h>

#include <algorithm>
#include <complex>
#include <cstring>
#include <fstream>

namespace LOFAR::Cobalt {
std::string FlysEyeKernel::theirSourceFile = "FlysEye.cu";
std::string FlysEyeKernel::theirFunction = "flysEye";

FlysEyeKernel::Parameters::Parameters(std::vector<unsigned> stationIndices_,
                                      unsigned nrChannels_,
                                      unsigned nrSamplesPerChannel_,
                                      unsigned nrTABs_, unsigned nrInputBits_,
                                      bool dumpBuffers_,
                                      std::string dumpFilePrefix_)
    : Kernel::Parameters("flysEye"),

      // mapping to translate station numbers in input array (which
      // are the station selected in the beamformer preprocessor)
      stationIndices(stationIndices_),

      nrChannels(nrChannels_), nrSamplesPerChannel(nrSamplesPerChannel_),

      nrTABs(nrTABs_), nrInputBits(nrInputBits_) {
  dumpBuffers = dumpBuffers_;
  dumpFilePrefix = dumpFilePrefix_;
}

size_t FlysEyeKernel::Parameters::bufferSize(BufferType bufferType) const {
  switch (bufferType) {
  case FlysEyeKernel::INPUT_DATA:
    return (size_t)nrChannels * nrSamplesPerChannel * kNrPolarizations *
           nrInputStations() * 2 * (nrInputBits / 8);
  case FlysEyeKernel::OUTPUT_DATA:
    return (size_t)nrChannels * nrSamplesPerChannel * kNrPolarizations *
           nrTABs * sizeof(std::complex<float>);
  case FlysEyeKernel::STATION_INDICES:
    return (size_t)stationIndices.size() * sizeof stationIndices[0];
  default:
    std::stringstream message;
    message << "Invalid bufferType (" << bufferType << ")";
    throw std::runtime_error(message.str());
  }
}

FlysEyeKernel::FlysEyeKernel(cu::Device &device, cu::Stream &stream,
                             const cu::Module &module, const Parameters &params)
    : CompiledKernel(device, stream,
                     cu::Function(module, theirFunction.c_str()), params),
      stationIndices(params.bufferSize(STATION_INDICES)) {
  // FlysEye parallellises over time in blockIdx.x
  setEnqueueWorkSizes(
      device, Grid(params.nrSamplesPerChannel, params.nrChannels),
      Block(2,
            std::max(
                16U,
                params.nrTABs))); // if < 16 tabs use more to fill out the wave
  // The additional tabs added to fill out the waves are skipped
  // in the kernel file. Additional threads are used to optimize
  // memory access

  // upload stationIndices, as they are static across the observation
  cu::HostMemory stationIndicesHost(params.bufferSize(STATION_INDICES));
  std::memcpy(stationIndicesHost, &params.stationIndices.front(),
              stationIndicesHost.size());
  stream.memcpyHtoDAsync(stationIndices, stationIndicesHost,
                         stationIndicesHost.size());

  // compute operation and byte counts
  const size_t nrBytesInput = params.bufferSize(FlysEyeKernel::INPUT_DATA);
  const size_t nrBytesOutput = params.bufferSize(FlysEyeKernel::OUTPUT_DATA);
  nrOperations = 0;
  nrBytesRead = nrBytesInput;
  nrBytesWritten = nrBytesOutput;
}

void FlysEyeKernel::enqueue(const BlockID &blockId,
                            const cu::DeviceMemory &input,
                            cu::DeviceMemory &output) {
  setArg(0, output);
  setArg(1, input);
  setArg(2, stationIndices);
  Kernel::enqueue(blockId, input, output);
}

//--------  Template specializations for KernelFactory  --------//
template <>
CompileDefinitions KernelFactory<FlysEyeKernel>::compileDefinitions() const {
  CompileDefinitions defs =
      KernelFactoryBase::compileDefinitions(itsParameters);

  defs["NR_POLARIZATIONS"] = std::to_string(FlysEyeKernel::kNrPolarizations);

  defs["NR_INPUT_STATIONS"] = std::to_string(itsParameters.nrInputStations());
  defs["NR_OUTPUT_STATIONS"] =
      std::to_string(itsParameters.nrSelectedStations());

  defs["NR_CHANNELS"] = std::to_string(itsParameters.nrChannels);
  defs["NR_SAMPLES_PER_CHANNEL"] =
      std::to_string(itsParameters.nrSamplesPerChannel);

  defs["NR_TABS"] = std::to_string(itsParameters.nrTABs);
  defs["NR_INPUT_BITS"] = std::to_string(itsParameters.nrInputBits);

  return defs;
}
} // namespace LOFAR::Cobalt
