#ifndef LOFAR_GPUPROC_CUDA_BEAM_FORMER_WMMA_KERNEL_H
#define LOFAR_GPUPROC_CUDA_BEAM_FORMER_WMMA_KERNEL_H

#include <GPUProc/KernelFactory.h>
#include <GPUProc/Kernels/Kernel.h>

#include <string>
#include <vector>

namespace LOFAR::Cobalt {
class BeamFormerWMMAKernel : public CompiledKernel {
public:
  static std::string theirSourceFile;
  static std::string theirFunction;

  enum BufferType { INPUT_DATA, OUTPUT_DATA, BEAM_FORMER_WEIGHTS };

  // Parameters that must be passed to the constructor of the
  // BeamFormerWMMAKernel class.
  struct Parameters : Kernel::Parameters {
    Parameters(unsigned nrStations, unsigned nrChannels,
               unsigned nrSamplesPerChannel, unsigned nrTABs,
               unsigned nrPolarizations, unsigned nrInputBits,
               bool dumpBuffers = false, std::string dumpFilePrefix = "");

    unsigned nrStations;

    unsigned nrChannels;
    unsigned nrSamplesPerChannel;
    unsigned nrPolarizations;

    unsigned nrTABs;
    unsigned nrInputBits;

    unsigned wmmaK;

    size_t bufferSize(BufferType bufferType) const;
  };

  BeamFormerWMMAKernel(cu::Device &device, cu::Stream &stream,
                       const cu::Module &module, const Parameters &param);

  void setWeights(const cu::DeviceMemory &weights);

  void enqueue(const BlockID &blockId, const cu::DeviceMemory &input,
               cu::DeviceMemory &output);

  static constexpr unsigned kWARP_SIZE = 32;

  static constexpr unsigned kWMMA_M = 16;
  static constexpr unsigned kWMMA_N = 16;

  // number of warps in row/column per block
  static constexpr unsigned kBLOCK_ROW_WARPS = 2;
  static constexpr unsigned kBLOCK_COL_WARPS = 4;

  // number of tiles in row/column per warp
  static constexpr unsigned kWARP_ROW_TILES = 2;
  static constexpr unsigned kWARP_COL_TILES = 1;
};

// # --------  Template specializations for KernelFactory  -------- #//

template <>
CompileDefinitions
KernelFactory<BeamFormerWMMAKernel>::compileDefinitions() const;
} // namespace LOFAR::Cobalt

#endif
