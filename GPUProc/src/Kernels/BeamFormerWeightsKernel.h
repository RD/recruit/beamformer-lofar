#ifndef LOFAR_GPUPROC_CUDA_BEAM_FORMER_WEIGHTS_KERNEL_H
#define LOFAR_GPUPROC_CUDA_BEAM_FORMER_WEIGHTS_KERNEL_H

#include <GPUProc/KernelFactory.h>
#include <GPUProc/Kernels/Kernel.h>

#include <string>
#include <vector>

namespace LOFAR::Cobalt {
class BeamFormerWeightsKernel : public CompiledKernel {
public:
  static std::string theirSourceFile;
  static std::string theirFunction;

  enum BufferType { BEAM_FORMER_DELAYS, BEAM_FORMER_WEIGHTS, DELAY_INDICES };

  // Parameters that must be passed to the constructor of the
  // BeamFormerWeightsKernel class.
  struct Parameters : Kernel::Parameters {
    Parameters(std::vector<unsigned> stationIndices,
               std::vector<unsigned> delayIndices, unsigned nrDelays,
               unsigned nrChannels, unsigned nrTABs, unsigned nrOutputBits,
               double subbandWidth, bool singlePrecision = false,
               bool dumpBuffers = false, std::string dumpFilePrefix = "");

    // The input data is indexed by its station number
    // in the beamformer preprocessor, so we need to map
    // them to beam-former station numbers.
    std::vector<unsigned> stationIndices;

    // The beam-former delays are indexed by their station number
    // in the observation, so we need to map them to beam-former
    // station numbers.
    std::vector<unsigned> delayIndices;

    unsigned nrDelays;

    unsigned nrChannels;

    unsigned nrSAPs;
    unsigned nrTABs;
    unsigned nrOutputBits;
    double subbandBandwidth;

    // Compute the weights in single-precision
    bool singlePrecision;

    size_t bufferSize(BufferType bufferType) const;

    size_t nrSelectedStations() const { return stationIndices.size(); }
  };

  BeamFormerWeightsKernel(cu::Device &device, cu::Stream &stream,
                          const cu::Module &module, const Parameters &param);

  void setSubbandFrequency(double subbandFrequency) {
    subbandFrequency_ = subbandFrequency;
  };

  void enqueue(const BlockID &blockId, const cu::DeviceMemory &input,
               cu::DeviceMemory &output);

  static const unsigned kNrPolarizations = 2;

private:
  double subbandFrequency_;

  cu::DeviceMemory delayIndices;
};

// # --------  Template specializations for KernelFactory  -------- #//

template <>
CompileDefinitions
KernelFactory<BeamFormerWeightsKernel>::compileDefinitions() const;
} // namespace LOFAR::Cobalt

#endif
