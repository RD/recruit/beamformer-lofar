#ifndef LOFAR_GPUPROC_CUDA_QUANTIZE_OUTPUT_KERNEL_H
#define LOFAR_GPUPROC_CUDA_QUANTIZE_OUTPUT_KERNEL_H

#include <CoInterface/Span.h>
#include <GPUProc/KernelFactory.h>
#include <GPUProc/Kernels/Kernel.h>

#include <string>
#include <vector>

namespace LOFAR::Cobalt {

class QuantizeOutputKernel : public CompiledKernel {
public:
  static std::string theirSourceFile;
  static std::string theirFunction;

  enum BufferType {
    INPUT_DATA,
    OUTPUT_DATA,   // total size of output, quantized values, scales and offsets
    OUTPUT_VALUES, // size of quantized values
    OUTPUT_METADATA // size of metadata (same for scales and offsets)
  };

  // Parameters that must be passed to the constructor of the
  struct Parameters : Kernel::Parameters {
    Parameters();
    Parameters(unsigned nrChannels, unsigned nrSamplesPerChannel,
               unsigned nrTABs, unsigned nrStokes, bool outputComplexVoltages,
               unsigned nrQuantizeBits, float quantizeScaleMax,
               float quantizeScaleMin, bool sIpositive,
               bool dumpBuffers = false, std::string dumpFilePrefix = "");

    unsigned nrChannels;
    unsigned nrSamplesPerChannel;

    unsigned nrTABs;
    unsigned nrStokes;
    bool outputComplexVoltages;
    unsigned nrQuantizeBits;
    float quantizeScaleMax;
    float quantizeScaleMin;
    bool sIpositive;

    unsigned int nrThreadsPerBlock;

    size_t bufferSize(BufferType bufferType) const;
  };

  // Offset in bytes in GPU buffer during quantization
  struct OutputBufferOffsets {
    size_t scale, offset, data;
  };

  QuantizeOutputKernel(cu::Device &device, cu::Stream &stream,
                       const cu::Module &module, const Parameters &params);

  void enqueue(const BlockID &blockId, const cu::DeviceMemory &input,
               cu::DeviceMemory &output);

  struct OutputBufferOffsets bufferOffsets() const;

  struct Parameters parameters;
};

// # --------  Template specializations for KernelFactory  -------- #//
template <>
CompileDefinitions
KernelFactory<QuantizeOutputKernel>::compileDefinitions() const;
} // namespace LOFAR::Cobalt

#endif
