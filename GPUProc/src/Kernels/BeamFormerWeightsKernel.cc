#include "BeamFormerWeightsKernel.h"

#include <CoInterface/BlockID.h>
#include <GPUProc/gpu_utils.h>

#include <algorithm>
#include <complex>
#include <cstring>
#include <fstream>
#include <iomanip>

namespace LOFAR::Cobalt {
std::string BeamFormerWeightsKernel::theirSourceFile = "BeamFormerWeights.cu";
std::string BeamFormerWeightsKernel::theirFunction = "beamFormerWeights";

BeamFormerWeightsKernel::Parameters::Parameters(
    std::vector<unsigned> stationIndices_, std::vector<unsigned> delayIndices_,
    unsigned nrDelays_, unsigned nrChannels_, unsigned nrTABs_,
    unsigned nrOutputBits_, double subbandBandWidth_, bool singlePrecision_,
    bool dumpBuffers_, std::string dumpFilePrefix_)
    : Kernel::Parameters("beamFormerWeights"),

      // mapping to translate station numbers in input array (which
      // are the station selected in the beamformer preprocessor)
      stationIndices(stationIndices_),

      // mapping to translate station numbers in delay arrays (which
      // are the observation stations) to the beamformer stations
      delayIndices(delayIndices_), nrDelays(nrDelays_),

      nrChannels(nrChannels_),

      nrTABs(nrTABs_), nrOutputBits(nrOutputBits_),
      subbandBandwidth(subbandBandWidth_), singlePrecision(singlePrecision_) {
  dumpBuffers = dumpBuffers_;
  dumpFilePrefix = dumpFilePrefix_;
}

size_t
BeamFormerWeightsKernel::Parameters::bufferSize(BufferType bufferType) const {
  switch (bufferType) {
  case BeamFormerWeightsKernel::BEAM_FORMER_WEIGHTS:
    return (size_t)nrChannels * nrTABs * stationIndices.size() * 2 *
           (nrOutputBits / 8);
  case BeamFormerWeightsKernel::DELAY_INDICES:
    return (size_t)delayIndices.size() * sizeof delayIndices[0];
  case BeamFormerWeightsKernel::BEAM_FORMER_DELAYS:
    return (size_t)nrDelays * nrTABs * sizeof(double);
  default:
    std::stringstream message;
    message << "Invalid bufferType (" << bufferType << ")";
    throw std::runtime_error(message.str());
  }
}

BeamFormerWeightsKernel::BeamFormerWeightsKernel(cu::Device &device,
                                                 cu::Stream &stream,
                                                 const cu::Module &module,
                                                 const Parameters &params)
    : CompiledKernel(device, stream,
                     cu::Function(module, theirFunction.c_str()), params),
      delayIndices(params.bufferSize(DELAY_INDICES)) {
  Block block(params.nrChannels);
  Grid grid(params.nrTABs, params.nrSelectedStations());

  setEnqueueWorkSizes(device, grid, block);

  // upload delayIndices, as they are static across the observation
  cu::HostMemory delayIndicesHost(params.bufferSize(DELAY_INDICES));
  std::memcpy(delayIndicesHost, &params.delayIndices.front(),
              delayIndicesHost.size());
  stream.memcpyHtoDAsync(delayIndices, delayIndicesHost,
                         delayIndicesHost.size());

  // compute operation and byte counts
  const size_t nrBytesDelays =
      params.bufferSize(BeamFormerWeightsKernel::BEAM_FORMER_DELAYS);
  const size_t nrBytesOutput =
      params.bufferSize(BeamFormerWeightsKernel::BEAM_FORMER_WEIGHTS);

  // Operation count for weight computation:
  // - 2x mul to compute phi per station
  // - 1x sincospi to compute rv per station
  nrOperations = params.nrChannels * params.nrTABs * kNrPolarizations *
                 params.nrSelectedStations() * 3;

  nrBytesRead = nrBytesDelays;
  nrBytesWritten = nrBytesOutput;
}

void BeamFormerWeightsKernel::enqueue(const BlockID &blockId,
                                      const cu::DeviceMemory &input,
                                      cu::DeviceMemory &output) {
  setArg(0, output);
  setArg(1, input);
  setArg(2, delayIndices);
  setArg(3, subbandFrequency_);
  Kernel::enqueue(blockId, input, output);
}

//--------  Template specializations for KernelFactory  --------//
template <>
CompileDefinitions
KernelFactory<BeamFormerWeightsKernel>::compileDefinitions() const {
  CompileDefinitions defs =
      KernelFactoryBase::compileDefinitions(itsParameters);

  defs["NR_OUTPUT_STATIONS"] =
      std::to_string(itsParameters.nrSelectedStations());
  defs["NR_DELAYS"] = std::to_string(itsParameters.nrDelays);

  defs["NR_CHANNELS"] = std::to_string(itsParameters.nrChannels);

  defs["NR_TABS"] = std::to_string(itsParameters.nrTABs);
  defs["NR_OUTPUT_BITS"] = std::to_string(itsParameters.nrOutputBits);
  defs["SUBBAND_BANDWIDTH"] =
      (std::stringstream() << std::fixed << std::setprecision(7)
                           << itsParameters.subbandBandwidth)
          .str();

  if (itsParameters.singlePrecision) {
    defs["SINGLE_PRECISION"] = "1";
  }

  return defs;
}
} // namespace LOFAR::Cobalt
