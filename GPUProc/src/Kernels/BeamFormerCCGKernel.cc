#include "BeamFormerCCGKernel.h"

#include <CoInterface/BlockID.h>
#include <GPUProc/gpu_utils.h>

#include <algorithm>
#include <complex>
#include <fstream>
#include <iostream>
#include <memory>

#include <cuda_fp16.h>

namespace LOFAR::Cobalt {
BeamFormerCCGKernel::Parameters::Parameters(
    unsigned nrStations_, unsigned nrChannels_, unsigned nrSamplesPerChannel_,
    unsigned nrTABs_, unsigned nrPolarizations_, unsigned nrInputBits_,
    bool dumpBuffers_, std::string dumpFilePrefix_)
    : Kernel::Parameters("beamFormerCCG"), nrStations(nrStations_),
      nrChannels(nrChannels_), nrSamplesPerChannel(nrSamplesPerChannel_),
      nrTABs(nrTABs_), nrPolarizations(nrPolarizations_),
      nrInputBits(nrInputBits_) {
  dumpBuffers = dumpBuffers_;
  dumpFilePrefix = dumpFilePrefix_;

  if (nrInputBits == 16) {
    dimensions = ccglib::mma::GEMM::GetDimensions(ccglib::ValueType::float16,
                                                  ccglib::mma::opt);
  } else if (nrInputBits == 32) {
    dimensions = ccglib::mma::GEMM::GetDimensions(ccglib::ValueType::float32,
                                                  ccglib::mma::opt);
  } else {
    throw std::runtime_error(
        "BeamFormerCCGKernel only supports 16 or 32 bit input.");
  }
}

size_t
BeamFormerCCGKernel::Parameters::bufferSize(BufferType bufferType) const {
  const size_t global_m = nrTABs;
  const size_t global_n = nrSamplesPerChannel;
  const size_t global_k = nrStations;

  const size_t m_per_block = dimensions.x;
  const size_t n_per_block = dimensions.y;
  const size_t k_per_wmma = dimensions.z;

  const size_t global_m_padded = ceilDiv(global_m, m_per_block) * m_per_block;
  const size_t global_n_padded = ceilDiv(global_n, n_per_block) * n_per_block;
  const size_t global_k_padded = ceilDiv(global_k, k_per_wmma) * k_per_wmma;

  switch (bufferType) {
  case BeamFormerCCGKernel::INPUT_DATA:
    return (size_t)nrChannels * nrSamplesPerChannel * nrPolarizations *
           nrStations * 2 *
           (nrInputBits / 8); // complex of two float16 or float32
  case BeamFormerCCGKernel::INPUT_DATA_TRANSPOSED:
    return (size_t)nrChannels * global_n_padded * nrPolarizations *
           global_k_padded * 2 *
           (nrInputBits / 8); // complex of two float16 or float32
  case BeamFormerCCGKernel::OUTPUT_DATA:
    return (size_t)nrChannels * nrSamplesPerChannel * nrPolarizations * nrTABs *
           sizeof(std::complex<float>); // always complex float32
  case BeamFormerCCGKernel::BEAM_FORMER_WEIGHTS:
    return (size_t)nrTABs * nrChannels * nrStations * 2 *
           (nrInputBits / 8); // complex of two float16 or float32
  case BeamFormerCCGKernel::BEAM_FORMER_WEIGHTS_TRANSPOSED:
    return (size_t)global_m_padded * nrChannels * global_k_padded * 2 *
           (nrInputBits / 8); // complex of two float16 or float32
  default:
    std::stringstream message;
    message << "Invalid bufferType (" << bufferType << ")";
    throw std::runtime_error(message.str());
  }
}

BeamFormerCCGKernel::BeamFormerCCGKernel(cu::Device &device, cu::Stream &stream,
                                         const Parameters &params)
    : Kernel(device, stream, params), parameters(params) {
  const size_t batch_size = params.nrChannels;
  const size_t global_m = params.nrTABs;
  const size_t global_n = params.nrSamplesPerChannel;
  const size_t global_k = params.nrStations;
  const size_t nr_input_bits = params.nrInputBits;
  const size_t nr_polarizations = params.nrPolarizations;

  const size_t m_per_block = params.dimensions.x;
  const size_t n_per_block = params.dimensions.y;
  const size_t k_per_wmma = params.dimensions.z;

  transpose_weights_ = std::make_unique<ccglib::transpose::Transpose>(
      batch_size, global_m, global_k, m_per_block, k_per_wmma,
      params.nrInputBits, device, stream, ccglib::transpose::complex_last);
  transpose_input_ = std::make_unique<ccglib::transpose::Transpose>(
      batch_size, global_n, global_k, n_per_block, k_per_wmma,
      params.nrInputBits, device, stream, ccglib::transpose::complex_last);
  if (nr_input_bits == 16) {
    gemm_ = std::make_unique<ccglib::mma::GEMM>(
        batch_size, global_m, global_n, global_k, nr_input_bits, device, stream,
        ccglib::ValueType::float16, ccglib::mma::opt,
        ccglib::mma::complex_last);
  } else if (nr_input_bits == 32) {
    gemm_ = std::make_unique<ccglib::mma::GEMM>(
        batch_size, global_m, global_n, global_k, nr_input_bits, device, stream,
        ccglib::ValueType::float32, ccglib::mma::opt,
        ccglib::mma::complex_last);
  }

  // compute operation and byte counts
  const size_t nrBytesWeights =
      params.bufferSize(BeamFormerCCGKernel::BEAM_FORMER_WEIGHTS);
  const size_t nrBytesInput =
      params.bufferSize(BeamFormerCCGKernel::INPUT_DATA);
  const size_t nrBytesOutput =
      params.bufferSize(BeamFormerCCGKernel::OUTPUT_DATA);

  // Operation count for complex weighted multiply-add:
  // - 1x complex fused multiply-add per sample
  const size_t nrOperationsCWMA = 1UL * params.nrChannels * params.nrTABs *
                                  params.nrPolarizations * params.nrStations *
                                  params.nrSamplesPerChannel * 8;
  nrOperations = nrOperationsCWMA;
  nrBytesRead = nrBytesWeights + nrBytesInput;
  nrBytesWritten = nrBytesOutput;

  // Allocate buffer for one polarization of tranposed input
  const size_t sizeof_input_transposed =
      parameters.bufferSize(INPUT_DATA_TRANSPOSED);
  input_transposed_ =
      std::make_unique<cu::DeviceMemory>(sizeof_input_transposed / 2);
}

void BeamFormerCCGKernel::setWeights(const cu::DeviceMemory &weights) {
  const size_t sizeof_weights_transposed =
      parameters.bufferSize(BEAM_FORMER_WEIGHTS_TRANSPOSED);
  weights_transposed_ =
      std::make_unique<cu::DeviceMemory>(sizeof_weights_transposed);
  transpose_weights_->Run(const_cast<cu::DeviceMemory &>(weights),
                          *weights_transposed_);
}

void BeamFormerCCGKernel::enqueue(const BlockID &blockId,
                                  const cu::DeviceMemory &input,
                                  cu::DeviceMemory &output) {
  itsCounter.recordStart(itsStream);
  launch(blockId, input, output);
  itsCounter.recordStop(itsStream);
}

void BeamFormerCCGKernel::launch(const BlockID &blockId,
                                 const cu::DeviceMemory &input,
                                 cu::DeviceMemory &output) {
  const size_t input_size_half = input.size() / 2;
  const size_t output_size_half = output.size() / 2;

  for (unsigned pol = 0; pol < 2; pol++) {
    const size_t input_offset = pol * input_size_half;
    const size_t output_offset = pol * output_size_half;
    cu::DeviceMemory input_pol(input + input_offset, input_size_half);
    cu::DeviceMemory output_pol(output + output_offset, output_size_half);
    transpose_input_->Run(const_cast<cu::DeviceMemory &>(input_pol),
                          *input_transposed_);
    gemm_->Run(*weights_transposed_, *input_transposed_, output_pol);
  }
}

} // namespace LOFAR::Cobalt
