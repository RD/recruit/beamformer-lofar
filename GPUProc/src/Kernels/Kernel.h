#ifndef LOFAR_GPUPROC_CUDA_KERNEL_H
#define LOFAR_GPUPROC_CUDA_KERNEL_H

#include <iosfwd>
#include <string>

#include <GPUProc/PerformanceCounter.h>
#include <GPUProc/gpu_utils.h>

namespace LOFAR::Cobalt {

/*
 * Returns ceil(n/divisor).
 */
template <typename T> inline static T ceilDiv(T n, T divisor) {
  return (n + divisor - 1) / divisor;
}

// # Forward declarations
struct BlockID;

/*
 * A wrapper for a generic kernel that can be executed on a GPU, and transforms
 * data from an input buffer to an output buffer.
 */
class Kernel {
public:
  // Parameters that must be passed to the constructor of this Kernel class.
  // TODO: more at constructor passed immediates can be turned into defines
  // (blockDim/gridDim too if enforced fixed (consider conditional define)
  // or drop opt)
  struct Parameters {
    Parameters(const std::string &name);

    std::string name;

    bool dumpBuffers;
    std::string dumpFilePrefix;
  };

  void enqueue(const BlockID &blockId, const cu::DeviceMemory &input,
               cu::DeviceMemory &output);

  // Warning: user has to make sure the Kernel is not running!
  RunningStatistics getStats() { return itsCounter.getStats(); }

  size_t getNrOperations() { return nrOperations; }

  size_t getNrBytesRead() { return nrBytesRead; }

  size_t getNrBytesWritten() { return nrBytesWritten; }

protected:
  // Construct a kernel.
  Kernel(cu::Device &device, cu::Stream &stream, const Parameters &params);

  // Performance counter for work done by this kernel
  PerformanceCounter itsCounter;

  size_t nrOperations, nrBytesRead, nrBytesWritten;

  // Launch the actual kernel
  virtual void launch(const BlockID &blockId, const cu::DeviceMemory &input,
                      cu::DeviceMemory &output) = 0;

  // The GPU Stream associated with this kernel.
  cu::Stream &itsStream;

  // The parameters as given to the constructor.
  const Parameters &itsParameters;

private:
  // Dump output buffer of a this kernel to disk. Use \a blockId to
  // distinguish between the different blocks and subbands.
  // \attention This method is for debugging purposes only, as it has a
  // severe impact on performance.
  void dumpBuffers(const BlockID &blockId,
                   const cu::DeviceMemory &output) const;
};

/*
 * A Kernel that is actually a CUDA JIT compiled function.
 */
class CompiledKernel : public Kernel {
protected:
  // Construct a kernel.
  CompiledKernel(cu::Device &device, cu::Stream &stream,
                 const cu::Function &function, const Parameters &params,
                 bool verbose = true);

  // Explicit destructor, because the implicitly generated one is public.
  virtual ~CompiledKernel();

  // Set kernel DeviceMemory object argument number \a index to \a mem.
  // \a mem must outlive kernel execution.
  void setArg(size_t index, const cu::DeviceMemory &mem);

  // Set pointer to kernel device memory object (as void *) number \a index
  // to \a val. \a *val must outlive kernel execution.
  // Note: Prefer to use setArg() passing a DeviceMemory ref over this overload.
  void setArg(size_t index, const void **val);

  // Set kernel immediate argument number \a index to \a val.
  // \a val must outlive kernel execution.
  // Not for device memory objects (be it as DeviceMemory or as void *).
  template <typename T> void setArg(size_t index, const T &val) {
    doSetArg(index, &val);
  }

  void launch(const BlockID &blockId, const cu::DeviceMemory &input,
              cu::DeviceMemory &output);

  // Set the passed execution configuration if supported on the hardware
  // in the stream for this kernel.
  // If not supported and NULL was passed in errorStrings, an exc is thrown.
  // If not supported and errorsStrings is valid, an error string is written
  // to the errorStrings pointer.
  void setEnqueueWorkSizes(cu::Device &device, Grid globalWorkSize,
                           Block localWorkSize,
                           std::string *errorStrings = NULL);

  const unsigned maxThreadsPerBlock;

private:
  // Function for kernel execution
  cu::Function itsFunction;

  // Function arguments as set.
  std::vector<const void *> itsKernelArgs;

  // Helper function to modify _kernelArgs.
  void doSetArg(size_t index, const void *argp);

  // The grid of blocks dimensions for kernel execution.
  Grid itsGridDims;

  // The block of threads dimensions for kernel execution.
  Block itsBlockDims;

  // Print resource usage and launch configuration.
  bool verbose_;
};
} // namespace LOFAR::Cobalt

#endif
