#include "BeamFormerKernel.h"

#include <CoInterface/BlockID.h>
#include <GPUProc/gpu_utils.h>

#include <algorithm>
#include <complex>
#include <cstring>
#include <fstream>
#include <iomanip>

namespace LOFAR::Cobalt {
std::string BeamFormerKernel::theirSourceFile = "BeamFormer.cu";
std::string BeamFormerKernel::theirFunction = "beamFormer";

BeamFormerKernel::Parameters::Parameters(
    std::vector<unsigned> stationIndices_, std::vector<unsigned> delayIndices_,
    unsigned nrDelays_, unsigned nrChannels_, unsigned nrSamplesPerChannel_,
    unsigned nrTABs_, unsigned nrInputBits_, double subbandBandWidth_,
    bool computeWeights_, bool dumpBuffers_, std::string dumpFilePrefix_)
    : Kernel::Parameters("beamFormer"),

      // mapping to translate station numbers in input array (which
      // are the station selected in the beamformer preprocessor)
      stationIndices(stationIndices_),

      // mapping to translate station numbers in delay arrays (which
      // are the observation stations) to the beamformer stations
      delayIndices(delayIndices_), nrDelays(nrDelays_),

      nrChannels(nrChannels_), nrSamplesPerChannel(nrSamplesPerChannel_),

      nrTABs(nrTABs_), nrInputBits(nrInputBits_),
      subbandBandwidth(subbandBandWidth_) {
  computeWeights = computeWeights_;
  nrStationsPerPass =
      std::min(nrSelectedStations(), size_t(kNrStationsPerPass));

  dumpBuffers = dumpBuffers_;
  dumpFilePrefix = dumpFilePrefix_;
}

size_t BeamFormerKernel::Parameters::bufferSize(BufferType bufferType) const {
  switch (bufferType) {
  case BeamFormerKernel::INPUT_DATA:
    return (size_t)nrChannels * nrSamplesPerChannel * kNrPolarizations *
           nrInputStations() * 2 * (nrInputBits / 8);
  case BeamFormerKernel::OUTPUT_DATA:
    return (size_t)nrChannels * nrSamplesPerChannel * kNrPolarizations *
           nrTABs * sizeof(std::complex<float>);
  case BeamFormerKernel::STATION_INDICES:
    return (size_t)stationIndices.size() * sizeof stationIndices[0];
  case BeamFormerKernel::DELAY_INDICES:
    return (size_t)delayIndices.size() * sizeof delayIndices[0];
  case BeamFormerKernel::BEAM_FORMER_DELAYS:
    return (size_t)nrDelays * nrTABs * sizeof(double);
  case BeamFormerKernel::BEAM_FORMER_WEIGHTS:
    return (size_t)nrTABs * nrChannels * stationIndices.size() * 2 *
           (nrInputBits / 8);
  default:
    std::stringstream message;
    message << "Invalid bufferType (" << bufferType << ")";
    throw std::runtime_error(message.str());
  }
}

BeamFormerKernel::BeamFormerKernel(cu::Device &device, cu::Stream &stream,
                                   const cu::Module &module,
                                   const Parameters &params)
    : CompiledKernel(device, stream,
                     cu::Function(module, theirFunction.c_str()), params),
      stationIndices(params.bufferSize(STATION_INDICES)),
      delayIndices(params.bufferSize(DELAY_INDICES)) {
  // Beamformer kernel requires 1 channel in the blockDim.z dimension
  // FlysEye parallellises over time in blockIdx.x
  setEnqueueWorkSizes(
      device,
      Grid(params.nrChannels,
           (params.nrTABs + kNrTabsPerBlock - 1) / kNrTabsPerBlock),
      Block(kNrPolarizations, kNrTabsPerBlock));

  // upload stationIndices, as they are static across the observation
  cu::HostMemory stationIndicesHost(params.bufferSize(STATION_INDICES));
  std::memcpy(stationIndicesHost, &params.stationIndices.front(),
              stationIndicesHost.size());
  stream.memcpyHtoDAsync(stationIndices, stationIndicesHost,
                         stationIndicesHost.size());

  // upload delayIndices, as they are static across the observation
  if (params.computeWeights) {
    cu::HostMemory delayIndicesHost(params.bufferSize(DELAY_INDICES));
    std::memcpy(delayIndicesHost, &params.delayIndices.front(),
                delayIndicesHost.size());
    stream.memcpyHtoDAsync(delayIndices, delayIndicesHost,
                           delayIndicesHost.size());
  }

  // compute operation and byte counts
  const size_t nrBytesDelays =
      params.bufferSize(BeamFormerKernel::BEAM_FORMER_DELAYS);
  const size_t nrBytesWeights =
      params.bufferSize(BeamFormerKernel::BEAM_FORMER_WEIGHTS);
  const size_t nrBytesInput = params.bufferSize(BeamFormerKernel::INPUT_DATA);
  const size_t nrBytesOutput = params.bufferSize(BeamFormerKernel::OUTPUT_DATA);

  // Operation count for weight computation:
  // - 2x mul to compute phi per station
  // - 1x sincospi to compute rv per station
  const size_t nrOperationsWeights = params.nrChannels * params.nrTABs *
                                     kNrPolarizations *
                                     params.nrSelectedStations() * 3;
  nrOperations += params.computeWeights * nrOperationsWeights;

  // Operation count for complex weighted multiply-add:
  // - 1x complex fused multiply-add per sample
  const size_t nrOperationsCWMA = params.nrChannels * params.nrTABs *
                                  kNrPolarizations * params.nrInputStations() *
                                  params.nrSamplesPerChannel * 8;
  nrOperations = nrOperationsWeights + nrOperationsCWMA;

  if (params.computeWeights) {
    nrBytesRead = nrBytesDelays + nrBytesInput;
  } else {
    nrBytesRead = nrBytesWeights + nrBytesInput;
  }
  const unsigned nrPasses =
      (params.nrSelectedStations() + params.nrStationsPerPass - 1) /
      params.nrStationsPerPass;
  nrBytesRead += (nrPasses - 1) * (nrBytesOutput / params.nrSelectedStations() *
                                   params.nrStationsPerPass);
  nrBytesWritten = nrBytesOutput;
}

void BeamFormerKernel::setWeights(const cu::DeviceMemory &weights) {
  setArg(3, weights);
}

void BeamFormerKernel::setDelays(const cu::DeviceMemory &delays) {
  setArg(3, delays);
}

void BeamFormerKernel::enqueue(const BlockID &blockId,
                               const cu::DeviceMemory &input,
                               cu::DeviceMemory &output) {
  setArg(0, output);
  setArg(1, input);
  setArg(2, stationIndices);
  setArg(4, delayIndices);
  setArg(5, subbandFrequency_);
  Kernel::enqueue(blockId, input, output);
}

//--------  Template specializations for KernelFactory  --------//
template <>
CompileDefinitions KernelFactory<BeamFormerKernel>::compileDefinitions() const {
  CompileDefinitions defs =
      KernelFactoryBase::compileDefinitions(itsParameters);

  defs["NR_POLARIZATIONS"] = std::to_string(BeamFormerKernel::kNrPolarizations);

  defs["NR_TIME_PER_PASS"] = std::to_string(BeamFormerKernel::kNrTimePerPass);
  defs["NR_STATIONS_PER_PASS"] =
      std::to_string(itsParameters.nrStationsPerPass);

  defs["NR_INPUT_STATIONS"] = std::to_string(itsParameters.nrInputStations());
  defs["NR_OUTPUT_STATIONS"] =
      std::to_string(itsParameters.nrSelectedStations());
  defs["NR_DELAYS"] =
      std::to_string(itsParameters.computeWeights * itsParameters.nrDelays);

  defs["NR_CHANNELS"] = std::to_string(itsParameters.nrChannels);
  defs["NR_SAMPLES_PER_CHANNEL"] =
      std::to_string(itsParameters.nrSamplesPerChannel);

  defs["NR_TABS"] = std::to_string(itsParameters.nrTABs);
  defs["NR_INPUT_BITS"] = std::to_string(itsParameters.nrInputBits);
  defs["SUBBAND_BANDWIDTH"] =
      (std::stringstream() << std::fixed << std::setprecision(7)
                           << itsParameters.subbandBandwidth)
          .str();

  return defs;
}
} // namespace LOFAR::Cobalt
