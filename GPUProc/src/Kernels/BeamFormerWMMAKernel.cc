#include "BeamFormerWMMAKernel.h"

#include <CoInterface/BlockID.h>
#include <GPUProc/gpu_utils.h>

#include <algorithm>
#include <complex>
#include <fstream>

#include <cuda_fp16.h>

namespace LOFAR::Cobalt {
std::string BeamFormerWMMAKernel::theirSourceFile = "BeamFormerWMMA.cu";
std::string BeamFormerWMMAKernel::theirFunction = "wmma_gemm";

BeamFormerWMMAKernel::Parameters::Parameters(
    unsigned nrStations_, unsigned nrChannels_, unsigned nrSamplesPerChannel_,
    unsigned nrTABs_, unsigned nrPolarizations_, unsigned nrInputBits_,
    bool dumpBuffers_, std::string dumpFilePrefix_)
    : Kernel::Parameters("beamFormerWMMA"), nrStations(nrStations_),
      nrChannels(nrChannels_), nrSamplesPerChannel(nrSamplesPerChannel_),
      nrTABs(nrTABs_), nrPolarizations(nrPolarizations_),
      nrInputBits(nrInputBits_), wmmaK(nrInputBits == 16 ? 16 : 8) {
  dumpBuffers = dumpBuffers_;
  dumpFilePrefix = dumpFilePrefix_;
}

size_t
BeamFormerWMMAKernel::Parameters::bufferSize(BufferType bufferType) const {
  switch (bufferType) {
  case BeamFormerWMMAKernel::INPUT_DATA:
    return (size_t)nrChannels * nrSamplesPerChannel * nrPolarizations *
           nrStations * 2 *
           (nrInputBits / 8); // complex of two float16 or float32
  case BeamFormerWMMAKernel::OUTPUT_DATA:
    return (size_t)nrChannels * nrSamplesPerChannel * nrPolarizations * nrTABs *
           sizeof(std::complex<float>); // always complex float32
  case BeamFormerWMMAKernel::BEAM_FORMER_WEIGHTS:
    return (size_t)nrTABs * nrChannels * nrStations * 2 *
           (nrInputBits / 8); // complex of two float16 or float32
  default:
    std::stringstream message;
    message << "Invalid bufferType (" << bufferType << ")";
    throw std::runtime_error(message.str());
  }
}

BeamFormerWMMAKernel::BeamFormerWMMAKernel(cu::Device &device,
                                           cu::Stream &stream,
                                           const cu::Module &module,
                                           const Parameters &params)
    : CompiledKernel(device, stream,
                     cu::Function(module, theirFunction.c_str()), params) {
  // number of tiles in row/column per block
  const unsigned BLOCK_ROW_TILES = (kWARP_ROW_TILES * kBLOCK_ROW_WARPS); // 4
  const unsigned BLOCK_COL_TILES = (kWARP_COL_TILES * kBLOCK_COL_WARPS); // 4

  // total size of tiles in row/column per block
  const unsigned M_TILES = (BLOCK_ROW_TILES * kWMMA_M); // 4*16=64
  const unsigned N_TILES = (BLOCK_COL_TILES * kWMMA_N); // 4*16=64

  // 32 threads per block in x, 2 in y, 4 in z (total 256)
  Block block(kWARP_SIZE, kBLOCK_ROW_WARPS, kBLOCK_COL_WARPS);
  Grid grid(std::max(1u, params.nrTABs / M_TILES),
            std::max(1u, params.nrSamplesPerChannel / N_TILES),
            params.nrChannels * params.nrPolarizations);

  setEnqueueWorkSizes(device, grid, block);

  // compute operation and byte counts
  const size_t nrBytesWeights =
      params.bufferSize(BeamFormerWMMAKernel::BEAM_FORMER_WEIGHTS);
  const size_t nrBytesInput =
      params.bufferSize(BeamFormerWMMAKernel::INPUT_DATA);
  const size_t nrBytesOutput =
      params.bufferSize(BeamFormerWMMAKernel::OUTPUT_DATA);

  // Operation count for complex weighted multiply-add:
  // - 1x complex fused multiply-add per sample
  const size_t nrOperationsCWMA = 1UL * params.nrChannels * params.nrTABs *
                                  params.nrPolarizations * params.nrStations *
                                  params.nrSamplesPerChannel * 8;
  nrOperations = nrOperationsCWMA;
  nrBytesRead = nrBytesWeights + nrBytesInput;
  nrBytesWritten = nrBytesOutput;
}

void BeamFormerWMMAKernel::setWeights(const cu::DeviceMemory &weights) {
  setArg(0, weights);
}

void BeamFormerWMMAKernel::enqueue(const BlockID &blockId,
                                   const cu::DeviceMemory &input,
                                   cu::DeviceMemory &output) {
  setArg(1, input);
  setArg(2, output);
  Kernel::enqueue(blockId, input, output);
}

//--------  Template specializations for KernelFactory  --------//
template <>
CompileDefinitions
KernelFactory<BeamFormerWMMAKernel>::compileDefinitions() const {
  CompileDefinitions defs =
      KernelFactoryBase::compileDefinitions(itsParameters);

  defs["NR_STATIONS"] = std::to_string(itsParameters.nrStations);
  defs["NR_CHANNELS"] = std::to_string(itsParameters.nrChannels);
  defs["NR_SAMPLES"] = std::to_string(itsParameters.nrSamplesPerChannel);
  defs["NR_BEAMS"] = std::to_string(itsParameters.nrTABs);
  defs["NR_POLARIZATIONS"] = std::to_string(itsParameters.nrPolarizations);
  defs["NR_INPUT_BITS"] = std::to_string(itsParameters.nrInputBits);

  defs["WARP_SIZE"] = std::to_string(BeamFormerWMMAKernel::kWARP_SIZE);
  defs["WMMA_M"] = std::to_string(BeamFormerWMMAKernel::kWMMA_M);
  defs["WMMA_N"] = std::to_string(BeamFormerWMMAKernel::kWMMA_N);
  defs["WMMA_K"] = std::to_string(itsParameters.wmmaK);
  defs["BLOCK_ROW_WARPS"] =
      std::to_string(BeamFormerWMMAKernel::kBLOCK_ROW_WARPS);
  defs["BLOCK_COL_WARPS"] =
      std::to_string(BeamFormerWMMAKernel::kBLOCK_COL_WARPS);
  defs["WARP_ROW_TILES"] =
      std::to_string(BeamFormerWMMAKernel::kWARP_ROW_TILES);
  defs["WARP_COL_TILES"] =
      std::to_string(BeamFormerWMMAKernel::kWARP_COL_TILES);

  return defs;
}
} // namespace LOFAR::Cobalt
