#include "QuantizeOutputKernel.h"

#include <CoInterface/BlockID.h>
#include <GPUProc/gpu_utils.h>

#include <cassert>

namespace LOFAR::Cobalt {
std::string QuantizeOutputKernel::theirSourceFile = "QuantizeOutput.cu";
std::string QuantizeOutputKernel::theirFunction = "quantizeOutput";

QuantizeOutputKernel::Parameters::Parameters()
    : Kernel::Parameters(theirFunction) {}

QuantizeOutputKernel::Parameters::Parameters(
    unsigned nrChannels_, unsigned nrSamplesPerChannel_, unsigned nrTABs_,
    unsigned nrStokes_, bool outputComplexVoltages_, unsigned nrQuantizeBits_,
    float quantizeScaleMax_, float quantizeScaleMin_, bool sIpositive_,
    bool dumpBuffers_, std::string dumpFilePrefix_)
    : Kernel::Parameters(theirFunction), nrChannels(nrChannels_),
      nrSamplesPerChannel(nrSamplesPerChannel_), nrTABs(nrTABs_),
      nrStokes(nrStokes_), outputComplexVoltages(outputComplexVoltages_),
      nrQuantizeBits(nrQuantizeBits_), quantizeScaleMax(quantizeScaleMax_),
      quantizeScaleMin(quantizeScaleMin_), sIpositive(sIpositive_) {
  nrThreadsPerBlock = 512;

  dumpBuffers = dumpBuffers_;
  dumpFilePrefix = dumpFilePrefix_;
}

size_t
QuantizeOutputKernel::Parameters::bufferSize(BufferType bufferType) const {
  switch (bufferType) {
  case QuantizeOutputKernel::INPUT_DATA:
    return (size_t)nrTABs * nrStokes * nrSamplesPerChannel * nrChannels *
           sizeof(float); // values
  case QuantizeOutputKernel::OUTPUT_VALUES:
    return (size_t)nrTABs * nrStokes * nrSamplesPerChannel * nrChannels *
           ((float)nrQuantizeBits / 8);
  case QuantizeOutputKernel::OUTPUT_METADATA:
    return (size_t)nrTABs * nrStokes * nrChannels * sizeof(float);
  case QuantizeOutputKernel::OUTPUT_DATA:
    return bufferSize(OUTPUT_VALUES) + 2 * bufferSize(OUTPUT_METADATA);

  default:
    std::stringstream message;
    message << "Invalid bufferType (" << bufferType << ")";
    throw std::runtime_error(message.str());
  }
}

QuantizeOutputKernel::QuantizeOutputKernel(cu::Device &device,
                                           cu::Stream &stream,
                                           const cu::Module &module,
                                           const Parameters &params)
    : CompiledKernel(device, stream,
                     cu::Function(module, theirFunction.c_str()), params),
      parameters(params) {
  assert(params.nrSamplesPerChannel > 0);
  assert(params.nrStokes == 1 || params.nrStokes == 4);

  const int nrMPs =
      device.getAttribute(CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT);
  Grid grid(params.nrStokes * nrMPs);
  Block block(params.nrThreadsPerBlock);

  setEnqueueWorkSizes(device, grid, block);

  const unsigned n = params.nrSamplesPerChannel;
  const unsigned nrOperationsMean = n;
  const unsigned nrOperationsSD = 3 * n;
  const unsigned nrOperationsQuantization = 3 * n;
  const unsigned nrOperationsPerBlock =
      nrOperationsMean + nrOperationsSD + nrOperationsQuantization;
  nrOperations = params.nrTABs * params.nrStokes * params.nrChannels *
                 nrOperationsPerBlock;

  nrBytesRead = parameters.bufferSize(INPUT_DATA);
  nrBytesWritten = parameters.bufferSize(OUTPUT_DATA);
}

struct QuantizeOutputKernel::OutputBufferOffsets
QuantizeOutputKernel::bufferOffsets() const {
  const size_t sizeof_metada = parameters.bufferSize(OUTPUT_METADATA);
  return {0, sizeof_metada, 2 * sizeof_metada};
}

void QuantizeOutputKernel::enqueue(const BlockID &blockId,
                                   const cu::DeviceMemory &input,
                                   cu::DeviceMemory &output) {
  setArg(0, output);
  setArg(1, input);
  setArg(2, parameters.quantizeScaleMax);
  setArg(3, parameters.quantizeScaleMin);

  Kernel::enqueue(blockId, input, output);
}

//--------  Template specializations for KernelFactory  --------//

template <>
CompileDefinitions
KernelFactory<QuantizeOutputKernel>::compileDefinitions() const {
  CompileDefinitions defs =
      KernelFactoryBase::compileDefinitions(itsParameters);

  defs["NR_CHANNELS"] = std::to_string(itsParameters.nrChannels);
  defs["NR_SAMPLES_PER_CHANNEL"] =
      std::to_string(itsParameters.nrSamplesPerChannel);

  defs["NR_TABS"] = std::to_string(itsParameters.nrTABs);
  defs["COMPLEX_VOLTAGES"] = itsParameters.outputComplexVoltages ? "1" : "0";
  defs["STOKES_I_POSITIVE"] = itsParameters.sIpositive ? "1" : "0";
  defs["NR_COHERENT_STOKES"] = std::to_string(itsParameters.nrStokes);

  defs["NR_QUANTIZE_BITS"] = std::to_string(itsParameters.nrQuantizeBits);

  defs["NR_THREADS_PER_BLOCK"] =
      std::to_string(itsParameters.nrThreadsPerBlock);

  return defs;
}

} // namespace LOFAR::Cobalt
