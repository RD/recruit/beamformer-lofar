#ifndef LOFAR_GPUPROC_CUDA_FLYS_EYE_KERNEL_H
#define LOFAR_GPUPROC_CUDA_FLYS_EYE_KERNEL_H

#include <GPUProc/KernelFactory.h>
#include <GPUProc/Kernels/Kernel.h>

#include <string>
#include <vector>

namespace LOFAR::Cobalt {
class FlysEyeKernel : public CompiledKernel {
public:
  static std::string theirSourceFile;
  static std::string theirFunction;

  enum BufferType { INPUT_DATA, OUTPUT_DATA, STATION_INDICES };

  // Parameters that must be passed to the constructor of the
  // FlysEye class.
  struct Parameters : Kernel::Parameters {
    Parameters(std::vector<unsigned> stationIndices, unsigned nrChannels,
               unsigned nrSamplesPerChannel, unsigned nrTABs,
               unsigned nrInputBits, bool dumpBuffers = false,
               std::string dumpFilePrefix = "");

    // The input data is indexed by its station number
    // in the beamformer preprocessor, so we need to map
    // them to beam-former station numbers.
    std::vector<unsigned> stationIndices;

    unsigned nrChannels;
    unsigned nrSamplesPerChannel;

    unsigned nrTABs;
    unsigned nrInputBits;
    double subbandBandwidth;

    size_t bufferSize(BufferType bufferType) const;

    size_t nrInputStations() const { return stationIndices.size(); }
    size_t nrSelectedStations() const { return stationIndices.size(); }
  };

  FlysEyeKernel(cu::Device &device, cu::Stream &stream,
                const cu::Module &module, const Parameters &param);

  void enqueue(const BlockID &blockId, const cu::DeviceMemory &input,
               cu::DeviceMemory &output);

  static constexpr unsigned kNrPolarizations = 2;

  cu::DeviceMemory stationIndices;
};

// # --------  Template specializations for KernelFactory  -------- #//

template <>
CompileDefinitions KernelFactory<FlysEyeKernel>::compileDefinitions() const;
} // namespace LOFAR::Cobalt

#endif
