#ifndef LOFAR_GPUPROC_CUDA_FIR_FILTER_KERNEL_H
#define LOFAR_GPUPROC_CUDA_FIR_FILTER_KERNEL_H

#include <GPUProc/KernelFactory.h>
#include <GPUProc/Kernels/Kernel.h>

#include <string>
#include <vector>

namespace LOFAR::Cobalt {
class FIR_FilterKernel : public CompiledKernel {
public:
  static std::string theirSourceFile;
  static std::string theirFunction;

  enum BufferType { INPUT_DATA, OUTPUT_DATA, FILTER_WEIGHTS, HISTORY_DATA };

  // Parameters that must be passed to the constructor of the
  // FIR_FilterKernel class.
  struct Parameters : Kernel::Parameters {
    Parameters(unsigned nrTABs, unsigned nrBitsPerSample, unsigned nrChannels,
               unsigned nrSamplesPerChannel, float scaleFactor,
               bool enableFFT = false, const bool dumpBuffers = false,
               std::string dumpFilePrefix = "");

    // The number of TABs to filter.
    unsigned nrTABs;

    unsigned nrBitsPerSample;
    unsigned nrBytesPerComplexSample() const;

    unsigned nrChannels;
    unsigned nrSamplesPerChannel;
    unsigned nrSamplesPerSubband() const;

    // The number of PPF filter taps.
    static const unsigned nrTaps = 16;

    // The number of history samples used for each block
    unsigned nrHistorySamples() const;

    // Additional scale factor (e.g. for FFT normalization).
    float scaleFactor;

    bool enableFFT;

    size_t bufferSize(FIR_FilterKernel::BufferType bufferType) const;
  };

  static constexpr unsigned kComplex = 2;
  static constexpr unsigned kNrPolarizations = 2;

  FIR_FilterKernel(cu::Device &device, cu::Stream &stream,
                   const cu::Module &module, const Parameters &params);

  void setWeights(const void *weights);

  void enqueue(const BlockID &blockId, const cu::DeviceMemory &input,
               cu::DeviceMemory &output);

private:
  // The FIR filter weights
  cu::HostMemory h_filterWeights;
  cu::DeviceMemory d_filterWeights;

  // The history samples
  cu::DeviceMemory d_historySamples;
};

// # --------  Template specializations for KernelFactory  -------- #//

template <>
CompileDefinitions KernelFactory<FIR_FilterKernel>::compileDefinitions() const;

template <> CompileFlags KernelFactory<FIR_FilterKernel>::compileFlags() const;
} // namespace LOFAR::Cobalt

#endif
