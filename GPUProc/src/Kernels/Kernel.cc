#include <cuda_runtime.h>
#include <iostream>
#include <ostream>
#include <sstream>

#include <CoInterface/BlockID.h>
#include <GPUProc/Kernels/Kernel.h>
#include <GPUProc/PerformanceCounter.h>

namespace LOFAR::Cobalt {
Kernel::Parameters::Parameters(const std::string &name)
    : name(name), dumpBuffers(false) {}

Kernel::Kernel(cu::Device &device, cu::Stream &stream, const Parameters &params)
    : itsCounter(params.name), itsStream(stream), itsParameters(params) {}

void Kernel::enqueue(const BlockID &blockId, const cu::DeviceMemory &input,
                     cu::DeviceMemory &output) {
  // record duration of last invocation (or noop
  // if there was none)
  itsCounter.recordStart(itsStream);
  launch(blockId, input, output);
  itsCounter.recordStop(itsStream);

  if (itsParameters.dumpBuffers && blockId.block >= 0) {
    itsStream.synchronize();
    dumpBuffers(blockId, output);
  }
}

void Kernel::dumpBuffers(const BlockID &blockId,
                         const cu::DeviceMemory &output) const {
  const std::string dumpFile =
      (std::stringstream() << itsParameters.dumpFilePrefix
                           << blockId.globalSubbandIdx % blockId.block)
          .str();
  dumpBuffer(output, dumpFile);
}

CompiledKernel::CompiledKernel(cu::Device &device, cu::Stream &stream,
                               const cu::Function &function,
                               const Parameters &params, bool verbose)
    : Kernel(device, stream, params), itsFunction(function),
      maxThreadsPerBlock(
          function.getAttribute(CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK)),
      verbose_(verbose) {
  if (verbose) {
    std::clog << "Function " << function.name() << ":"
              << "\n  nr. of registers used : "
              << function.getAttribute(CU_FUNC_ATTRIBUTE_NUM_REGS)
              << "\n  nr. of bytes of shared memory used (static) : "
              << function.getAttribute(CU_FUNC_ATTRIBUTE_SHARED_SIZE_BYTES)
              << std::endl;
  }
}

CompiledKernel::~CompiledKernel() {}

void CompiledKernel::setArg(size_t index, const cu::DeviceMemory &mem) {
  doSetArg(index, mem.parameter());
}

void CompiledKernel::setArg(size_t index, const void **val) {
  doSetArg(index, (const void *)val);
}

void CompiledKernel::doSetArg(size_t index, const void *argp) {
  if (index >= itsKernelArgs.size()) {
    itsKernelArgs.resize(index + 1);
  }
  itsKernelArgs[index] = argp;
}

void CompiledKernel::launch(const BlockID &, const cu::DeviceMemory &,
                            cu::DeviceMemory &) {
  itsStream.launchKernel(itsFunction, itsGridDims.x, itsGridDims.y,
                         itsGridDims.z, itsBlockDims.x, itsBlockDims.y,
                         itsBlockDims.z, 0, itsKernelArgs);
}

void CompiledKernel::setEnqueueWorkSizes(cu::Device &device,
                                         Grid globalWorkSize,
                                         Block localWorkSize,
                                         std::string *errorStrings) {
  std::ostringstream errMsgs;

  // Enforce by the hardware supported work sizes to see errors early

  Block maxLocalWorkSize = getMaxBlockDims(device);
  if (localWorkSize.x > maxLocalWorkSize.x ||
      localWorkSize.y > maxLocalWorkSize.y ||
      localWorkSize.z > maxLocalWorkSize.z)
    errMsgs << "  - localWorkSize must be at most " << maxLocalWorkSize
            << std::endl;

  if (localWorkSize.x * localWorkSize.y * localWorkSize.z > maxThreadsPerBlock)
    errMsgs << "  - localWorkSize total must be at most " << maxThreadsPerBlock
            << " threads/block" << std::endl;

  // globalWorkSize may (in theory) be all zero (no work), so allow.
  // Do reject an all zero localWorkSize. We need to mod or div by it.
  if (localWorkSize.x == 0 || localWorkSize.y == 0 || localWorkSize.z == 0) {
    errMsgs << "  - localWorkSize dimensions must be non-zero" << std::endl;
  } else {
    Grid maxGridWorkSize = getMaxGridDims(device);
    if (globalWorkSize.x > maxGridWorkSize.x ||
        globalWorkSize.y > maxGridWorkSize.y ||
        globalWorkSize.z > maxGridWorkSize.z)
      errMsgs << "  - globalWorkSize must be at most " << maxGridWorkSize
              << std::endl;
  }

  // On error, store error messages in output parameter, or throw.
  std::string errStr(errMsgs.str());
  if (!errStr.empty()) {
    if (errorStrings != NULL) {
      *errorStrings = errStr;
    } else {
      std::stringstream message;
      message << "setEnqueueWorkSizes(): unsupported globalWorkSize "
              << globalWorkSize << " and/or localWorkSize " << localWorkSize
              << " selected:" << std::endl
              << errStr;
      throw std::runtime_error(message.str());
    }
  }

  if (verbose_) {
    std::clog << "  grid size: " << globalWorkSize << std::endl;
    std::clog << "  block size: " << localWorkSize << std::endl;
  }

  itsGridDims = globalWorkSize;
  itsBlockDims = localWorkSize;
}
} // namespace LOFAR::Cobalt
