#ifndef LOFAR_GPUPROC_CUDA_BEAM_FORMER_KERNEL_H
#define LOFAR_GPUPROC_CUDA_BEAM_FORMER_KERNEL_H

#include <GPUProc/KernelFactory.h>
#include <GPUProc/Kernels/Kernel.h>

#include <string>
#include <vector>

namespace LOFAR::Cobalt {
class BeamFormerKernel : public CompiledKernel {
public:
  static std::string theirSourceFile;
  static std::string theirFunction;

  enum BufferType {
    INPUT_DATA,
    OUTPUT_DATA,
    STATION_INDICES,
    DELAY_INDICES,
    BEAM_FORMER_DELAYS,
    BEAM_FORMER_WEIGHTS
  };

  // Parameters that must be passed to the constructor of the
  // BeamFormerKernel class.
  struct Parameters : Kernel::Parameters {
    Parameters(std::vector<unsigned> stationIndices,
               std::vector<unsigned> delayIndices, unsigned nrDelays,
               unsigned nrChannels, unsigned nrSamplesPerChannel,
               unsigned nrTABs, unsigned nrInputBits, double subbandWidth,
               bool computeWeights = true, bool dumpBuffers = false,
               std::string dumpFilePrefix = "");

    // The input data is indexed by its station number
    // in the beamformer preprocessor, so we need to map
    // them to beam-former station numbers.
    std::vector<unsigned> stationIndices;

    // The beam-former delays are indexed by their station number
    // in the observation, so we need to map them to beam-former
    // station numbers.
    std::vector<unsigned> delayIndices;

    unsigned nrDelays;

    unsigned nrChannels;
    unsigned nrSamplesPerChannel;

    unsigned nrSAPs;
    unsigned nrTABs;
    unsigned nrInputBits;
    double subbandBandwidth;

    bool computeWeights;

    unsigned nrStationsPerPass;

    size_t bufferSize(BufferType bufferType) const;

    size_t nrInputStations() const { return delayIndices.size(); }
    size_t nrSelectedStations() const { return stationIndices.size(); }
  };

  static constexpr unsigned kNrPolarizations = 2;
  static constexpr unsigned kNrTimePerPass = 32;
  static constexpr unsigned kNrStationsPerPass = 32;
  static constexpr unsigned kNrTabsPerBlock = 32;

  BeamFormerKernel(cu::Device &device, cu::Stream &stream,
                   const cu::Module &module, const Parameters &param);

  void setWeights(const cu::DeviceMemory &weights);
  void setDelays(const cu::DeviceMemory &delays);
  void setSubbandFrequency(double subbandFrequency) {
    subbandFrequency_ = subbandFrequency;
  };

  void enqueue(const BlockID &blockId, const cu::DeviceMemory &input,
               cu::DeviceMemory &output);

private:
  double subbandFrequency_;

  cu::DeviceMemory stationIndices;
  cu::DeviceMemory delayIndices;
};

// # --------  Template specializations for KernelFactory  -------- #//

template <>
CompileDefinitions KernelFactory<BeamFormerKernel>::compileDefinitions() const;
} // namespace LOFAR::Cobalt

#endif
