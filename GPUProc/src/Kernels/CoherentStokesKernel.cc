#include "CoherentStokesKernel.h"

#include <CoInterface/BlockID.h>
#include <GPUProc/FilterBank.h>
#include <GPUProc/gpu_utils.h>

#include <cassert>
#include <complex>

namespace LOFAR::Cobalt {
std::string CoherentStokesKernel::theirSourceFile = "CoherentStokes.cu";
std::string CoherentStokesKernel::theirFunction = "coherentStokes";

CoherentStokesKernel::Parameters::Parameters()
    : Kernel::Parameters(theirFunction) {}

CoherentStokesKernel::Parameters::Parameters(
    unsigned nrChannels_, unsigned nrSamplesPerChannel_, unsigned nrTABs_,
    unsigned nrStokes_, unsigned nrBitsPerSample_, bool outputComplexVoltages_,
    unsigned timeIntegrationFactor_, bool quantizeOutput_,
    unsigned nrChannelsDelayComp_, bool dumpBuffers_,
    std::string dumpFilePrefix_)
    : Kernel::Parameters(theirFunction), nrChannels(nrChannels_),
      nrSamplesPerChannel(nrSamplesPerChannel_), nrTABs(nrTABs_),
      nrStokes(nrStokes_), nrBitsPerSample(nrBitsPerSample_),
      outputComplexVoltages(outputComplexVoltages_),
      timeIntegrationFactor(timeIntegrationFactor_),
      outputOrder(quantizeOutput_),
      nrBandpassWeights(
          nrChannelsDelayComp_ ? nrChannels_ / nrChannelsDelayComp_ : 0),
      nrChannelsDelayComp(nrChannelsDelayComp_) {
  dumpBuffers = dumpBuffers_;
  dumpFilePrefix = dumpFilePrefix_;
}

size_t
CoherentStokesKernel::Parameters::bufferSize(BufferType bufferType) const {

  switch (bufferType) {
  case CoherentStokesKernel::INPUT_DATA:
    return (size_t)nrChannels * nrSamplesPerChannel * kNrPolarizations *
           nrTABs * 2 * nrBitsPerSample / 8;

  case CoherentStokesKernel::OUTPUT_DATA:
    return (size_t)nrTABs * nrStokes * nrSamplesPerChannel /
           timeIntegrationFactor * nrChannels * nrBitsPerSample / 8;

  case CoherentStokesKernel::BAND_PASS_CORRECTION_WEIGHTS:
    return nrBandpassWeights ? (size_t)nrBandpassWeights * nrBitsPerSample / 8
                             : 1UL;

  default:
    std::stringstream message;
    message << "Invalid bufferType (" << bufferType << ")";
    throw std::runtime_error(message.str());
  }
}

CoherentStokesKernel::CoherentStokesKernel(cu::Device &device,
                                           cu::Stream &stream,
                                           const cu::Module &module,
                                           const Parameters &params)
    : CompiledKernel(device, stream,
                     cu::Function(module, theirFunction.c_str()), params),

      stream_(stream),
      warpSize_(device.getAttribute(CU_DEVICE_ATTRIBUTE_WARP_SIZE)),
      parameters_(params), bandPassCorrectionWeights(params.bufferSize(
                               BAND_PASS_CORRECTION_WEIGHTS)) {
  assert(params.timeIntegrationFactor > 0);
  assert(params.nrSamplesPerChannel % params.timeIntegrationFactor == 0);
  assert(params.nrStokes == 1 || params.nrStokes == 4);
  if (params.nrBandpassWeights) {
    assert(1 < params.nrBandpassWeights &&
           params.nrBandpassWeights < params.nrChannels);
    assert(params.nrChannels % params.nrBandpassWeights == 0);
  }

  if (params.timeIntegrationFactor < warpSize_) {
    const unsigned int block_x = warpSize_;
    const unsigned int nr_samples_per_block =
        block_x * params.timeIntegrationFactor;
    const unsigned int grid_x =
        (params.nrSamplesPerChannel + nr_samples_per_block - 1) /
        nr_samples_per_block;
    const unsigned int grid_y = params.nrChannels;
    const unsigned int grid_z = params.nrTABs;
    Grid grid(grid_x, grid_y, grid_z);
    Block block(block_x);
    setEnqueueWorkSizes(device, grid, block);
  } else {
    const unsigned int block_x = warpSize_;
    const unsigned int nr_samples_per_block = block_x;
    const unsigned int grid_x =
        (params.nrSamplesPerChannel + nr_samples_per_block - 1) /
        nr_samples_per_block;
    const unsigned int grid_y = params.nrChannels;
    const unsigned int grid_z =
        (params.nrTABs + params.nrTabsPerBlock - 1) / params.nrTabsPerBlock;
    Grid grid(grid_x, grid_y, grid_z);
    Block block(block_x);
    setEnqueueWorkSizes(device, grid, block);
  }

  nrOperations =
      (size_t)params.nrChannels * params.nrSamplesPerChannel * params.nrTABs *
      (params.nrStokes == 1 ? 8 : 20 + 2.0 / params.timeIntegrationFactor);
  nrBytesRead = (size_t)params.nrChannels * params.nrSamplesPerChannel *
                params.nrTABs * kNrPolarizations * 2 * params.nrBitsPerSample /
                8;
  nrBytesWritten = (size_t)params.nrTABs * params.nrStokes *
                   params.nrSamplesPerChannel / params.timeIntegrationFactor *
                   params.nrChannels * params.nrBitsPerSample / 8;

  if (params.nrBandpassWeights) {
    cu::HostMemory bpWeights(bandPassCorrectionWeights.size());
    const unsigned nrTaps = 16;
    FilterBank filterBank(true, nrTaps, params.nrChannelsDelayComp, KAISER);
    filterBank.computeCorrectionFactors(bpWeights, params.nrBandpassWeights,
                                        params.nrChannels);
    stream.memcpyHtoDAsync(bandPassCorrectionWeights, bpWeights,
                           bandPassCorrectionWeights.size());
  }
}

void CoherentStokesKernel::enqueue(const BlockID &blockId,
                                   const cu::DeviceMemory &input,
                                   cu::DeviceMemory &output) {
  setArg(0, output);
  setArg(1, input);
  setArg(2, bandPassCorrectionWeights);
  if (parameters_.timeIntegrationFactor > warpSize_) {
    stream_.zero(output, output.size());
  }
  Kernel::enqueue(blockId, input, output);
}

//--------  Template specializations for KernelFactory  --------//

template <>
CompileDefinitions
KernelFactory<CoherentStokesKernel>::compileDefinitions() const {
  CompileDefinitions defs =
      KernelFactoryBase::compileDefinitions(itsParameters);

  defs["NR_POLARIZATIONS"] =
      std::to_string(CoherentStokesKernel::kNrPolarizations);

  defs["NR_BITS_PER_SAMPLE"] = std::to_string(itsParameters.nrBitsPerSample);

  defs["NR_CHANNELS"] = std::to_string(itsParameters.nrChannels);
  defs["NR_SAMPLES_PER_CHANNEL"] =
      std::to_string(itsParameters.nrSamplesPerChannel);

  defs["NR_TABS"] = std::to_string(itsParameters.nrTABs);
  defs["COMPLEX_VOLTAGES"] = itsParameters.outputComplexVoltages ? "1" : "0";
  defs["NR_COHERENT_STOKES"] = std::to_string(itsParameters.nrStokes);
  defs["TIME_INTEGRATION_FACTOR"] =
      std::to_string(itsParameters.timeIntegrationFactor);
  defs["NR_TABS_PER_THREAD"] = std::to_string(itsParameters.nrTabsPerBlock);

  if (!itsParameters.outputOrder) {
    defs["OUTPUT_ORDER_SAMPLES_CHANNELS"] = "1";
  } else {
    defs["OUTPUT_ORDER_CHANNELS_SAMPLES"] = "1";
  }

  defs["NR_BANDPASS_WEIGHTS"] = std::to_string(itsParameters.nrBandpassWeights);

  return defs;
}

} // namespace LOFAR::Cobalt
