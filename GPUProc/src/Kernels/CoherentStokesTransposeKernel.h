#ifndef LOFAR_GPUPROC_CUDA_COHERENT_STOKES_TRANSPOSE_KERNEL_H
#define LOFAR_GPUPROC_CUDA_COHERENT_STOKES_TRANSPOSE_KERNEL_H

#include <GPUProc/KernelFactory.h>
#include <GPUProc/Kernels/Kernel.h>

#include <string>
#include <vector>

namespace LOFAR::Cobalt {
class CoherentStokesTransposeKernel : public CompiledKernel {
public:
  static std::string theirSourceFile;
  static std::string theirFunction;

  enum BufferType { INPUT_DATA, OUTPUT_DATA };

  // Parameters that must be passed to the constructor of the
  // BeamFormerKernel class.
  struct Parameters : Kernel::Parameters {
    Parameters(unsigned nrChannels, unsigned nrSamplesPerChannel,
               unsigned nrTABs, unsigned nrBitsPerSample,
               bool enableFFT = false, bool dumpOutput = false,
               std::string dumpFilePrefix = "");

    unsigned nrChannels;
    unsigned nrSamplesPerChannel;
    unsigned nrTABs;
    unsigned nrBitsPerSample;

    bool enableFFT;

    size_t bufferSize(BufferType bufferType) const;
  };

  CoherentStokesTransposeKernel(cu::Device &device, cu::Stream &stream,
                                const cu::Module &module,
                                const Parameters &params);

  void enqueue(const BlockID &blockId, const cu::DeviceMemory &input,
               cu::DeviceMemory &output);

  static constexpr unsigned kNrPolarizations = 2;
};

// # --------  Template specializations for KernelFactory  -------- #//

template <>
CompileDefinitions
KernelFactory<CoherentStokesTransposeKernel>::compileDefinitions() const;

template <>
CompileFlags KernelFactory<CoherentStokesTransposeKernel>::compileFlags() const;
} // namespace LOFAR::Cobalt

#endif
