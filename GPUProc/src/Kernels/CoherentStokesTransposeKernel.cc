#include "CoherentStokesTransposeKernel.h"

#include <CoInterface/BlockID.h>
#include <GPUProc/gpu_utils.h>

#include <cassert>
#include <complex>
#include <sstream>

#include <mathdx-helper.hpp>
namespace {
/*
 * Returns `value' rounded up to `alignment'.
 * T must be an integral type.
 */
template <typename T> inline static T align(T value, size_t alignment) {
  return (value + alignment - 1) / alignment * alignment;
}

} // namespace

namespace LOFAR::Cobalt {
std::string CoherentStokesTransposeKernel::theirSourceFile =
    "CoherentStokesTranspose.cu";
std::string CoherentStokesTransposeKernel::theirFunction =
    "coherentStokesTranspose";

CoherentStokesTransposeKernel::Parameters::Parameters(
    unsigned nrChannels_, unsigned nrSamplesPerChannel_, unsigned nrTABs_,
    unsigned nrBitsPerSample_, bool enableFFT_, bool dumpBuffers_,
    std::string dumpFilePrefix_)
    : Kernel::Parameters(theirFunction), nrChannels(nrChannels_),
      nrSamplesPerChannel(nrSamplesPerChannel_), nrTABs(nrTABs_),
      nrBitsPerSample(nrBitsPerSample_), enableFFT(enableFFT_) {
  dumpBuffers = dumpBuffers_;
  dumpFilePrefix = dumpFilePrefix_;
}

size_t CoherentStokesTransposeKernel::Parameters::bufferSize(
    BufferType bufferType) const {
  switch (bufferType) {
  case CoherentStokesTransposeKernel::INPUT_DATA:
  case CoherentStokesTransposeKernel::OUTPUT_DATA:
    return (size_t)nrChannels * nrSamplesPerChannel * kNrPolarizations *
           nrTABs * 2 * nrBitsPerSample / 8;
  default:
    std::stringstream message;
    message << "Invalid bufferType (" << bufferType << ")";
    throw std::runtime_error(message.str());
  }
}

CoherentStokesTransposeKernel::CoherentStokesTransposeKernel(
    cu::Device &device, cu::Stream &stream, const cu::Module &module,
    const Parameters &params)
    : CompiledKernel(device, stream,
                     cu::Function(module, theirFunction.c_str()), params) {
  assert(params.nrSamplesPerChannel > 0);
  assert(params.nrTABs > 0);

  Block block{256};

  if (params.enableFFT) {
    const dim3 fft_block_dim =
        mathdx::get_global_from_module<dim3>(module, "fft_block_dim");
    block = Block(fft_block_dim);
  }

  setEnqueueWorkSizes(device, Grid(params.nrTABs, params.nrSamplesPerChannel),
                      block);

  nrOperations = 0;
  nrBytesRead = nrBytesWritten =
      (size_t)params.nrTABs * kNrPolarizations * params.nrChannels *
      params.nrSamplesPerChannel * sizeof(std::complex<float>);
}

void CoherentStokesTransposeKernel::enqueue(const BlockID &blockId,
                                            const cu::DeviceMemory &input,
                                            cu::DeviceMemory &output) {
  setArg(0, output);
  setArg(1, input);
  Kernel::enqueue(blockId, input, output);
}

//--------  Template specializations for KernelFactory  --------//

template <>
CompileDefinitions
KernelFactory<CoherentStokesTransposeKernel>::compileDefinitions() const {
  CompileDefinitions defs =
      KernelFactoryBase::compileDefinitions(itsParameters);

  defs["NR_POLARIZATIONS"] =
      std::to_string(CoherentStokesTransposeKernel::kNrPolarizations);

  defs["NR_CHANNELS"] = std::to_string(itsParameters.nrChannels);
  defs["NR_SAMPLES_PER_CHANNEL"] =
      std::to_string(itsParameters.nrSamplesPerChannel);

  defs["NR_TABS"] = std::to_string(itsParameters.nrTABs);

  defs["NR_BITS_PER_SAMPLE"] = std::to_string(itsParameters.nrBitsPerSample);

  if (itsParameters.enableFFT) {
    defs["ENABLE_FFT"] = std::to_string(1);
  }

  return defs;
}

template <>
CompileFlags
KernelFactory<CoherentStokesTransposeKernel>::compileFlags() const {
  CompileFlags flags = KernelFactoryBase::compileFlags(itsParameters);

  if (itsParameters.enableFFT) {
    const std::vector<std::string> include_dirs = mathdx::get_include_dirs();
    std::copy(include_dirs.begin(), include_dirs.end(),
              std::inserter(flags, flags.end()));
    flags.insert("--device-as-default-execution-space");
  }

  return flags;
}

} // namespace LOFAR::Cobalt
