#ifndef LOFAR_GPUPROC_CUDA_BEAM_FORMER_CCG_KERNEL_H
#define LOFAR_GPUPROC_CUDA_BEAM_FORMER_CCG_KERNEL_H

#include <ccglib/ccglib.hpp>

#include <GPUProc/KernelFactory.h>
#include <GPUProc/Kernels/Kernel.h>

#include <string>
#include <vector>
namespace LOFAR::Cobalt {
class BeamFormerCCGKernel : public Kernel {
public:
  enum BufferType {
    INPUT_DATA,
    INPUT_DATA_TRANSPOSED,
    OUTPUT_DATA,
    BEAM_FORMER_WEIGHTS,
    BEAM_FORMER_WEIGHTS_TRANSPOSED
  };

  // Parameters that must be passed to the constructor of the
  // BeamFormerCCGKernel class.
  struct Parameters : Kernel::Parameters {
    Parameters(unsigned nrStations, unsigned nrChannels,
               unsigned nrSamplesPerChannel, unsigned nrTABs,
               unsigned nrPolarizations, unsigned nrInputBits,
               bool dumpBuffers = false, std::string dumpFilePrefix = "");

    unsigned nrStations;

    unsigned nrChannels;
    unsigned nrSamplesPerChannel;
    unsigned nrPolarizations;

    unsigned nrTABs;
    unsigned nrInputBits;

    dim3 dimensions;

    size_t bufferSize(BufferType bufferType) const;
  };

  BeamFormerCCGKernel(cu::Device &device, cu::Stream &stream,
                      const Parameters &param);

  void setWeights(const cu::DeviceMemory &weights);

  void enqueue(const BlockID &blockId, const cu::DeviceMemory &input,
               cu::DeviceMemory &output);

private:
  void launch(const BlockID &blockId, const cu::DeviceMemory &input,
              cu::DeviceMemory &output);

  Parameters parameters;

  std::shared_ptr<cu::DeviceMemory> weights_transposed_;
  std::shared_ptr<cu::DeviceMemory> input_transposed_;
  std::unique_ptr<ccglib::transpose::Transpose> transpose_weights_;
  std::unique_ptr<ccglib::transpose::Transpose> transpose_input_;
  std::unique_ptr<ccglib::mma::GEMM> gemm_;
};

} // namespace LOFAR::Cobalt

#endif
