#ifndef LOFAR_GPUPROC_CUDA_COHERENT_STOKES_KERNEL_H
#define LOFAR_GPUPROC_CUDA_COHERENT_STOKES_KERNEL_H

#include <GPUProc/KernelFactory.h>
#include <GPUProc/Kernels/Kernel.h>

#include <string>
#include <vector>

namespace LOFAR::Cobalt {

class CoherentStokesKernel : public CompiledKernel {
public:
  static std::string theirSourceFile;
  static std::string theirFunction;

  static constexpr unsigned int kWarpSize = 32;

  enum BufferType { INPUT_DATA, OUTPUT_DATA, BAND_PASS_CORRECTION_WEIGHTS };

  // Parameters that must be passed to the constructor of the
  // CoherentStokesKernel class.
  struct Parameters : Kernel::Parameters {
    Parameters();
    Parameters(unsigned nrChannels, unsigned nrSamplesPerChannel,
               unsigned nrTABs, unsigned nrStokes, unsigned nrBitsPerSample,
               bool outputComplexVoltages, unsigned timeIntegrationFactor,
               bool quantizeOutput, // needed to set output ordering
               unsigned nrChannelsDelayComp =
                   0, // if >0, apply bandpass correction due to the input PPF
                      // stage, will be >0 only if the input PPF is applied
               bool dumpBuffers = false, std::string dumpFilePrefix = "");

    unsigned nrChannels;
    unsigned nrSamplesPerChannel;

    unsigned nrTABs;
    unsigned nrStokes;
    unsigned nrBitsPerSample;
    bool outputComplexVoltages;
    unsigned timeIntegrationFactor;
    unsigned nrTabsPerBlock = 8;

    bool outputOrder; // 1: channels/samples 0: samples/channels

    // Weights (subset of nrChannels) that repeats over the full bandwidth
    // This should be a divisor of nrChannels
    // i.e., integer x nrBandpassWeights=nrChannels
    unsigned nrBandpassWeights; // if > 1, apply bandpass correction
    unsigned nrChannelsDelayComp;

    size_t bufferSize(BufferType bufferType) const;
  };

  static constexpr unsigned kNrPolarizations = 2;

  CoherentStokesKernel(cu::Device &device, cu::Stream &stream,
                       const cu::Module &module, const Parameters &param);

  void enqueue(const BlockID &blockId, const cu::DeviceMemory &input,
               cu::DeviceMemory &output);

private:
  cu::Stream &stream_;
  unsigned int warpSize_;
  Parameters parameters_;

  // The weights to correct the bandpass
  cu::DeviceMemory bandPassCorrectionWeights;
};

// # --------  Template specializations for KernelFactory  -------- #//

template <>
CompileDefinitions
KernelFactory<CoherentStokesKernel>::compileDefinitions() const;
} // namespace LOFAR::Cobalt

#endif
