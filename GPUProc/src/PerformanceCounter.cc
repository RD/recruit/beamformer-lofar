#include <iomanip>
#include <iostream>

#include "PerformanceCounter.h"

namespace LOFAR::Cobalt {
/*
 * The performance is measured by posting start and stop events onto the stream.
 *
 * The caller calls recordStart() and recordStop() to do so. The time span
 * between events can be measured only after they have occurred (after the
 * stream synchronises with the CPU). To not depend on stream synchronisation
 * here, we simply query the time between stop and start (logTime()) when we a)
 * start a new measurement (recordStart), or b) on destruction
 */
PerformanceCounter::PerformanceCounter(const std::string &name) : name(name) {}

PerformanceCounter::~PerformanceCounter() {
  // record any lingering information
  logTime();

  if (print) {
    std::clog << "(" << std::setw(30) << name << "): " << stats << std::endl;
  }
}

void PerformanceCounter::recordStart(cu::Stream &stream) {
  // record any lingering information
  logTime();

  events.push_back(std::pair<cu::Event, cu::Event>());

  stream.record(events.back().first);
}

void PerformanceCounter::recordStop(cu::Stream &stream) {
  stream.record(events.back().second);
}

void PerformanceCounter::logTime() {
  while (events.size() > 0) {

    // get the difference between start and stop. push it on the stats object
    try {
      cu::Event &start = events.front().first;
      cu::Event &stop = events.front().second;
      stats.push(stop.elapsedTime(start));
      events.pop_front();
    } catch (cu::Error) {
      // catch errors in case the event was not posted -- the current interface
      // has no easy way to check beforehand.
    }
  }
}

bool PerformanceCounter::print = true;

} // namespace LOFAR::Cobalt
