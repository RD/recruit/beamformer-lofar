#include "KernelFactory.h"

#include <string>

namespace LOFAR::Cobalt {
KernelFactoryBase::~KernelFactoryBase() {}

CompileDefinitions KernelFactoryBase::compileDefinitions(
    const Kernel::Parameters & /*param*/) const {
  CompileDefinitions defs;
  return defs;
}

CompileFlags
KernelFactoryBase::compileFlags(const Kernel::Parameters & /*param*/) const {
  CompileFlags flags;
  return flags;
}

} // namespace LOFAR::Cobalt
