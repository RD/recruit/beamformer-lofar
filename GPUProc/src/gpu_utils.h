#ifndef LOFAR_GPUPROC_CUDA_GPU_UTILS_H
#define LOFAR_GPUPROC_CUDA_GPU_UTILS_H

#include <map>
#include <ostream>
#include <set>
#include <string>
#include <vector>

#include <cudawrappers/cu.hpp>

namespace LOFAR::Cobalt {
// Map for storing compile definitions that will be passed to the GPU kernel
// compiler on the command line. The key is the name of the preprocessor
// variable, a value should only be assigned if the preprocessor variable
// needs to have a value.
struct CompileDefinitions : std::map<std::string, std::string> {};

// Return default compile definitions.
CompileDefinitions defaultCompileDefinitions();

// Set for storing compile flags that will be passed to the GPU kernel
// compiler on the command line. Flags generally don't have an associated
// value; if they do the value should become part of the flag in this set.
struct CompileFlags : std::set<std::string> {};

// Return default compile flags.
CompileFlags defaultCompileFlags();

// Compile \a srcFilename and return the PTX code as string.
// \par srcFilename Name of the file containing the source code to be
//      compiled to PTX. If \a srcFilename is a relative path and if the
//      environment variable \c LOFARROOT is set, then prefix \a srcFilename
//      with the path \c $LOFARROOT/share/gpu/kernels.
// \par flags Set of flags that need to be passed to the compiler on the
//      command line.
// \par definitions Map of key/value pairs containing the preprocessor
//      variables that need to be passed to the compiler on the command
//      line.
// \par device Devices to compile for.
std::string createPTX(const cu::Device &device, std::string srcFilename,
                      CompileDefinitions definitions = CompileDefinitions(),
                      CompileFlags flags = CompileFlags());

// Create a Module from a PTX (string).
// \par context The context that the Module should be associated with.
// \par srcFilename Name of the
cu::Module createModule(const cu::Context &context,
                        const std::string &srcFilename, const std::string &ptx);

// Dump the contents of a device memory buffer as raw binary data to file.
// \par deviceMemory Device memory buffer to be dumped.
// \par dumpFile Name of the dump file.
// \warning The underlying cu::Stream must be synchronized, in order to
// dump a device buffer. This may have a serious impact on performance.
void dumpBuffer(const cu::DeviceMemory &deviceMemory,
                const std::string &dumpFile);

// Struct representing a CUDA Grid, which is similar to the @c dim3 type
// in the CUDA Runtime API.
struct Grid {
  Grid(unsigned int x_ = 1, unsigned int y_ = 1, unsigned int z_ = 1);
  unsigned int x;
  unsigned int y;
  unsigned int z;
  friend std::ostream &operator<<(std::ostream &os, const Grid &grid);
  operator dim3() { return dim3(x, y, z); }
};

// Struct representing a CUDA Block, which is similar to the @c dim3 type
// in the CUDA Runtime API.
//
// @invariant x > 0, y > 0, z > 0
struct Block {
  Block(unsigned int x_ = 1, unsigned int y_ = 1, unsigned int z_ = 1);
  Block(const dim3 &block) : x(block.x), y(block.y), z(block.z) {}
  unsigned int x;
  unsigned int y;
  unsigned int z;
  friend std::ostream &operator<<(std::ostream &os, const Block &block);
  operator dim3() { return dim3(x, y, z); }
};

// Return the maximum dimensions of a block of threads.
struct Block getMaxBlockDims(cu::Device &device);

// Return the maximum dimensions of a grid of blocks.
struct Grid getMaxGridDims(cu::Device &device);
} // namespace LOFAR::Cobalt

#endif
