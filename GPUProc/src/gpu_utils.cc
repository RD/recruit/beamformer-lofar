#include <GPUProc/gpu_utils.h>

#include <chrono>
#include <cstdio>  // for popen(), pclose(), fgets()
#include <cstdlib> // for getenv()
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "cuda_config.h"

#include <cudawrappers/nvrtc.hpp>
#include <iterator>

namespace LOFAR::Cobalt {

namespace {

// Return the highest compute target supported by the given device
CUjit_target computeTarget(const cu::Device &device) {
  unsigned major =
      device.getAttribute<CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR>();
  unsigned minor =
      device.getAttribute<CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR>();

  return (CUjit_target)(major * 10 + minor);
}

// Return the highest compute target supported by all the given devices
CUjit_target computeTarget(const std::vector<cu::Device> &devices) {
  CUjit_target minTarget = CU_TARGET_COMPUTE_70;

  for (std::vector<cu::Device>::const_iterator i = devices.begin();
       i != devices.end(); ++i) {
    CUjit_target target = computeTarget(*i);

    if (i == devices.begin() || target < minTarget)
      minTarget = target;
  }

  return minTarget;
}

// Translate a compute target to a virtual architecture (= the version
// the .cu file is written in).
std::string get_virtarch(CUjit_target target) {
  return (std::stringstream() << "compute_" << target).str();
}

// Translate a compute target to a GPU architecture (= the instruction
// set supported by the actual GPU).
std::string get_gpuarch(CUjit_target target) {
  return (std::stringstream() << "sm_" << target).str();
}

std::string lofarRoot() {
  // Prefer copy over racy static var or mutex.
  const char *env = std::getenv("LOFARROOT");
  return env ? std::string(env) : std::string();
}

std::string prefixPath() { return lofarRoot() + "/share/gpu/kernels"; }

std::string includePath() { return lofarRoot() + "/include"; }

std::ostream &operator<<(std::ostream &os, const CompileDefinitions &defs) {
  CompileDefinitions::const_iterator it;
  for (it = defs.begin(); it != defs.end(); ++it) {
    os << " -D" << it->first;
    if (!it->second.empty()) {
      os << "=" << it->second;
    }
  }
  return os;
}

std::ostream &operator<<(std::ostream &os, const CompileFlags &flags) {
  CompileFlags::const_iterator it;
  for (it = flags.begin(); it != flags.end(); ++it) {
    os << " " << *it;
  }
  return os;
}

std::string doCreatePTX(const std::string &source, const CompileFlags &flags,
                        const CompileDefinitions &defs) {
#if defined(RTC_VERBOSE)
  std::clog << "Starting runtime compilation:\n\t" << source << flags << defs
            << std::endl;
#endif

  // Append compile flags and compile definitions to get compile options
  std::stringstream options_stream;
  options_stream << flags;
  options_stream << defs;
  options_stream << " -rdc=true";
  options_stream << " -I" << CUDA_TOOLKIT_INCLUDE_DIRECTORIES;

  // Create vector of compile options with strings
  std::vector<std::string> options_vector{
      std::istream_iterator<std::string>{options_stream},
      std::istream_iterator<std::string>{}};

  // Create array of compile options with character arrays
  std::vector<const char *> options_c_char;
  std::transform(options_vector.begin(), options_vector.end(),
                 std::back_inserter(options_c_char),
                 [](const std::string &option) { return option.c_str(); });

  // Open source file
  std::ifstream input_stream;
  input_stream.open(source, std::ifstream::in);
  if (input_stream.fail()) {
    std::stringstream message;
    message << "Failed to open file:\n\t" << source;
    throw std::runtime_error(message.str());
  }
  std::ostringstream file_contents;
  file_contents << input_stream.rdbuf();
  input_stream.close();

  // Try to compile source into NVRTC program
  nvrtc::Program program(file_contents.str(), source.c_str());
  try {
    program.compile(options_vector);
  } catch (nvrtc::Error &error) {
    // Print compilation log (if any)
    std::string log = program.getLog();
    if (log.size()) // ignore logs with only trailing NULL
    {
      std::clog << log << std::endl;
    }

    throw error;
  }

  return program.getPTX();
}

} // namespace

CompileDefinitions defaultCompileDefinitions() {
  CompileDefinitions defs;
  return defs;
}

CompileFlags defaultCompileFlags() {
  CompileFlags flags;

  // For now, keep optimisations the same to detect changes in
  // output with reference.
  flags.insert("--restrict");

  // Enable source-file mappings to aid profiling
  // This flag slightly increases compilation times and size
  // of the generated PTX, but does not affect kernel performance.
  flags.insert("-lineinfo");

  // Make use of fast math library.
  flags.insert("--use_fast_math");

  return flags;
}

std::string createPTX(const cu::Device &device, std::string srcFilename,
                      CompileDefinitions definitions, CompileFlags flags) {
  // The CUDA code is assumed to be written for the architecture of the
  // oldest device.
  flags.insert((std::stringstream()
                << "--gpu-architecture=" << get_virtarch(computeTarget(device)))
                   .str());

  // Add default definitions and flags
  CompileDefinitions defaultDefinitions(defaultCompileDefinitions());
  definitions.insert(defaultDefinitions.begin(), defaultDefinitions.end());
  CompileFlags defaultFlags(defaultCompileFlags());
  flags.insert(defaultFlags.begin(), defaultFlags.end());

  // Prefix the CUDA kernel filename if it's a relative path.
  if (!srcFilename.empty() && srcFilename[0] != '/') {
    srcFilename = prefixPath() + "/" + srcFilename;
  }

  return doCreatePTX(srcFilename, flags, definitions);
}

cu::Module createModule(const cu::Context &context,
                        const std::string &srcFilename,
                        const std::string &ptx) {
  const unsigned int BUILD_MAX_LOG_SIZE = 4095;
  /*
   * JIT compilation options.
   * Note: need to pass a void* with option vals. Preferably, do not alloc
   * dyn (mem leaks on exc).
   * Instead, use local vars for small variables and vector<char> xxx;
   * passing &xxx[0] for output c-strings.
   */

  cu::Module::optionmap_t options;

  // input and output var for JIT compiler
  size_t infoLogSize = BUILD_MAX_LOG_SIZE + 1;
  // idem (hence not the a single var or const)
  size_t errorLogSize = BUILD_MAX_LOG_SIZE + 1;

  std::vector<char> infoLog(infoLogSize);
  options[CU_JIT_INFO_LOG_BUFFER] = &infoLog[0];
  options[CU_JIT_INFO_LOG_BUFFER_SIZE_BYTES] =
      reinterpret_cast<void *>(infoLogSize);

  std::vector<char> errorLog(errorLogSize);
  options[CU_JIT_ERROR_LOG_BUFFER] = &errorLog[0];
  options[CU_JIT_ERROR_LOG_BUFFER_SIZE_BYTES] =
      reinterpret_cast<void *>(errorLogSize);

  float jitWallTime = 0;
  try {
    auto start = std::chrono::steady_clock::now();
    cu::Module module(reinterpret_cast<const void *>(ptx.data()), options);
    auto end = std::chrono::steady_clock::now();
    jitWallTime =
        std::chrono::duration_cast<std::chrono::milliseconds>(end - start)
            .count();

    // TODO: check what the ptx compiler prints. Don't print bogus. See if
    // infoLogSize indeed is set to 0 if all cool.
    // TODO: maybe retry if buffer len exhausted, esp for errors
    if (infoLogSize > infoLog.size()) {
      // zero-term log and guard against bogus JIT opt val output
      infoLogSize = infoLog.size();
    }
    infoLog[infoLogSize - 1] = '\0';
#if defined(RTC_VERBOSE)
    std::clog << "Build info for '" << srcFilename
              << "' (build time: " << jitWallTime << " ms):" << std::endl
              << &infoLog[0] << std::endl;
#endif

    return module;
  } catch (std::runtime_error &exc) {
    if (errorLogSize > errorLog.size()) { // idem
      errorLogSize = errorLog.size();
    }
    errorLog[errorLogSize - 1] = '\0';
    std::cerr << "Build errors for '" << srcFilename
              << "' (build time: " << jitWallTime << " ms):" << std::endl
              << &errorLog[0] << std::endl;
    throw;
  }
}

void dumpBuffer(const cu::DeviceMemory &deviceMemory,
                const std::string &dumpFile) {
  std::clog << "Dumping device memory to file: " << dumpFile << std::endl;
  cu::HostMemory hostMemory(deviceMemory.size());
  cu::memcpyDtoH(hostMemory, deviceMemory, deviceMemory.size());
  std::ofstream ofs(dumpFile.c_str(), std::ios::binary);
  ofs.write(static_cast<char *>(hostMemory), hostMemory.size());
}

Grid::Grid(unsigned int x_, unsigned int y_, unsigned int z_)
    : x(x_), y(y_), z(z_) {}

std::ostream &operator<<(std::ostream &os, const Grid &grid) {
  os << "[" << grid.x << ", " << grid.y << ", " << grid.z << "]";
  return os;
}

Block::Block(unsigned int x_, unsigned int y_, unsigned int z_)
    : x(x_), y(y_), z(z_) {
  // Cannot enforce this as an obj invariant (x, y, z public on purpose),
  // but intended to trigger bugs early.
  if (x == 0 || y == 0 || z == 0) {
    std::stringstream message;
    message << "Block(): block dims must be non-zero: " << x << " " << y << " "
            << z;
    throw std::runtime_error(message.str());
  }
}

std::ostream &operator<<(std::ostream &os, const Block &block) {
  os << "[" << block.x << ", " << block.y << ", " << block.z << "]";
  return os;
}

struct Block getMaxBlockDims(cu::Device &device) {
  Block block;
  block.x = (unsigned)device.getAttribute(CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X);
  block.y = (unsigned)device.getAttribute(CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y);
  block.z = (unsigned)device.getAttribute(CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z);
  return block;
}

struct Grid getMaxGridDims(cu::Device &device) {
  Grid grid;
  grid.x = (unsigned)device.getAttribute(CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_X);
  grid.y = (unsigned)device.getAttribute(CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Y);
  grid.z = (unsigned)device.getAttribute(CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Z);
  return grid;
}

} // namespace LOFAR::Cobalt
