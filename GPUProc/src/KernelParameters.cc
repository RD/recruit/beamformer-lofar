#include "KernelParameters.h"

namespace LOFAR::Cobalt {

KernelParameters::Observation::Observation(
    const ObservationSettings &obsSettings)
    : observationID(obsSettings.observationID), nrSAPs(obsSettings.SAPs.size()),
      nrStations(obsSettings.antennaFieldNames.size()),
      nrBitsPerSample(obsSettings.nrBitsPerSample),
      blockSize(obsSettings.blockSize),
      subbandWidth(obsSettings.subbandWidth()), subbands(obsSettings.subbands),
      clockMHz(obsSettings.clockMHz) {
  ASSERT(obsSettings.antennaFieldNames.size() ==
         obsSettings.antennaFields.size());
}

KernelParameters::Preprocessor::Preprocessor(
    const std::vector<ObservationSettings::AntennaFieldName>
        &obsAntennaFieldNames,
    const std::vector<ObservationSettings::AntennaFieldName>
        &bfAntennaFieldNames,
    bool delayCompensationEnabled, bool bandPassCorrectionEnabled,
    unsigned nrDelayCompensationChannels, bool inputPPF)
    : obsStationIndices(ObservationSettings::AntennaFieldName::indices(
          bfAntennaFieldNames, obsAntennaFieldNames)),
      delayCompensationEnabled(delayCompensationEnabled),
      bandPassCorrectionEnabled(bandPassCorrectionEnabled),
      nrDelayCompensationChannels(nrDelayCompensationChannels),
      inputPPF(inputPPF) {}

KernelParameters::BeamFormer::BeamFormer(
    const std::vector<ObservationSettings::AntennaFieldName>
        &obsAntennaFieldNames,
    const std::vector<ObservationSettings::AntennaFieldName>
        &preAntennaFieldNames,
    const ObservationSettings::BeamFormer::Pipeline &bfSettings)
    : preStationIndices(ObservationSettings::AntennaFieldName::indices(
          bfSettings.antennaFieldNames, preAntennaFieldNames)),
      obsStationIndices(ObservationSettings::AntennaFieldName::indices(
          bfSettings.antennaFieldNames, obsAntennaFieldNames)),
      doFlysEye(bfSettings.doFlysEye), nrSAPs(bfSettings.SAPs.size()),
      maxNrCoherentTABsPerSAP(bfSettings.maxNrCoherentTABsPerSAP()),
      maxNrIncoherentTABsPerSAP(bfSettings.maxNrIncoherentTABsPerSAP()),
      coherentSettings(bfSettings.coherentSettings),
      incoherentSettings(bfSettings.incoherentSettings) {
  ASSERTSTR(preStationIndices.size() <= obsStationIndices.size(),
            "preStationIndices.size() <= ObsStationIndices.size()");
}

} // end namespace Cobalt
} // end namespace LOFAR
