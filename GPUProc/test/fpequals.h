#ifndef LOFAR_GPUPROC_FPEQUALS_H
#define LOFAR_GPUPROC_FPEQUALS_H

#include <catch2/matchers/catch_matchers_floating_point.hpp>
#include <cuda_fp16.h>

namespace LOFAR::Cobalt {

template <typename T> void fpEquals(T x, T y, float epsilon) {
  REQUIRE_THAT(y, Catch::Matchers::WithinAbs(x, epsilon) ||
                      Catch::Matchers::WithinRel(x, epsilon * 100));
}

template <typename T>
void fpEquals(std::complex<T> x, std::complex<T> y, float epsilon) {
  fpEquals(x.real(), y.real(), epsilon);
  fpEquals(x.imag(), y.imag(), epsilon);
}

template <typename T> float getEpsilon() {
  if constexpr (std::is_same_v<T, half>) {
    return 0.000976562;
  }
  return std::numeric_limits<T>::epsilon();
}

} // namespace LOFAR::Cobalt

#endif
