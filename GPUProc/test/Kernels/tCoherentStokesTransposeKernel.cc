#include <CoInterface/BlockID.h>
#include <CoInterface/Span.h>
#include <GPUProc/Kernels/CoherentStokesTransposeKernel.h>

#include <complex>

#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>
#include <cuda_fp16.h>
#include <fftw3.h>

#include "../TestUtil.h"
#include "../fpequals.h"

using namespace LOFAR::Cobalt;

template <typename InputType> class TestImpl : public TestFixture {
public:
  TestImpl() : TestFixture(){};

  void init(unsigned nr_channels, unsigned nr_samples_per_channel,
            unsigned nr_tabs, bool enable_fft) {
    nr_channels_ = nr_channels;
    nr_samples_per_channel_ = nr_samples_per_channel;
    nr_tabs_ = nr_tabs;
    enable_fft_ = enable_fft;

    shape_input_ = {nr_channels_, nr_samples_per_channel_, nr_tabs_,
                    kNrPolarizations};
    shape_output_ = {nr_tabs_, kNrPolarizations, nr_samples_per_channel_,
                     nr_channels_};

    const size_t nr_input_bits = sizeof(InputType) * 8;

    CoherentStokesTransposeKernel::Parameters parameters(
        nr_channels_,           // nrChannels
        nr_samples_per_channel, // nrSamplesPerChannel
        nr_tabs,                // nrTABs
        nr_input_bits,          // nrBitsPerSample
        enable_fft              // enableFFT
    );

    host_input_.reset(new cu::HostMemory(
        parameters.bufferSize(CoherentStokesTransposeKernel::INPUT_DATA)));
    host_output_.reset(new cu::HostMemory(
        parameters.bufferSize(CoherentStokesTransposeKernel::OUTPUT_DATA)));

    // The factory needs to be kept alive during the lifetime of this class
    factory_.reset(
        new KernelFactory<CoherentStokesTransposeKernel>(device_, parameters));

    kernel_.reset(factory_->create(context_, stream_));
  }

  void runTestZero() {
    auto input_data = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_input_), shape_input_);
    auto output_data = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_output_), shape_output_);
    std::complex<InputType> one(1, 1);
    std::complex<InputType> zero(1, 1);
    input_data.fill(zero);
    output_data.fill(one);

    runKernel();

    for (unsigned tab = 0; tab < nr_tabs_; tab++) {
      for (unsigned pol = 0; pol < kNrPolarizations; pol++) {
        for (unsigned sample = 0; sample < nr_samples_per_channel_; sample++) {
          for (unsigned channel = 0; channel < nr_channels_; channel++) {
            REQUIRE(output_data(tab, pol, sample, channel) == zero);
          }
        }
      }
    }
  }

  void runTestOne() {
    auto input_data = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_input_), shape_input_);
    auto output_data = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_output_), shape_output_);
    std::complex<InputType> one(1, 1);
    std::complex<InputType> zero(1, 1);
    input_data.fill(one);
    output_data.fill(zero);

    runKernel();

    for (unsigned tab = 0; tab < nr_tabs_; tab++) {
      for (unsigned pol = 0; pol < kNrPolarizations; pol++) {
        for (unsigned sample = 0; sample < nr_samples_per_channel_; sample++) {
          for (unsigned channel = 0; channel < nr_channels_; channel++) {
            REQUIRE(output_data(tab, pol, sample, channel) == one);
          }
        }
      }
    }
  }

  void runTestDifferent() {
    auto input_data = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_input_), shape_input_);
    auto output_data = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_output_), shape_output_);

    for (unsigned channel = 0; channel < nr_channels_; channel++) {
      for (unsigned sample = 0; sample < nr_samples_per_channel_; sample++) {
        for (unsigned tab = 0; tab < nr_tabs_; tab++) {
          for (unsigned pol = 0; pol < kNrPolarizations; pol++) {
            const InputType value =
                1 + 1000 * channel + 100 * sample + 10 * tab + pol;
            input_data(channel, sample, tab, pol) =
                std::complex<InputType>(value, -value);
          }
        }
      }
    }

    output_data.fill(0);

    runKernel();

    for (unsigned tab = 0; tab < nr_tabs_; tab++) {
      for (unsigned pol = 0; pol < kNrPolarizations; pol++) {
        for (unsigned sample = 0; sample < nr_samples_per_channel_; sample++) {
          for (unsigned channel = 0; channel < nr_channels_; channel++) {
            REQUIRE(output_data(tab, pol, sample, channel) ==
                    input_data(channel, sample, tab, pol));
          }
        }
      }
    }
  }

  void runTestFFT() {
    auto input_data = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_input_), shape_input_);
    auto output_data = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_output_), shape_output_);
    output_data.fill(0);

    for (unsigned channel = 0; channel < nr_channels_; channel++) {
      for (unsigned sample = 0; sample < nr_samples_per_channel_; sample++) {
        for (unsigned tab = 0; tab < nr_tabs_; tab++) {
          for (unsigned pol = 0; pol < kNrPolarizations; pol++) {
            const InputType value =
                (1 + 1000 * channel + 100 * sample + 10 * tab + pol);
            input_data(channel, sample, tab, pol) =
                std::complex<InputType>(value, -value);
          }
        }
      }
    }

    runKernel();

    xt::xtensor<std::complex<InputType>, 4> output_ref =
        xt::zeros<std::complex<InputType>>(shape_output_);

    xt::xtensor<std::complex<InputType>, 4> input_transposed =
        xt::transpose(input_data, {2, 3, 1, 0});

    runFFT(input_transposed.data(), output_ref.data());

    const float epsilon = getEpsilon<InputType>() * nr_channels_;

    for (unsigned tab = 0; tab < nr_tabs_; tab++) {
      for (unsigned pol = 0; pol < kNrPolarizations; pol++) {
        for (unsigned sample = 0; sample < nr_samples_per_channel_; sample++) {
          for (unsigned channel = 0; channel < nr_channels_; channel++) {
            fpEquals(output_data(tab, pol, sample, channel),
                     output_ref(tab, pol, sample, channel), epsilon);
          }
        }
      }
    }
  }

private:
  void runKernel() {
    cu::DeviceMemory dev_output(host_output_->size());
    stream_.memcpyHtoDAsync(dev_output, *host_output_, host_output_->size());

    cu::DeviceMemory dev_input(host_input_->size());
    stream_.memcpyHtoDAsync(dev_input, *host_input_, host_input_->size());

    BlockID block_id;

    kernel_->enqueue(block_id, dev_input, dev_output);

    stream_.memcpyDtoHAsync(*host_output_, dev_output, host_output_->size());

    stream_.synchronize();
  }

  void runFFT(std::complex<InputType> *input, std::complex<InputType> *output) {
    fftwf_complex *in = reinterpret_cast<fftwf_complex *>(input);
    fftwf_complex *out = reinterpret_cast<fftwf_complex *>(output);
    int rank = 1;
    int n[] = {static_cast<int>(nr_channels_)};
    int howmany = nr_tabs_ * kNrPolarizations * nr_samples_per_channel_;
    int idist = n[0];
    int odist = n[0];
    int istride = 1;
    int ostride = 1;
    int *inembed = n;
    int *onembed = n;
    int sign = FFTW_BACKWARD;
    int flags = FFTW_ESTIMATE;
    fftwf_plan plan =
        fftwf_plan_many_dft(rank, n, howmany, in, inembed, istride, idist, out,
                            onembed, ostride, odist, sign, flags);
    fftwf_execute(plan);
    fftwf_destroy_plan(plan);

    // FFT shift
    for (size_t i = 1; i < howmany * n[0]; i += 2) {
      output[i] *= -1;
    }
  }

  const unsigned kNrPolarizations = 2;

  unsigned nr_channels_;
  unsigned nr_samples_per_channel_;
  unsigned nr_tabs_;
  bool enable_fft_;

  std::unique_ptr<KernelFactory<CoherentStokesTransposeKernel>> factory_;
  std::unique_ptr<CoherentStokesTransposeKernel> kernel_;

  std::array<size_t, 4> shape_input_;
  std::array<size_t, 4> shape_output_;

  std::unique_ptr<cu::HostMemory> host_input_;
  std::unique_ptr<cu::HostMemory> host_output_;
};

using TestTypes = std::tuple<float>;
TEMPLATE_LIST_TEST_CASE_METHOD(TestImpl, "CoherentStokesTransposeKernel",
                               "[transpose]", TestTypes) {

  if (TestImpl<TestType>::skip_) {
    return;
  }

  const unsigned kNrChannels = 16;
  const unsigned kNrTabs = 4;
  const unsigned kNrSamplesPerChannel = 128;
  const bool kEnableFFT = false;

  TestImpl<TestType>::init(kNrChannels, kNrSamplesPerChannel, kNrTabs,
                           kEnableFFT);

  SECTION("Zero input") { TestImpl<TestType>::runTestZero(); }
  SECTION("One input") { TestImpl<TestType>::runTestOne(); }
  SECTION("Different input") { TestImpl<TestType>::runTestDifferent(); }
}

using TestTypes = std::tuple<float>;
TEMPLATE_LIST_TEST_CASE_METHOD(TestImpl, "CoherentStokesTransposeKernel",
                               "[transpose+fft]", TestTypes) {

  if (TestImpl<TestType>::skip_) {
    return;
  }

  const unsigned kNrChannels = 256;
  const unsigned kNrTabs = 4;
  const unsigned kNrSamplesPerChannel = 128;
  const bool kEnableFFT = true;

  TestImpl<TestType>::init(kNrChannels, kNrSamplesPerChannel, kNrTabs,
                           kEnableFFT);

  SECTION("Different input") { TestImpl<TestType>::runTestFFT(); }
}
