#include <CoInterface/BlockID.h>
#include <CoInterface/Span.h>
#include <GPUProc/Kernels/CoherentStokesKernel.h>

#include <complex>

#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <cuda_fp16.h>
#include <xtensor/xview.hpp>

#include "../TestUtil.h"
#include "../fpequals.h"

using namespace LOFAR::Cobalt;

template <typename InputType> class TestImpl : public TestFixture {
public:
  void init(unsigned nr_tabs, unsigned nr_samples_per_channel,
            unsigned nr_channels, unsigned time_integration_factor) {
    nr_tabs_ = nr_tabs;
    nr_samples_per_channel_ = nr_samples_per_channel;
    nr_channels_ = nr_channels;
    time_integration_factor_ = time_integration_factor;

    shape_input_data_ = {nr_tabs, kNrPolarizations, nr_samples_per_channel,
                         nr_channels};
    shape_output_data_ = {nr_tabs, kNrStokes,
                          nr_samples_per_channel / time_integration_factor,
                          nr_channels};

    const size_t nr_bits_per_sample = sizeof(InputType) * 8;

    CoherentStokesKernel::Parameters parameters(
        nr_channels,             // nrChannels
        nr_samples_per_channel,  // nrSamplesPerChannel
        nr_tabs,                 // nrTABs
        kNrStokes,               // nrStokes
        nr_bits_per_sample,      // nrBitsPerSample
        kComplexVoltages,        // outputComplexVoltages
        time_integration_factor, // timeIntegrationFactor
        kQuantizeOutput,         // quantizeOutput
        0                        // nrChannelsDelayComp
    );

    host_input_data_.reset(new cu::HostMemory(
        parameters.bufferSize(CoherentStokesKernel::INPUT_DATA)));
    host_output_data_.reset(new cu::HostMemory(
        parameters.bufferSize(CoherentStokesKernel::OUTPUT_DATA)));

    // The factory needs to be kept alive during the lifetime of this class
    factory_.reset(
        new KernelFactory<CoherentStokesKernel>(device_, parameters));

    kernel_.reset(factory_->create(context_, stream_));
  }

  // An input of all zeros should result in an output of all zeros.
  void runTestZeros() {
    auto input_data =
        CreateSpan(static_cast<std::complex<InputType> *>(*host_input_data_),
                   shape_input_data_);
    input_data.fill(std::complex<InputType>(0));
    xt::xtensor<InputType, 4> output_ref =
        xt::zeros<InputType>(shape_output_data_);
    runKernel(output_ref);
  }

  // ***********************************************************
  // tests if the stokes parameters are calculate correctly. For a single sample
  // I = X *  con(X) + Y * con(Y)
  // Q = X *  con(X) - Y * con(Y)
  // U = 2 * RE(X * con(Y))
  // V = 2 * IM(X * con(Y))
  //
  // This reduces to (validate on paper by Wouter and John):
  // PX = RE(X) * RE(X) + IM(X) * IM(X)
  // PY = RE(Y) * RE(Y) + IM(Y) * IM(Y)
  // I = PX + PY
  // Q = PX - PY
  //
  // U = 2 * (RE(X) * RE(Y) + IM(X) * IM(Y))
  // V = 2 * (IM(X) * RE(Y) - RE(X) * IM(Y))
  void runTestCoherentNoComplex1Sample() {
    auto input_data =
        CreateSpan(static_cast<std::complex<InputType> *>(*host_input_data_),
                   shape_input_data_);
    input_data(0, 0, 0, 0) = std::complex<InputType>(2.0f, 0.0f);
    input_data(0, 1, 0, 0) = std::complex<InputType>(1.0f, 0.0f);

    xt::xtensor<InputType, 4> output_ref =
        xt::zeros<InputType>(shape_output_data_);
    output_ref(0, 0, 0, 0) = 5.0f;
    output_ref(0, 1, 0, 0) = 3.0f;
    output_ref(0, 2, 0, 0) = 4.0f;
    output_ref(0, 3, 0, 0) = 0.0f;

    runKernel(output_ref);
  }

  void runTestCoherentComplex1Sample() {
    auto input_data =
        CreateSpan(static_cast<std::complex<InputType> *>(*host_input_data_),
                   shape_input_data_);
    input_data(0, 0, 0, 0) = std::complex<InputType>(0.0f, 2.0f);
    input_data(0, 1, 0, 0) = std::complex<InputType>(0.0f, 1.0f);

    xt::xtensor<InputType, 4> output_ref =
        xt::zeros<InputType>(shape_output_data_);
    output_ref(0, 0, 0, 0) = 5.0f;
    output_ref(0, 1, 0, 0) = 3.0f;
    output_ref(0, 2, 0, 0) = 4.0f;
    output_ref(0, 3, 0, 0) = 0.0f;

    runKernel(output_ref);
  }

  void runTestCoherent4DifferentValuesSample() {
    auto input_data =
        CreateSpan(static_cast<std::complex<InputType> *>(*host_input_data_),
                   shape_input_data_);
    input_data(0, 0, 0, 0) = std::complex<InputType>(1.0f, 2.0f);
    input_data(0, 1, 0, 0) = std::complex<InputType>(3.0f, 4.0f);

    xt::xtensor<InputType, 4> output_ref =
        xt::zeros<InputType>(shape_output_data_);
    output_ref(0, 0, 0, 0) = 30.0f;
    output_ref(0, 1, 0, 0) = -20.0f;
    output_ref(0, 2, 0, 0) = 22.0f;
    output_ref(0, 3, 0, 0) = 4.0f;

    runKernel(output_ref);
  }

  void runTestBasicIntegration() {
    auto input_data =
        CreateSpan(static_cast<std::complex<InputType> *>(*host_input_data_),
                   shape_input_data_);
    input_data.fill(std::complex<InputType>(1.0f, 0.0f));

    xt::xtensor<InputType, 4> output_ref =
        xt::zeros<InputType>(shape_output_data_);
    xt::view(output_ref, xt::all(), 0, xt::all(), xt::all()) =
        2.0f * nr_samples_per_channel_;
    xt::view(output_ref, xt::all(), 1, xt::all(), xt::all()) = 0.0f;
    xt::view(output_ref, xt::all(), 2, xt::all(), xt::all()) =
        2.0f * nr_samples_per_channel_;
    xt::view(output_ref, xt::all(), 3, xt::all(), xt::all()) = 0.0f;

    runKernel(output_ref);
  }

  void runTestCoherent2DifferentValuesAllDim() {
    auto input_data =
        CreateSpan(static_cast<std::complex<InputType> *>(*host_input_data_),
                   shape_input_data_);
    input_data.fill(std::complex<InputType>(1.0f, 2.0f));

    xt::xtensor<InputType, 4> output_ref =
        xt::zeros<InputType>(shape_output_data_);
    xt::view(output_ref, xt::all(), 0, xt::all(), xt::all()) =
        10.0f * time_integration_factor_;
    xt::view(output_ref, xt::all(), 1, xt::all(), xt::all()) = 0.0f;
    xt::view(output_ref, xt::all(), 2, xt::all(), xt::all()) =
        10.0f * time_integration_factor_;
    xt::view(output_ref, xt::all(), 3, xt::all(), xt::all()) = 0.0f;

    runKernel(output_ref);
  }

  void runTest1536OutputSamples() {
    auto input_data =
        CreateSpan(static_cast<std::complex<InputType> *>(*host_input_data_),
                   shape_input_data_);
    input_data.fill(std::complex<InputType>(1.0f, 2.0f));

    xt::xtensor<InputType, 4> output_ref =
        xt::zeros<InputType>(shape_output_data_);

    xt::view(output_ref, xt::all(), 0, xt::all(), xt::all()) =
        10.0f * time_integration_factor_;
    xt::view(output_ref, xt::all(), 1, xt::all(), xt::all()) = 0.0f;
    xt::view(output_ref, xt::all(), 2, xt::all(), xt::all()) =
        10.0f * time_integration_factor_;

    runKernel(output_ref);
  }

private:
  void runKernel(const xt::xtensor<InputType, 4> &ref_output) {
    const size_t sizeof_output_data = host_output_data_->size();
    cu::DeviceMemory dev_output_data(sizeof_output_data);

    const size_t sizeof_input_data = host_input_data_->size();
    cu::DeviceMemory dev_input_data_(sizeof_input_data);
    stream_.memcpyHtoDAsync(dev_input_data_, *host_input_data_,
                            sizeof_input_data);

    BlockID block_id;

    kernel_->enqueue(block_id, dev_input_data_, dev_output_data);

    stream_.memcpyDtoHAsync(*host_output_data_, dev_output_data,
                            host_output_data_->size());

    stream_.synchronize();

    const float epsilon = getEpsilon<InputType>();

    auto output_data = CreateSpan(static_cast<InputType *>(*host_output_data_),
                                  shape_output_data_);
    for (size_t idx = 0; idx < output_data.size(); ++idx) {
      fpEquals(ref_output.data()[idx], output_data.data()[idx], epsilon);
    }
  }

  const unsigned kNrPolarizations = 2;
  const unsigned kNrStokes = 4;

  const bool kComplexVoltages = false;
  const bool kQuantizeOutput = false;

  unsigned nr_tabs_;
  unsigned nr_samples_per_channel_;
  unsigned nr_channels_;
  unsigned time_integration_factor_;
  bool complex_voltages_;

  std::unique_ptr<KernelFactory<CoherentStokesKernel>> factory_;
  std::unique_ptr<CoherentStokesKernel> kernel_;

  std::array<size_t, 4> shape_input_data_;
  std::array<size_t, 4> shape_output_data_;

  std::unique_ptr<cu::HostMemory> host_input_data_;
  std::unique_ptr<cu::HostMemory> host_output_data_;
};

using TestTypes = std::tuple<half, float>;
TEMPLATE_LIST_TEST_CASE_METHOD(TestImpl, "CoherentStokesKernel",
                               "[correctness]", TestTypes) {
  if (TestImpl<TestType>::skip_) {
    return;
  }

  SECTION("ZeroTest") {
    const unsigned int kTimeIntegrationFactor = 1;
    const unsigned int kNrSamplesPerChannel = 32;

    std::vector<size_t> tabs{1, 13};
    std::vector<size_t> channels{1, 16, 32};
    std::vector<size_t> samples{channels[0] * kNrSamplesPerChannel,
                                channels[1] * kNrSamplesPerChannel,
                                channels[2] * kNrSamplesPerChannel};

    for (size_t nr_tabs : tabs) {
      for (size_t nr_channels : channels) {
        for (size_t nr_input_samples : samples) {
          const unsigned int nr_output_samples =
              nr_input_samples / kTimeIntegrationFactor;

          TestImpl<TestType>::init(nr_tabs, kNrSamplesPerChannel, nr_channels,
                                   kTimeIntegrationFactor);
          TestImpl<TestType>::runTestZeros();
        }
      }
    }
  }

  SECTION("CoherentNoComplex1SampleTest") {
    const unsigned int kNrTabs = 1;
    const unsigned int kNrInputSamples = 1;
    const unsigned int kNrChannels = 1;
    const unsigned int kTimeIntegrationFactor = 1;
    TestImpl<TestType>::init(kNrTabs, kNrInputSamples, kNrChannels,
                             kTimeIntegrationFactor);
    TestImpl<TestType>::runTestCoherentNoComplex1Sample();
  }

  SECTION("CoherentComplex1SampleTest") {
    const unsigned int kNrTabs = 1;
    const unsigned int kNrInputSamples = 1;
    const unsigned int kNrChannels = 1;
    const unsigned int kTimeIntegrationFactor = 1;
    TestImpl<TestType>::init(kNrTabs, kNrInputSamples, kNrChannels,
                             kTimeIntegrationFactor);
    TestImpl<TestType>::runTestCoherentComplex1Sample();
  }

  SECTION("Coherent4DifferentValuesSampleTest") {
    const unsigned int kNrTabs = 1;
    const unsigned int kNrInputSamples = 1;
    const unsigned int kNrChannels = 1;
    const unsigned int kTimeIntegrationFactor = 1;
    TestImpl<TestType>::init(kNrTabs, kNrInputSamples, kNrChannels,
                             kTimeIntegrationFactor);
    TestImpl<TestType>::runTestCoherent4DifferentValuesSample();
  }

  SECTION("BasicIntegrationTest") {
    const unsigned int kNrTabs = 1;
    const unsigned int kNrInputSamples = 16;
    const unsigned int kNrChannels = 1;
    const unsigned int kTimeIntegrationFactor = 16;
    TestImpl<TestType>::init(kNrTabs, kNrInputSamples, kNrChannels,
                             kTimeIntegrationFactor);
    TestImpl<TestType>::runTestBasicIntegration();
  }

  SECTION("Coherent2DifferentValuesAllDimTest") {
    const unsigned int kNrTabs = 17;
    const unsigned int kNrInputSamples = 600;
    const unsigned int kNrChannels = 1;
    const unsigned int kTimeIntegrationFactor = 1;
    TestImpl<TestType>::init(kNrTabs, kNrInputSamples, kNrChannels,
                             kTimeIntegrationFactor);
    TestImpl<TestType>::runTestCoherent2DifferentValuesAllDim();
  }

  SECTION("Coherent1536OutputSamples") {
    const unsigned int kNrTabs = 1;
    const unsigned int kNrChannels = 16;
    const unsigned int kNrOutputSamples = 1536;

    std::vector<size_t> integration_sizes{2, 3, 4, 8, 16, 32, 64};

    for (size_t integration_size : integration_sizes) {
      const unsigned int nr_input_samples = integration_size * kNrOutputSamples;
      TestImpl<TestType>::init(kNrTabs, nr_input_samples, kNrChannels,
                               integration_size);
      TestImpl<TestType>::runTest1536OutputSamples();
    }
  }
}