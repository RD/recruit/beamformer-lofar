#include <random>

#include <CoInterface/BlockID.h>
#include <CoInterface/Span.h>
#include <GPUProc/Kernels/QuantizeOutputKernel.h>

#include <catch2/catch_template_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>

#include "../TestUtil.h"

using namespace LOFAR::Cobalt;

class TestImpl : public TestFixture {
public:
  void init(unsigned nr_channels, unsigned nr_samples_per_channel,
            unsigned nr_stokes, unsigned nr_quantize_bits) {
    nr_samples_per_channel_ = nr_samples_per_channel;
    nr_channels_ = nr_channels;
    nr_stokes_ = nr_stokes;
    nr_quantize_bits_ = nr_quantize_bits;

    shape_input_data_ = {kNrTabs, nr_stokes, nr_channels,
                         nr_samples_per_channel};

    QuantizeOutputKernel::Parameters parameters(
        nr_channels,            // nrChannels
        nr_samples_per_channel, // nrSamplesPerChannel
        kNrTabs,                // nrTABs
        nr_stokes,              // nrStokes
        kComplexVoltages,       // outputComplexVoltages
        nr_quantize_bits,       // nrQuantizeBits
        kQuantizeScale,         // quantizeScaleMax
        -kQuantizeScale,        // quantizeScaleMin
        kStokesIPositive        // sIpositive
    );

    host_input_data_.reset(new cu::HostMemory(
        parameters.bufferSize(QuantizeOutputKernel::INPUT_DATA)));
    host_output_data_.reset(new cu::HostMemory(
        parameters.bufferSize(QuantizeOutputKernel::OUTPUT_DATA)));
    device_input_data_.reset(new cu::DeviceMemory(
        parameters.bufferSize(QuantizeOutputKernel::INPUT_DATA)));
    device_output_data_.reset(new cu::DeviceMemory(
        parameters.bufferSize(QuantizeOutputKernel::OUTPUT_DATA)));

    // The factory needs to be kept alive during the lifetime of this class
    factory_.reset(
        new KernelFactory<QuantizeOutputKernel>(device_, parameters));

    kernel_.reset(factory_->create(context_, stream_));
  }

  void initializeInput(Span<float, 4> &input_data) {
    // Initialze the input with random normal values, zero-mean for values that
    // are quantized to signed, 0.5 mean for values that are quantized to
    // unsigned.
    std::mt19937 mt(0);
    std::uniform_real_distribution<float> dist_signed(-1, 1);
    std::uniform_real_distribution<float> dist_unsigned(0, 1);

    for (size_t tab = 0; tab < kNrTabs; tab++) {
      for (size_t stokes = 0; stokes < nr_stokes_; stokes++) {
        bool stokes_quv = (stokes > 0);
        bool zero_mean = (kComplexVoltages || stokes_quv);
        for (size_t sample = 0; sample < nr_samples_per_channel_; sample++) {
          for (size_t channel = 0; channel < nr_channels_; channel++) {
            const float value = zero_mean ? dist_signed(mt) : dist_unsigned(mt);
            input_data(tab, stokes, channel, sample) = value;
          }
        }
      }
    }
  }

  void runTest(float tolerance) {
    auto input_data =
        CreateSpan(static_cast<float *>(*host_input_data_), shape_input_data_);

    initializeInput(input_data);

    // After the call to runKernel bewlow, host_output_data
    // contains the metadata and quantized data
    runKernel();

    // Allocate a new buffer for reconstructed floating-point data
    const std::array<size_t, 4> shape_output_values = {
        kNrTabs, nr_stokes_, nr_samples_per_channel_, nr_channels_};
    xt::xtensor<float, 4> output_values(shape_output_values);

    // Do the reconstruction
    convertI2F(output_values, *host_output_data_);

    // Compare the values
    for (int tab = 0; tab < kNrTabs; tab++) {
      for (int stoke = 0; stoke < nr_stokes_; stoke++) {
        for (int channel = 0; channel < nr_channels_; channel++) {
          for (int sample = 0; sample < nr_samples_per_channel_; sample++) {
            const float ref = input_data(tab, stoke, channel, sample);
            const float tst = output_values(tab, stoke, sample, channel);
            REQUIRE_THAT(tst, Catch::Matchers::WithinAbs(ref, tolerance));
          }
        }
      }
    }
  }

private:
  void runKernel() {
    const size_t sizeof_input_data = host_input_data_->size();
    stream_.memcpyHtoDAsync(*device_input_data_, *host_input_data_,
                            sizeof_input_data);

    BlockID block_id;

    kernel_->enqueue(block_id, *device_input_data_, *device_output_data_);

    stream_.memcpyDtoHAsync(*host_output_data_, *device_output_data_,
                            host_output_data_->size());

    stream_.synchronize();
  }

  void convertI2F(xt::xtensor<float, 4> &output, const char *input) {
    const std::array<size_t, 3> shape_scales{kNrTabs, nr_stokes_, nr_channels_};

    const std::array<size_t, 3> shape_offsets{kNrTabs, nr_stokes_,
                                              nr_channels_};

    const std::array<size_t, 4> shape_data{
        kNrTabs, nr_stokes_, nr_samples_per_channel_, nr_channels_};

    const QuantizeOutputKernel::OutputBufferOffsets buffer_offsets =
        kernel_->bufferOffsets();

    const char *ptr = input;
    const char *scales_ptr = input + buffer_offsets.scale;
    const char *offsets_ptr = input + buffer_offsets.offset;
    const char *data_ptr = input + buffer_offsets.data;
    auto qscales = CreateSpan<const float, 3>(
        reinterpret_cast<const float *>(scales_ptr), shape_scales);
    auto qoffsets = CreateSpan<const float, 3>(
        reinterpret_cast<const float *>(offsets_ptr), shape_offsets);
    auto data_8bit = CreateSpan<const int8_t, 4>(
        reinterpret_cast<const int8_t *>(data_ptr), shape_data);
    auto data_16bit = CreateSpan<const int16_t, 4>(
        reinterpret_cast<const int16_t *>(data_ptr), shape_data);

    for (size_t tab = 0; tab < kNrTabs; tab++) {
      for (size_t stoke = 0; stoke < nr_stokes_; stoke++) {
        const bool stokes_quv = stoke > 0;
        const bool zero_mean = kComplexVoltages || stokes_quv;

        for (size_t sample = 0; sample < nr_samples_per_channel_; sample++) {
          for (size_t channel = 0; channel < nr_channels_; channel++) {
            // Get quantized value as 32-bit integer
            int32_t x_int;
            if (nr_quantize_bits_ == 8) {
              const int8_t value = data_8bit(tab, stoke, sample, channel);
              x_int = zero_mean ? value
                                : *reinterpret_cast<const uint8_t *>(&value);
            } else if (nr_quantize_bits_ == 16) {
              const int16_t value = data_16bit(tab, stoke, sample, channel);
              x_int = zero_mean ? value
                                : *reinterpret_cast<const uint16_t *>(&value);
            }

            // Reconstruct sample
            const float x_scale = qscales(tab, stoke, channel);
            const float x_offset = qoffsets(tab, stoke, channel);
            const float x_float = x_int * x_scale + x_offset;
            output(tab, stoke, sample, channel) = x_float;
          }
        }
      }
    }
  }

  const unsigned kNrTabs = 21;
  const bool kComplexVoltages = false;
  const float kQuantizeScale = 2;
  const bool kStokesIPositive = false;

  unsigned nr_samples_per_channel_;
  unsigned nr_channels_;
  unsigned nr_stokes_;
  unsigned nr_quantize_bits_;

  std::unique_ptr<KernelFactory<QuantizeOutputKernel>> factory_;
  std::unique_ptr<QuantizeOutputKernel> kernel_;

  std::array<size_t, 4> shape_input_data_;

  std::unique_ptr<cu::HostMemory> host_input_data_;
  std::unique_ptr<cu::HostMemory> host_output_data_;

  std::unique_ptr<cu::DeviceMemory> device_input_data_;
  std::unique_ptr<cu::DeviceMemory> device_output_data_;
};

TEST_CASE_METHOD(TestFixture, "QuantizeOutputKernel", "[run]") {
  if (skip_) {
    return;
  }

  const unsigned kNrChannels = 16;
  const unsigned kNrSamplesPerChannel = 1024;
  const unsigned kNrTabs = 21;
  const unsigned kNrStokes = 4;
  const bool kComplexVoltages = GENERATE(false, true);
  const unsigned kNrQuantizeBits = GENERATE(8, 16);
  const float kQuantizeScaleMax = 5;
  const float kQuantizeScaleMin = -5;
  const bool kStokesIPositive = GENERATE(false, true);

  CAPTURE(kComplexVoltages);
  CAPTURE(kNrQuantizeBits);
  CAPTURE(kStokesIPositive);

  QuantizeOutputKernel::Parameters parameters(
      kNrChannels,          // nrChannels
      kNrSamplesPerChannel, // nrSamplesPerChannel
      kNrTabs,              // nrTABs
      kNrStokes,            // nrStokes
      kComplexVoltages,     // outputComplexVoltages
      kNrQuantizeBits,      // nrQuantizeBits
      kQuantizeScaleMax,    // quantizeScaleMax
      kQuantizeScaleMin,    // quantizeScaleMin
      kStokesIPositive      // sIpositive
  );

  KernelFactory<QuantizeOutputKernel> factory(device_, parameters);

  cu::DeviceMemory dev_input_data(
      parameters.bufferSize(QuantizeOutputKernel::INPUT_DATA)),
      dev_output_data(parameters.bufferSize(QuantizeOutputKernel::OUTPUT_DATA));

  std::unique_ptr<QuantizeOutputKernel> kernel(
      factory.create(context_, stream_));

  BlockID block_id;

  for (unsigned i = 0; i < 10; i++) {
    kernel->enqueue(block_id, dev_input_data, dev_output_data);
    stream_.synchronize();
  }
}

TEST_CASE_METHOD(TestImpl, "QuantizeOutputKernel", "[correctness]") {
  if (TestImpl::skip_) {
    return;
  }

  const unsigned kNrChannels = 64;
  const unsigned kNrSamplesPerChannel = 768;
  const unsigned kNrQuantizeBits = GENERATE(8, 16);
  const unsigned kNrStokes = GENERATE(1, 4);

  CAPTURE(kNrQuantizeBits);
  CAPTURE(kNrStokes);

  float tolerance = 0;
  if (kNrQuantizeBits == 8) {
    tolerance = 1e-1;
  } else {
    tolerance = 1e-3;
  }

  TestImpl::init(kNrChannels, kNrSamplesPerChannel, kNrStokes, kNrQuantizeBits);

  SECTION("Test") { TestImpl::runTest(tolerance); }
}
