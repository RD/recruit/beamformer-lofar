#include <CoInterface/BlockID.h>
#include <CoInterface/Span.h>
#include <GPUProc/Kernels/BeamFormerKernel.h>

#include <complex>

#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <cuda_fp16.h>
#include <xtensor/xview.hpp>

#include "../TestUtil.h"
#include "../fpequals.h"

using namespace LOFAR::Cobalt;

TEST_CASE_METHOD(TestFixture, "BeamFormerKernel", "[run]") {
  if (skip_) {
    return;
  }

  const unsigned kNrStations = GENERATE(4, 8);
  const unsigned kNrChannels = GENERATE(4, 64);
  const unsigned kNrSamplesPerChannel = GENERATE(1, 1024);
  const unsigned kNrTabs = kNrStations;
  const unsigned kNrInputBits = GENERATE(16, 32);
  const double kSubbandBandwidth = 100e3; // Hz
  const double kSubbandFrequency = 60e3;  // Hz

  CAPTURE(kNrStations);
  CAPTURE(kNrChannels);
  CAPTURE(kNrSamplesPerChannel);
  CAPTURE(kNrTabs);

  std::vector<unsigned> station_indices(kNrStations);
  std::iota(station_indices.begin(), station_indices.end(), 0);
  std::vector<unsigned> delay_indices(kNrStations);
  std::iota(delay_indices.begin(), delay_indices.end(), 0);

  BeamFormerKernel::Parameters parameters(
      station_indices, delay_indices,
      station_indices.size(), // nrDelays
      kNrChannels,            // nrChannels
      kNrSamplesPerChannel,   // nrSamplesPerChannal
      kNrTabs,                // nrTABs
      kNrInputBits,           // nrInputBits
      kSubbandBandwidth       // subbandBandwidth
  );

  KernelFactory<BeamFormerKernel> factory(device_, parameters);

  cu::DeviceMemory dev_input(
      parameters.bufferSize(BeamFormerKernel::INPUT_DATA)),
      dev_output(parameters.bufferSize(BeamFormerKernel::OUTPUT_DATA)),
      dev_tab_delays(
          parameters.bufferSize(BeamFormerKernel::BEAM_FORMER_DELAYS));

  std::unique_ptr<BeamFormerKernel> kernel(factory.create(context_, stream_));

  BlockID block_id;

  kernel->setDelays(dev_tab_delays);
  kernel->setSubbandFrequency(kSubbandFrequency);

  for (unsigned i = 0; i < 10; i++) {
    kernel->enqueue(block_id, dev_input, dev_output);
    stream_.synchronize();
  }
}

template <typename InputType> class TestImpl : public TestFixture {
public:
  void init(unsigned nr_stations, unsigned nr_channels,
            unsigned nr_samples_per_channel, unsigned nr_tabs,
            bool compute_weights) {
    nr_stations_ = nr_stations;
    nr_channels_ = nr_channels;
    nr_samples_per_channel_ = nr_samples_per_channel;
    nr_tabs_ = nr_tabs;
    compute_weights_ = compute_weights;

    // on kNrChannels=4 gives chnl freqs
    // {192+1/6, 196+1/6, 200+1/6, 204+1/6}
    subband_bandwidth_ = 4.0 * nr_channels;

    // to get a nice phi of N+pi/6
    subband_frequency_ = 200 + 1.0 / 6.0;

    shape_input_ = {nr_stations, nr_channels, nr_samples_per_channel,
                    nr_polarizations_};
    shape_output_ = {nr_channels, nr_samples_per_channel, nr_tabs,
                     nr_polarizations_};
    shape_delays_ = {nr_stations, nr_tabs};
    shape_weights_ = {nr_channels, nr_tabs, nr_stations};

    std::vector<unsigned> station_indices(nr_stations);
    std::iota(station_indices.begin(), station_indices.end(), 0);
    std::vector<unsigned> delay_indices(nr_stations);
    std::iota(delay_indices.begin(), delay_indices.end(), 0);

    const size_t nr_input_bits = sizeof(InputType) * 8;

    BeamFormerKernel::Parameters parameters(
        station_indices, delay_indices,
        station_indices.size(),  // nrDelays
        nr_channels_,            // nrChannels
        nr_samples_per_channel_, // nrSamplesPerChannal
        nr_tabs_,                // nrTABs
        nr_input_bits,           // nrInputBits
        subband_bandwidth_,      // subbandBandwidth
        compute_weights_         // computeWeights
    );

    host_input_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerKernel::INPUT_DATA)));
    host_output_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerKernel::OUTPUT_DATA)));
    host_delays_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerKernel::BEAM_FORMER_DELAYS)));
    if (!compute_weights) {
      host_weights_.reset(new cu::HostMemory(
          parameters.bufferSize(BeamFormerKernel::BEAM_FORMER_WEIGHTS)));
    }

    // The factory needs to be kept alive during the lifetime of this class
    factory_.reset(new KernelFactory<BeamFormerKernel>(device_, parameters));

    kernel_.reset(factory_->create(context_, stream_));
  }

  void computeWeights() {
    auto delays =
        CreateSpan(static_cast<double *>(*host_delays_), shape_delays_);
    auto weights =
        CreateSpan<std::complex<InputType>, 3>(*host_weights_, shape_weights_);

    for (unsigned channel = 0; channel < nr_channels_; channel++) {
      const double frequency = subband_frequency_ - 0.5 * subband_bandwidth_ +
                               channel * (subband_bandwidth_ / nr_channels_);
      const double delay = delays(0, 0);
      const double phi = -2.0 * delay * frequency * M_PI;
      std::complex<InputType> weight(InputType(cos(phi)), InputType(sin(phi)));
      xt::view(weights, channel, xt::all(), xt::all()).fill(weight);
    }
  }

  void runTestOnes() {
    auto input = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_input_), shape_input_);
    auto delays =
        CreateSpan(static_cast<double *>(*host_delays_), shape_delays_);
    delays.fill(0.0);

    if (!compute_weights_) {
      computeWeights();
    }

    input.fill(std::complex<InputType>(1, 1));

    std::complex<float> ref_value(nr_stations_, nr_stations_);

    runKernel(ref_value);
  }

  void runTestZero() {
    auto input = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_input_), shape_input_);
    auto delays =
        CreateSpan(static_cast<double *>(*host_delays_), shape_delays_);
    delays.fill(0.0);

    if (!compute_weights_) {
      computeWeights();
    }

    input.fill(std::complex<InputType>(0));

    std::complex<float> ref_value(0.0f, 0.0f);

    runKernel(ref_value);
  }

  void runTestReference() {
    auto input = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_input_), shape_input_);
    auto output = CreateSpan(static_cast<std::complex<float> *>(*host_output_),
                             shape_output_);

    auto delays =
        CreateSpan(static_cast<double *>(*host_delays_), shape_delays_);
    delays.fill(-0.5);

    if (!compute_weights_) {
      computeWeights();
    }

    const std::complex<float> input_value(float(sqrt(3.0)),
                                          2.0f); // to square a sqrt(3) later
    input.fill(std::complex<InputType>(input_value));

    output.fill(std::complex<float>(poison_, poison_));

    const std::complex<double> cos_sin_phi(
        cos(M_PI / 6.0),  // 0.5*sqrt(3.0) (~ 0.86602540378443864676)
        sin(M_PI / 6.0)); // 0.5

    // ref_value.real() is a nice 3*NR_STATIONS; imag() ends up at the just
    // acceptable ref output nr of 3*sqrt(3)*NR_STATIONS
    std::complex<float> ref_value = std::complex<float>(
        cos_sin_phi *
        std::complex<double>(input_value.real(), input_value.imag()) *
        (double)nr_stations_);

    runKernel(ref_value);
  }

private:
  void runKernel(const std::complex<float> &ref_value) {
    cu::DeviceMemory dev_output(host_output_->size());

    cu::DeviceMemory dev_input_(host_input_->size());
    stream_.memcpyHtoDAsync(dev_input_, *host_input_, host_input_->size());

    BlockID block_id;

    const size_t sizeof_delays_or_weights =
        compute_weights_ ? host_delays_->size() : host_weights_->size();
    cu::DeviceMemory dev_delays_or_weights(sizeof_delays_or_weights);

    if (compute_weights_) {
      stream_.memcpyHtoDAsync(dev_delays_or_weights, *host_delays_,
                              host_delays_->size());
    } else {
      stream_.memcpyHtoDAsync(dev_delays_or_weights, *host_weights_,
                              host_weights_->size());
    }

    kernel_->setWeights(dev_delays_or_weights);
    kernel_->setSubbandFrequency(subband_frequency_);
    kernel_->enqueue(block_id, dev_input_, dev_output);

    stream_.memcpyDtoHAsync(*host_output_, dev_output, host_output_->size());

    stream_.synchronize();

    const float epsilon = getEpsilon<InputType>();

    auto output = CreateSpan(static_cast<std::complex<float> *>(*host_output_),
                             shape_output_);
    for (size_t idx = 0; idx < output.size(); ++idx) {
      fpEquals(ref_value, output.data()[idx], epsilon);
    }
  }

  unsigned nr_stations_;
  unsigned nr_channels_;
  unsigned nr_samples_per_channel_;
  unsigned nr_tabs_;
  bool compute_weights_;
  const unsigned nr_polarizations_ = 2;
  double subband_frequency_;
  double subband_bandwidth_;
  const float poison_ = 42.0f;

  std::unique_ptr<KernelFactory<BeamFormerKernel>> factory_;
  std::unique_ptr<BeamFormerKernel> kernel_;

  std::array<size_t, 4> shape_input_;
  std::array<size_t, 4> shape_output_;
  std::array<size_t, 2> shape_delays_;
  std::array<size_t, 3> shape_weights_;

  std::unique_ptr<cu::HostMemory> host_input_;
  std::unique_ptr<cu::HostMemory> host_output_;
  std::unique_ptr<cu::HostMemory> host_delays_;
  std::unique_ptr<cu::HostMemory> host_weights_;
};

using TestTypes = std::tuple<half, float>;
TEMPLATE_LIST_TEST_CASE_METHOD(TestImpl, "BeamFormerKernel", "[correctness]",
                               TestTypes) {
  if (TestImpl<TestType>::skip_) {
    return;
  }

  const unsigned NR_STATIONS = 4;
  const unsigned kNrChannels = 4;
  const unsigned kNrSamplesPerChannel = 8;
  const unsigned kNrTabs = 4;
  const bool COMPUTE_WEIGHTS = GENERATE(true, false);

  TestImpl<TestType>::init(NR_STATIONS, kNrChannels, kNrSamplesPerChannel,
                           kNrTabs, COMPUTE_WEIGHTS);

  // ***********************************************************
  // Baseline test 1: If all delays data is zero and input (1, 1),
  // then the output must be all (NR_STATIONS, NR_STATIONS). (See below and/or
  // bfkernel why.) The output array is initialized with 42s
  /*
   * Reference output calculation:
   * The bfkernel calculates the beamformer weights per station from the
   * delays. Since delays is 0, phi will be -M_PI * delay * channel_frequency
   * = 0. The complex weight is then (cos(phi), sin(phi)) = (1, 0). The
   * samples (all (1, 1)) are then complex multiplied with the weights and
   * summed for all (participating) stations. The complex mul gives (1, 1).
   * Summing NR_STATIONS of samples gives (NR_STATIONS, NR_STATIONS) for all
   * samples.
   */
  SECTION("All zero delays, input (1, 1)") {
    TestImpl<TestType>::runTestOnes();
  }

  // ***********************************************************
  // Baseline test 2: If all input data is zero the output should be zero
  // while the delays are non zero The output array is initialized with 42s
  SECTION("Input zero, output zero.") { TestImpl<TestType>::runTestZero(); }

  // ***********************************************************
  // Test 3: Test all, but cause reasonably "simple" reference output.
  // Set an unrealistic sb freq and bw, such that the ref output becomes a
  // simple, non-zero nr and the same for all channels. Make sure the mult of
  // (any channelFrequency, delay) ends up at an -(integer+(1/12)). The phi
  // will then be a 2N+pi/6, whose cos and sin are then "simple", non-zero.
  // The phase shift to (complex) mult the samples with is ( cos(phi),
  // sin(phi) ), possibly times some weight correction. Then sum over all
  // stations, i.e. all equal, so * NR_STATIONS.
  SECTION("Simple reference output") { TestImpl<TestType>::runTestReference(); }
}
