#include <CoInterface/BlockID.h>
#include <CoInterface/Span.h>
#include <GPUProc/Kernels/BeamFormerWeightsKernel.h>

#include <complex>

#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators_adapters.hpp>
#include <cuda_fp16.h>

#include "../TestUtil.h"
#include "../fpequals.h"

using namespace LOFAR::Cobalt;

template <typename OutputType> class TestImpl : public TestFixture {
public:
  TestImpl() : TestFixture(){};

  void init(unsigned nr_stations, unsigned nr_channels, unsigned nr_tabs,
            bool single_precision) {
    nr_stations_ = nr_stations;
    nr_channels_ = nr_channels;
    nr_tabs_ = nr_tabs;
    subband_bandwidth_ = 4.0 * nr_channels;
    single_precision_ = single_precision;

    shape_delays_ = {nr_stations, nr_tabs};
    shape_weights_ = {nr_channels_, nr_tabs, nr_stations};

    std::vector<unsigned> station_indices(nr_stations);
    std::iota(station_indices.begin(), station_indices.end(), 0);
    std::vector<unsigned> delay_indices(nr_stations);
    std::iota(delay_indices.begin(), delay_indices.end(), 0);

    const size_t nr_output_bits = sizeof(OutputType) * 8;

    BeamFormerWeightsKernel::Parameters parameters(
        station_indices, delay_indices,
        station_indices.size(), // nrDelays
        nr_channels,            // nrChannels
        nr_tabs,                // nrTABs
        nr_output_bits,         // nrInputBits
        subband_bandwidth_,     // subbandBandwidth
        single_precision        // singlePrecision
    );

    host_delays_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerWeightsKernel::BEAM_FORMER_DELAYS)));
    host_weights_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerWeightsKernel::BEAM_FORMER_WEIGHTS)));

    // The factory needs to be kept alive during the lifetime of this class
    factory_.reset(
        new KernelFactory<BeamFormerWeightsKernel>(device_, parameters));

    kernel_.reset(factory_->create(context_, stream_));
  }

  void runTest() {
    const float epsilon = single_precision_ ? 1e-5 : getEpsilon<OutputType>();

    auto delays = CreateSpan<double, 2>(*host_delays_, shape_delays_);
    auto weights =
        CreateSpan<std::complex<OutputType>, 3>(*host_weights_, shape_weights_);

    delays.fill(-0.5);

    std::vector<std::complex<float>> refVals(nr_channels_);
    for (unsigned channel = 0; channel < nr_channels_; channel++) {
      const double frequency = subband_frequency_ - 0.5 * subband_bandwidth_ +
                               channel * (subband_bandwidth_ / nr_channels_);
      const double delay = delays[0];
      const double phi = -2.0 * delay * frequency * M_PI;
      refVals[channel] = std::complex<float>(cos(phi), sin(phi));
    }

    runKernel();

    for (unsigned station = 0; station < nr_stations_; station++) {
      for (unsigned tab = 0; tab < nr_tabs_; tab++) {
        for (unsigned channel = 0; channel < nr_channels_; channel++) {
          const std::complex<OutputType> weight_ =
              weights(channel, tab, station);
          const std::complex<float> weight(weight_.real(), weight_.imag());
          fpEquals(refVals[channel], weight, epsilon);
        }
      }
    }
  }

private:
  void runKernel() {
    cu::DeviceMemory dev_delays(host_delays_->size());
    stream_.memcpyHtoDAsync(dev_delays, *host_delays_, host_delays_->size());

    cu::DeviceMemory dev_weights(host_weights_->size());

    BlockID block_id;

    kernel_->setSubbandFrequency(subband_frequency_);
    kernel_->enqueue(block_id, dev_delays, dev_weights);

    stream_.memcpyDtoHAsync(*host_weights_, dev_weights, host_weights_->size());

    stream_.synchronize();
  }

  unsigned nr_stations_;
  unsigned nr_channels_;
  unsigned nr_tabs_;
  double subband_bandwidth_;
  const double subband_frequency_ = 200 + 1.0 / 6;
  bool single_precision_;

  std::unique_ptr<KernelFactory<BeamFormerWeightsKernel>> factory_;
  std::unique_ptr<BeamFormerWeightsKernel> kernel_;

  std::array<size_t, 2> shape_delays_;
  std::array<size_t, 3> shape_weights_;

  std::unique_ptr<cu::HostMemory> host_delays_;
  std::unique_ptr<cu::HostMemory> host_weights_;
};

using TestTypes = std::tuple<half, float>;
TEMPLATE_LIST_TEST_CASE_METHOD(TestImpl, "BeamFormerWeightsKernel",
                               "[correctness]", TestTypes) {

  if (TestImpl<TestType>::skip_) {
    return;
  }

  const unsigned kNrStations = 3;
  const unsigned kNrChannels = 4;
  const unsigned kNrTabs = 4;
  const bool kSinglePrecision = GENERATE(false, true);

  TestImpl<TestType>::init(kNrStations, kNrChannels, kNrTabs, kSinglePrecision);

  SECTION("Reference input") { TestImpl<TestType>::runTest(); }
}