#include <CoInterface/BlockID.h>
#include <CoInterface/Span.h>
#include <GPUProc/FilterBank.h>
#include <GPUProc/Kernels/FIR_FilterKernel.h>

#define CATCH_CONFIG_NOSTDOUT
#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <cuda_fp16.h>
#include <fftw3.h>
#include <xtensor/xview.hpp>

#include "../TestUtil.h"
#include "../fpequals.h"

using namespace LOFAR::Cobalt;

inline float operator*(float a, half b) { return a * __half2float(b); }

template <typename InputType> class TestImpl : public TestFixture {
public:
  void init(unsigned nr_taps, unsigned nr_tabs, unsigned nr_samples_per_channel,
            unsigned nr_channels, bool enable_fft = false) {
    nr_taps_ = nr_taps;
    nr_tabs_ = nr_tabs;
    nr_samples_per_channel_ = nr_samples_per_channel;
    nr_channels_ = nr_channels;
    enable_fft_ = enable_fft;

    const unsigned nr_input_bits = sizeof(InputType) * 8;

    shape_filter_weights_ = {nr_channels, nr_taps};
    shape_input_data_ = {nr_tabs, kNrPolarizations, nr_samples_per_channel,
                         nr_channels};
    shape_output_data_ = shape_input_data_;

    FIR_FilterKernel::Parameters parameters(
        nr_tabs_,               // nrTABs,
        nr_input_bits,          // nrBitsPerSample,
        nr_channels,            // nrChannels,
        nr_samples_per_channel, // nrSamplesPerChannel,
        kScaleFactor,           // scaleFactor
        enable_fft              // enableFFT
    );

    host_filter_weights_.reset(new cu::HostMemory(
        parameters.bufferSize(FIR_FilterKernel::FILTER_WEIGHTS)));
    host_input_data_.reset(new cu::HostMemory(
        parameters.bufferSize(FIR_FilterKernel::INPUT_DATA)));
    host_output_data_.reset(new cu::HostMemory(
        parameters.bufferSize(FIR_FilterKernel::OUTPUT_DATA)));
    device_input_data_.reset(new cu::DeviceMemory(
        parameters.bufferSize(FIR_FilterKernel::INPUT_DATA)));
    device_output_data_.reset(new cu::DeviceMemory(
        parameters.bufferSize(FIR_FilterKernel::OUTPUT_DATA)));

    // The factory needs to be kept alive during the lifetime of this class
    factory_.reset(new KernelFactory<FIR_FilterKernel>(device_, parameters));

    kernel_.reset(factory_->create(context_, stream_));
  }

  void runTestOne() {
    // Single impulse test on single non-zero weight

    auto input_data =
        CreateSpan(static_cast<std::complex<InputType> *>(*host_input_data_),
                   shape_input_data_);
    auto filter_weights = CreateSpan(
        static_cast<InputType *>(*host_filter_weights_), shape_filter_weights_);
    input_data.fill(0);
    filter_weights.fill(0);

    unsigned tab = 0;
    unsigned channel = 0;
    unsigned pol = 0;
    unsigned sample = 0;

    filter_weights(0, 0) = 2.0f;
    kernel_->setWeights(filter_weights.data());
    input_data(tab, pol, sample, channel) = std::complex<InputType>(3.0f, 0.0f);

    // Expected output: St0, pol0, ch0, sampl0: 6. The rest all 0.
    xt::xtensor<std::complex<InputType>, 4> output_ref =
        xt::zeros<std::complex<InputType>>(shape_output_data_);
    output_ref(tab, pol, sample, channel) = std::complex<InputType>(6.0f, 0.0f);

    runKernel();
    compare(output_ref);
  }

  void runTestTwo() {
    // Impulse train 2*NR_TAPS apart. All st, all ch, all pol.

    auto input_data =
        CreateSpan(static_cast<std::complex<InputType> *>(*host_input_data_),
                   shape_input_data_);
    auto filter_weights = CreateSpan(
        static_cast<InputType *>(*host_filter_weights_), shape_filter_weights_);
    input_data.fill(0);
    filter_weights.fill(0);

    for (unsigned channel = 0; channel < nr_channels_; channel++) {
      for (unsigned tap = 0; tap < nr_taps_; tap++) {
        filter_weights(channel, tap) = channel + tap;
      }
    }
    kernel_->setWeights(filter_weights.data());

    // Expected output: sequences of (filterbank scaled by tab nr, nr_taps
    // zeros)
    xt::xtensor<std::complex<InputType>, 4> output_ref =
        xt::zeros<std::complex<InputType>>(shape_output_data_);
    for (unsigned tab = 0; tab < nr_tabs_; tab++) {
      for (unsigned pol = 0; pol < kNrPolarizations; pol++) {
        for (unsigned sample = 0; sample < nr_samples_per_channel_;
             sample += 2 * nr_taps_) {
          for (unsigned tap = 0; tap < nr_taps_; tap++) {
            for (unsigned channel = 0; channel < nr_channels_; channel++) {
              input_data(tab, pol, sample, channel) =
                  std::complex<InputType>(tab, 0);
              output_ref(tab, pol, sample + tap, channel) =
                  std::complex<InputType>(
                      kScaleFactor * tab * filter_weights(channel, tap), 0);
            }
          }
        }
      }
    }

    runKernel();
    compare(output_ref);
  }

  void runTestThree() {
    // Scaled step test (scaled DC gain) on KAISER filterbank. Non-zero
    // imag input.

    auto input_data =
        CreateSpan(static_cast<std::complex<InputType> *>(*host_input_data_),
                   shape_input_data_);
    auto filter_weights = CreateSpan(
        static_cast<InputType *>(*host_filter_weights_), shape_filter_weights_);
    input_data.fill(0);

    FilterBank filterBank(true, nr_taps_, nr_channels_, KAISER);
    filterBank.negateWeights();
    filterBank.scaleWeights(std::sqrt((double)nr_channels_));
    filter_weights = filterBank.getWeights();
    kernel_->setWeights(filter_weights.data());

    // Expected output: sequences of (filterbank scaled by tab nr, nr_taps
    // zeros)
    auto expected_sums = xt::sum(filter_weights, {1});

    xt::xtensor<std::complex<InputType>, 4> output_ref =
        xt::zeros<std::complex<InputType>>(shape_output_data_);
    const std::complex<InputType> input_value(2, 3);
    for (unsigned tab = 0; tab < nr_tabs_; tab++) {
      for (unsigned pol = 0; pol < kNrPolarizations; pol++) {
        for (unsigned sample = 0; sample < nr_samples_per_channel_; sample++) {
          for (unsigned channel = 0; channel < nr_channels_; channel++) {
            input_data(tab, pol, sample, channel) = input_value;
            if (sample < (nr_taps_ - 1)) {
              const InputType scale =
                  kScaleFactor * xt::sum(xt::view(filter_weights, channel,
                                                  xt::range(0, sample + 1)))();
              output_ref(tab, pol, sample, channel) = scale * input_value;
            } else {
              const InputType scale = kScaleFactor * expected_sums(channel);
              output_ref(tab, pol, sample, channel) = scale * input_value;
            }
          }
        }
      }
    }

    runKernel();
    compare(output_ref);
  }

  void runTestFour() {
    // Test the use of history samples, by invoking the GPU kernel more
    // than once.

    auto input_data =
        CreateSpan(static_cast<std::complex<InputType> *>(*host_input_data_),
                   shape_input_data_);
    auto filter_weights = CreateSpan(
        static_cast<InputType *>(*host_filter_weights_), shape_filter_weights_);
    input_data.fill(0);

    // Set FIR filter weights: a triangle shaped pulse response.
    for (unsigned channel = 0; channel < nr_channels_; channel++) {
      for (unsigned tap = 0; tap < nr_taps_; tap++) {
        filter_weights(channel, tap) = (nr_taps_ - tap) / 16.0f;
      }
    }
    kernel_->setWeights(filter_weights.data());

    const size_t halfNrTaps = nr_taps_ / 2;
    const size_t quartNrTaps = halfNrTaps / 2;

    // Set input samples: a train of alternating real and imaginary pulses,
    // NR_TAPS / 4 samples apart.
    for (unsigned tab = 0; tab < nr_tabs_; tab++) {
      for (unsigned sample = 0; sample < nr_samples_per_channel_;
           sample += nr_taps_) {
        const size_t n0 = sample;
        const size_t n1 = n0 + quartNrTaps;
        const size_t n2 = n1 + quartNrTaps;
        const size_t n3 = n2 + quartNrTaps;
        for (unsigned channel = 0; channel < nr_channels_; channel++) {
          for (unsigned pol = 0; pol < kNrPolarizations; pol++) {
            reinterpret_cast<InputType *>(
                &input_data(tab, pol, n0, channel))[0] = 1.0f;
            reinterpret_cast<InputType *>(
                &input_data(tab, pol, n1, channel))[1] = 1.0f;
            reinterpret_cast<InputType *>(
                &input_data(tab, pol, n2, channel))[0] = -1.0f;
            reinterpret_cast<InputType *>(
                &input_data(tab, pol, n3, channel))[1] = -1.0f;
          }
        }
      }
    }

    xt::xtensor<std::complex<InputType>, 4> output_ref =
        xt::zeros<std::complex<InputType>>(shape_output_data_);

    for (unsigned tab = 0; tab < nr_tabs_; tab++) {
      for (unsigned pol = 0; pol < kNrPolarizations; pol++) {
        // transient part
        for (unsigned sample = 0; sample < halfNrTaps; sample++) {
          for (unsigned channel = 0; channel < nr_channels_; channel++) {
            const InputType value =
                kScaleFactor * filter_weights(channel, sample);
            reinterpret_cast<InputType *>(
                &output_ref(tab, pol, sample, channel))[0] = value;
            reinterpret_cast<InputType *>(&output_ref(
                tab, pol, sample + quartNrTaps, channel))[1] = value;
          }
        }
        // steady state part
        int sign = 1;
        for (unsigned sample = halfNrTaps;
             sample < nr_samples_per_channel_ - quartNrTaps; sample++) {
          sign = (sample % halfNrTaps == 0) ? -sign : sign;
          for (unsigned channel = 0; channel < nr_channels_; channel++) {
            const InputType value =
                kScaleFactor * sign / 2 * filter_weights(channel, 0);
            reinterpret_cast<InputType *>(
                &output_ref(tab, pol, sample, channel))[0] = value;
            reinterpret_cast<InputType *>(&output_ref(
                tab, pol, sample + quartNrTaps, channel))[1] = value;
          }
        }
      }
    }

    runKernel();

    // check transient part
    compare(output_ref, 0, halfNrTaps);

    // check steady state part
    compare(output_ref, halfNrTaps, nr_samples_per_channel_ - quartNrTaps);

    // Run the kernel for the second time. This time the history buffer should
    // be filled with previous samples. Hence we should NOT see the transient
    // behaviour of the FIR filter we saw previously in the first run.
    runKernel();

    // Set reference output. This time, there should be no transient behaviour.
    for (unsigned tab = 0; tab < nr_tabs_; tab++) {
      for (unsigned pol = 0; pol < kNrPolarizations; pol++) {
        int sign = -1;
        for (unsigned sample = 0;
             sample < nr_samples_per_channel_ - quartNrTaps; sample++) {
          sign = (sample % halfNrTaps == 0) ? -sign : sign;
          for (unsigned channel = 0; channel < nr_channels_; channel++) {
            const InputType value =
                kScaleFactor * sign / 2 * filter_weights(channel, 0);
            reinterpret_cast<InputType *>(
                &output_ref(tab, pol, sample, channel))[0] = value;
            reinterpret_cast<InputType *>(&output_ref(
                tab, pol, sample + quartNrTaps, channel))[1] = value;
          }
        }
      }
    }

    // check steady state part
    compare(output_ref, halfNrTaps, nr_samples_per_channel_ - quartNrTaps);
  }

  void runTestFive() {
    // Run the kernel, no correctness check.
    runKernel();
  }

private:
  void compare(xt::xtensor<std::complex<InputType>, 4> &reference,
               int sample_start = 0, int sample_end = -1) {
    assert(reference.shape().size() == shape_output_data_.size());
    const size_t n = reference.shape().size();
    for (int i = 0; i < reference.shape().size(); i++) {
      assert(reference.shape()[i] == shape_output_data_[i]);
    }

    auto output_data =
        CreateSpan(static_cast<std::complex<InputType> *>(*host_output_data_),
                   shape_output_data_);

    const float epsilon = getEpsilon<InputType>();

    if (sample_end == -1) {
      sample_end = nr_samples_per_channel_;
    }

    if (enable_fft_) {
      runFFT(reference.data(), reference.data());
    }

    for (int tab = 0; tab < nr_tabs_; tab++) {
      for (int pol = 0; pol < kNrPolarizations; pol++) {
        for (int sample = sample_start; sample < sample_end; sample++) {
          for (int channel = 0; channel < nr_channels_; channel++) {
            auto ref = reference(tab, pol, sample, channel);
            auto tst = output_data(tab, pol, sample, channel);
            fpEquals(ref, tst, epsilon);
          }
        }
      }
    }
  }

  void runKernel() {
    const size_t sizeof_input_data = host_input_data_->size();
    stream_.memcpyHtoDAsync(*device_input_data_, *host_input_data_,
                            sizeof_input_data);

    BlockID block_id;

    kernel_->enqueue(block_id, *device_input_data_, *device_output_data_);

    stream_.memcpyDtoHAsync(*host_output_data_, *device_output_data_,
                            host_output_data_->size());

    stream_.synchronize();
  }

  void runFFT(std::complex<InputType> *input, std::complex<InputType> *output) {
    fftwf_complex *in = reinterpret_cast<fftwf_complex *>(input);
    fftwf_complex *out = reinterpret_cast<fftwf_complex *>(output);
    int rank = 1;
    int n[] = {static_cast<int>(nr_channels_)};
    int howmany = nr_tabs_ * kNrPolarizations * nr_samples_per_channel_;
    int idist = n[0];
    int odist = n[0];
    int istride = 1;
    int ostride = 1;
    int *inembed = n;
    int *onembed = n;
    int sign = FFTW_FORWARD;
    int flags = FFTW_ESTIMATE;
    fftwf_plan plan =
        fftwf_plan_many_dft(rank, n, howmany, in, inembed, istride, idist, out,
                            onembed, ostride, odist, sign, flags);
    fftwf_execute(plan);
    fftwf_destroy_plan(plan);
  }

  const unsigned kNrPolarizations = 2;
  const float kScaleFactor = 1.0f;

  unsigned nr_taps_;
  unsigned nr_tabs_;
  unsigned nr_samples_per_channel_;
  unsigned nr_channels_;
  bool enable_fft_;

  std::unique_ptr<KernelFactory<FIR_FilterKernel>> factory_;
  std::unique_ptr<FIR_FilterKernel> kernel_;

  std::array<size_t, 2> shape_filter_weights_;
  std::array<size_t, 4> shape_input_data_;
  std::array<size_t, 4> shape_output_data_;

  std::unique_ptr<cu::HostMemory> host_filter_weights_;
  std::unique_ptr<cu::HostMemory> host_input_data_;
  std::unique_ptr<cu::HostMemory> host_output_data_;

  std::unique_ptr<cu::DeviceMemory> device_input_data_;
  std::unique_ptr<cu::DeviceMemory> device_output_data_;
};

using TestTypes = std::tuple<float>;
TEMPLATE_LIST_TEST_CASE_METHOD(TestImpl, "FIR_FilterKernel", "[correctness]",
                               TestTypes) {
  if (TestImpl<TestType>::skip_) {
    return;
  }

  const unsigned kNrTaps = 16;
  const unsigned kNrTabs = 20;
  const unsigned kNrSamplesPerChannel = 128;
  const unsigned kNrChannels = 64;

  TestImpl<TestType>::init(kNrTaps, kNrTabs, kNrSamplesPerChannel, kNrChannels);

  SECTION("Test1") { TestImpl<TestType>::runTestOne(); }

  SECTION("Test2") { TestImpl<TestType>::runTestTwo(); }

  SECTION("Test3") { TestImpl<TestType>::runTestThree(); }

  SECTION("Test4") { TestImpl<TestType>::runTestFour(); }
}

TEST_CASE_METHOD(TestImpl<float>, "FIR_FilterKernel", "[fir+fft]") {
  if (TestImpl<float>::skip_) {
    return;
  }

  const unsigned kNrTaps = 16;
  const unsigned kNrTabs = 4;
  const unsigned kNrSamplesPerChannel = 128;
  const unsigned kNrChannels = 64;
  const bool kEnableFFT = true;

  TestImpl<float>::init(kNrTaps, kNrTabs, kNrSamplesPerChannel, kNrChannels,
                        kEnableFFT);

  SECTION("Test2+FFT") { TestImpl<float>::runTestTwo(); }
}

TEST_CASE_METHOD(TestImpl<float>, "FIR_FilterKernel", "[cobalt]") {
  if (TestImpl<float>::skip_) {
    return;
  }

  const unsigned kNrTaps = 16;
  const unsigned kNrTabs = 20;
  const unsigned kNrSamplesPerChannel = 12288;
  const unsigned kNrChannels = 64;
  const bool kEnableFFT = true;

  TestImpl<float>::init(kNrTaps, kNrTabs, kNrSamplesPerChannel, kNrChannels,
                        kEnableFFT);

  SECTION("COBALT") { TestImpl<float>::runTestFive(); }
}
