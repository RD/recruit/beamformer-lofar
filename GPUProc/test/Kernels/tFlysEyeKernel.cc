#include <CoInterface/BlockID.h>
#include <CoInterface/Span.h>
#include <GPUProc/Kernels/FlysEyeKernel.h>

#include <complex>

#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <cuda_fp16.h>

#include "../TestUtil.h"
#include "../fpequals.h"

using namespace LOFAR::Cobalt;

TEST_CASE_METHOD(TestFixture, "FlysEyeKernel", "[run]") {
  if (skip_) {
    return;
  }

  const unsigned kNrStations = GENERATE(4, 8);
  const unsigned kNrChannels = GENERATE(4, 64);
  const unsigned kNrSamplesPerChannel = GENERATE(1, 1024);
  const unsigned kNrTabs = kNrStations;
  const unsigned kNrInputBits = GENERATE(16, 32);

  CAPTURE(kNrStations);
  CAPTURE(kNrChannels);
  CAPTURE(kNrSamplesPerChannel);
  CAPTURE(kNrTabs);

  std::vector<unsigned> station_indices(kNrStations);
  std::iota(station_indices.begin(), station_indices.end(), 0);

  FlysEyeKernel::Parameters parameters(
      station_indices,
      kNrChannels,          // nrChannels
      kNrSamplesPerChannel, // nrSamplesPerChannal
      kNrTabs,              // nrTABs
      kNrInputBits          // nrInputBits
  );

  KernelFactory<FlysEyeKernel> factory(device_, parameters);

  cu::DeviceMemory dev_input_data(
      parameters.bufferSize(FlysEyeKernel::INPUT_DATA)),
      dev_output_data(parameters.bufferSize(FlysEyeKernel::OUTPUT_DATA));

  std::unique_ptr<FlysEyeKernel> kernel(factory.create(context_, stream_));

  BlockID block_id;

  for (unsigned i = 0; i < 10; i++) {
    kernel->enqueue(block_id, dev_input_data, dev_output_data);
    stream_.synchronize();
  }
}

template <typename InputType> class TestImpl : public TestFixture {
public:
  void init(unsigned nr_stations, unsigned nr_channels,
            unsigned nr_samples_per_channel, unsigned nr_tabs) {
    nr_stations_ = nr_stations;
    nr_channels_ = nr_channels;
    nr_samples_per_channel_ = nr_samples_per_channel;
    nr_tabs_ = nr_tabs;

    shape_input_data_ = {nr_stations, nr_channels, nr_samples_per_channel,
                         nr_polarizations_};
    shape_output_data_ = {nr_channels, nr_samples_per_channel, nr_tabs,
                          nr_polarizations_};

    std::vector<unsigned> station_indices(nr_stations);
    std::iota(station_indices.begin(), station_indices.end(), 0);

    const size_t nr_input_bits = sizeof(InputType) * 8;

    FlysEyeKernel::Parameters parameters(
        station_indices,
        nr_channels_,            // nrChannels
        nr_samples_per_channel_, // nrSamplesPerChannal
        nr_tabs_,                // nrTABs
        nr_input_bits            // nrInputBits
    );

    host_input_data_.reset(
        new cu::HostMemory(parameters.bufferSize(FlysEyeKernel::INPUT_DATA)));
    host_output_data_.reset(
        new cu::HostMemory(parameters.bufferSize(FlysEyeKernel::OUTPUT_DATA)));

    // The factory needs to be kept alive during the lifetime of this class
    factory_.reset(new KernelFactory<FlysEyeKernel>(device_, parameters));

    kernel_.reset(factory_->create(context_, stream_));
  }

  void runTest() {
    auto input_data =
        CreateSpan(static_cast<std::complex<InputType> *>(*host_input_data_),
                   shape_input_data_);
    auto output_data =
        CreateSpan(static_cast<std::complex<float> *>(*host_output_data_),
                   shape_output_data_);

    for (size_t station = 0; station < nr_stations_; station++) {
      for (size_t channel = 0; channel < nr_channels_; channel++) {
        for (size_t sample = 0; sample < nr_samples_per_channel_; sample++) {
          for (size_t pol = 0; pol < nr_polarizations_; pol++) {
            const size_t real = (station + 1) * (channel + 1);
            const size_t imag = (sample + 1) * (pol + 1);
            input_data(station, channel, sample, pol) =
                std::complex<InputType>(real, imag);
          }
        }
      }
    }

    runKernel();

    for (size_t station = 0; station < nr_stations_; station++) {
      for (size_t channel = 0; channel < nr_channels_; channel++) {
        for (size_t sample = 0; sample < nr_samples_per_channel_; sample++) {
          for (size_t pol = 0; pol < nr_polarizations_; pol++) {
            const std::complex<InputType> value_ =
                input_data(station, channel, sample, pol);
            const std::complex<float> value(value_.real(), value_.imag());
            REQUIRE(value == output_data(channel, sample, station, pol));
          }
        }
      }
    }
  }

private:
  void runKernel() {
    cu::DeviceMemory dev_output_data(host_output_data_->size());

    cu::DeviceMemory dev_input_data_(host_input_data_->size());
    stream_.memcpyHtoDAsync(dev_input_data_, *host_input_data_,
                            host_input_data_->size());

    BlockID block_id;

    kernel_->enqueue(block_id, dev_input_data_, dev_output_data);

    stream_.memcpyDtoHAsync(*host_output_data_, dev_output_data,
                            host_output_data_->size());

    stream_.synchronize();
  }

  unsigned nr_stations_;
  unsigned nr_channels_;
  unsigned nr_samples_per_channel_;
  unsigned nr_tabs_;
  const unsigned nr_polarizations_ = 2;

  std::unique_ptr<KernelFactory<FlysEyeKernel>> factory_;
  std::unique_ptr<FlysEyeKernel> kernel_;

  std::array<size_t, 4> shape_input_data_;
  std::array<size_t, 4> shape_output_data_;

  std::unique_ptr<cu::HostMemory> host_input_data_;
  std::unique_ptr<cu::HostMemory> host_output_data_;
};

using TestTypes = std::tuple<half, float>;
TEMPLATE_LIST_TEST_CASE_METHOD(TestImpl, "FlysEyeKernel", "[correctness]",
                               TestTypes) {
  if (TestImpl<TestType>::skip_) {
    return;
  }

  const unsigned kNrStations = 4;
  const unsigned kNrChannels = 4;
  const unsigned kNrSamplesPerChannel = 8;
  const unsigned kNrTabs = 4;

  TestImpl<TestType>::init(kNrStations, kNrChannels, kNrSamplesPerChannel,
                           kNrTabs);

  SECTION("Dummy input") { TestImpl<TestType>::runTest(); }
}
