#include <CoInterface/BlockID.h>
#include <CoInterface/Span.h>
#include <GPUProc/Kernels/BeamFormerKernel.h>
#include <GPUProc/Kernels/BeamFormerWeightsKernel.h>

#include <complex>

#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <cuda_fp16.h>

#include "../TestUtil.h"
#include "../fpequals.h"

using namespace LOFAR::Cobalt;

template <typename InputType> class TestImpl : public TestFixture {
public:
  void init(unsigned nr_stations, unsigned nr_channels,
            unsigned nr_samples_per_channel, unsigned nr_tabs) {
    nr_stations_ = nr_stations;
    nr_channels_ = nr_channels;
    nr_samples_per_channel_ = nr_samples_per_channel;
    nr_tabs_ = nr_tabs;

    // on kNrChannels=4 gives chnl freqs
    // {192+1/6, 196+1/6, 200+1/6, 204+1/6}
    subband_bandwidth_ = 4.0 * nr_channels;

    // to get a nice phi of N+pi/6
    subband_frequency_ = 200 + 1.0 / 6.0;

    shape_input_data_ = {nr_stations, nr_channels, nr_samples_per_channel,
                         nr_polarizations_};
    shape_output_data_ = {nr_channels, nr_samples_per_channel, nr_tabs,
                          nr_polarizations_};
    shape_delays_ = {nr_stations, nr_tabs};
    shape_weights_ = {nr_channels, nr_tabs, nr_stations};

    std::vector<unsigned> station_indices(nr_stations);
    std::iota(station_indices.begin(), station_indices.end(), 0);
    std::vector<unsigned> delay_indices(nr_stations);
    std::iota(delay_indices.begin(), delay_indices.end(), 0);

    const size_t nr_input_bits = sizeof(InputType) * 8;

    BeamFormerKernel::Parameters bf_parameters(
        station_indices, delay_indices,
        station_indices.size(),  // nrDelays
        nr_channels_,            // nrChannels
        nr_samples_per_channel_, // nrSamplesPerChannal
        nr_tabs_,                // nrTABs
        nr_input_bits,           // nrInputBits
        subband_bandwidth_,      // subbandBandwidth
        false                    // computeWeights
    );

    host_input_data_.reset(new cu::HostMemory(
        bf_parameters.bufferSize(BeamFormerKernel::INPUT_DATA)));
    host_output_data_.reset(new cu::HostMemory(
        bf_parameters.bufferSize(BeamFormerKernel::OUTPUT_DATA)));
    host_delays_.reset(new cu::HostMemory(
        bf_parameters.bufferSize(BeamFormerKernel::BEAM_FORMER_DELAYS)));
    host_weights_.reset(new cu::HostMemory(
        bf_parameters.bufferSize(BeamFormerKernel::BEAM_FORMER_WEIGHTS)));

    factory_.reset(new KernelFactory<BeamFormerKernel>(device_, bf_parameters));

    kernel_.reset(factory_->create(context_, stream_));

    const size_t nr_output_bits = sizeof(InputType) * 8;

    BeamFormerWeightsKernel::Parameters bfw_parameters(
        station_indices, delay_indices,
        station_indices.size(), // nrDelays
        nr_channels,            // nrChannels
        nr_tabs,                // nrTABs
        nr_output_bits,         // nrInputBits
        subband_bandwidth_      // subbandBandwidth
    );

    bfw_factory_.reset(
        new KernelFactory<BeamFormerWeightsKernel>(device_, bfw_parameters));

    bfw_kernel_.reset(bfw_factory_->create(context_, stream_));
  }

  void runTestReference() {
    auto input_data =
        CreateSpan(static_cast<std::complex<InputType> *>(*host_input_data_),
                   shape_input_data_);
    auto output_data =
        CreateSpan(static_cast<std::complex<float> *>(*host_output_data_),
                   shape_output_data_);
    auto delays =
        CreateSpan(static_cast<double *>(*host_delays_), shape_delays_);

    const std::complex<float> input_value(float(sqrt(3.0)),
                                          2.0f); // to square a sqrt(3) later
    input_data.fill(std::complex<InputType>(input_value));

    output_data.fill(std::complex<float>(poison_, poison_));

    delays.fill(-0.5);

    const std::complex<double> cos_sin_phi(
        cos(M_PI / 6.0),  // 0.5*sqrt(3.0) (~ 0.86602540378443864676)
        sin(M_PI / 6.0)); // 0.5

    // ref_value.real() is a nice 3*NR_STATIONS; imag() ends up at the just
    // acceptable ref output nr of 3*sqrt(3)*NR_STATIONS
    std::complex<float> ref_value = std::complex<float>(
        cos_sin_phi *
        std::complex<double>(input_value.real(), input_value.imag()) *
        (double)nr_stations_);

    runKernel(ref_value);
  }

private:
  void runKernel(const std::complex<float> &ref_value) {
    cu::DeviceMemory dev_delays(host_delays_->size());
    stream_.memcpyHtoDAsync(dev_delays, *host_delays_, host_delays_->size());

    cu::DeviceMemory dev_weights(host_weights_->size());

    cu::DeviceMemory dev_output_data(host_output_data_->size());

    cu::DeviceMemory dev_input_data_(host_input_data_->size());
    stream_.memcpyHtoDAsync(dev_input_data_, *host_input_data_,
                            host_input_data_->size());

    BlockID block_id;

    bfw_kernel_->setSubbandFrequency(subband_frequency_);
    bfw_kernel_->enqueue(block_id, dev_delays, dev_weights);

    kernel_->setWeights(dev_weights);
    kernel_->setSubbandFrequency(subband_frequency_);
    kernel_->enqueue(block_id, dev_input_data_, dev_output_data);

    stream_.memcpyDtoHAsync(*host_output_data_, dev_output_data,
                            host_output_data_->size());

    stream_.synchronize();

    const float epsilon = getEpsilon<InputType>();

    auto output_data =
        CreateSpan(static_cast<std::complex<float> *>(*host_output_data_),
                   shape_output_data_);
    for (size_t idx = 0; idx < output_data.size(); ++idx) {
      fpEquals(ref_value, output_data.data()[idx], epsilon);
    }
  }

  unsigned nr_stations_;
  unsigned nr_channels_;
  unsigned nr_samples_per_channel_;
  unsigned nr_tabs_;
  const unsigned nr_polarizations_ = 2;
  double subband_frequency_;
  double subband_bandwidth_;
  const float poison_ = 42.0f;

  std::unique_ptr<KernelFactory<BeamFormerKernel>> factory_;
  std::unique_ptr<BeamFormerKernel> kernel_;

  std::unique_ptr<KernelFactory<BeamFormerWeightsKernel>> bfw_factory_;
  std::unique_ptr<BeamFormerWeightsKernel> bfw_kernel_;

  std::array<size_t, 4> shape_input_data_;
  std::array<size_t, 4> shape_output_data_;
  std::array<size_t, 2> shape_delays_;
  std::array<size_t, 3> shape_weights_;

  std::unique_ptr<cu::HostMemory> host_input_data_;
  std::unique_ptr<cu::HostMemory> host_output_data_;
  std::unique_ptr<cu::HostMemory> host_delays_;
  std::unique_ptr<cu::HostMemory> host_weights_;
};

using TestTypes = std::tuple<half, float>;
TEMPLATE_LIST_TEST_CASE_METHOD(TestImpl, "BeamFormerAndWeightsKernel",
                               "[correctness]", TestTypes) {
  if (TestImpl<TestType>::skip_) {
    return;
  }

  const unsigned kNrStations = 4;
  const unsigned kNrChannels = 4;
  const unsigned kNrSamplesPerChannel = 8;
  const unsigned kNrTabs = 4;

  TestImpl<TestType>::init(kNrStations, kNrChannels, kNrSamplesPerChannel,
                           kNrTabs);

  // ***********************************************************
  // Test all, but cause reasonably "simple" reference output.
  // Set an unrealistic sb freq and bw, such that the ref output becomes a
  // simple, non-zero nr and the same for all channels. Make sure the mult of
  // (any channelFrequency, delay) ends up at an -(integer+(1/12)). The phi
  // will then be a 2N+pi/6, whose cos and sin are then "simple", non-zero.
  // The phase shift to (complex) mult the samples with is ( cos(phi),
  // sin(phi) ), possibly times some weight correction. Then sum over all
  // stations, i.e. all equal, so * NR_STATIONS.
  SECTION("Simple reference output") { TestImpl<TestType>::runTestReference(); }
}
