#include <CoInterface/BlockID.h>
#include <CoInterface/Span.h>
#include <GPUProc/Kernels/BeamFormerKernel.h>
#include <GPUProc/Kernels/BeamFormerWMMAKernel.h>

#include <complex>

#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <cuda_fp16.h>
#include <xtensor/xview.hpp>

#include "../TestUtil.h"
#include "../fpequals.h"

using namespace LOFAR::Cobalt;

template <typename InputType> class TestImpl : public TestFixture {
public:
  void init(unsigned nr_stations, unsigned nr_channels,
            unsigned nr_samples_per_channel, unsigned nr_tabs,
            unsigned nr_polarizations) {
    nr_stations_ = nr_stations;
    nr_channels_ = nr_channels;
    nr_samples_per_channel_ = nr_samples_per_channel;
    nr_tabs_ = nr_tabs;
    nr_polarizations_ = nr_polarizations;

    // on NR_CHANNELS=4 gives chnl freqs
    // {192+1/6, 196+1/6, 200+1/6, 204+1/6}
    subband_bandwidth_ = 4.0 * nr_channels;

    // to get a nice phi of N+pi/6
    subband_frequency_ = 200 + 1.0 / 6.0;

    shape_input_ = {nr_channels, nr_polarizations, nr_stations,
                    nr_samples_per_channel};
    shape_output_ = {nr_channels, nr_polarizations, nr_tabs,
                     nr_samples_per_channel};
    shape_delays_ = {nr_tabs, nr_stations};
    shape_weights_ = {nr_channels, nr_tabs, nr_stations};

    const size_t nr_input_bits = sizeof(InputType) * 8;

    BeamFormerWMMAKernel::Parameters parameters(
        nr_stations,             // nrStations
        nr_channels_,            // nrChannels
        nr_samples_per_channel_, // nrSamplesPerChannal
        nr_tabs_,                // nrTABs
        nr_polarizations_,       // nrPolarizations
        nr_input_bits            // nrInputBits
    );

    host_input_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerWMMAKernel::INPUT_DATA)));
    host_output_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerWMMAKernel::OUTPUT_DATA)));
    const size_t sizeof_delays =
        std::accumulate(shape_delays_.begin(), shape_delays_.end(), 1,
                        std::multiplies<size_t>());
    host_delays_.reset(new cu::HostMemory(sizeof_delays));
    host_weights_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerWMMAKernel::BEAM_FORMER_WEIGHTS)));

    factory_.reset(
        new KernelFactory<BeamFormerWMMAKernel>(device_, parameters));

    kernel_.reset(factory_->create(context_, stream_));
  }

  void computeWeights() {
    auto delays =
        CreateSpan(static_cast<double *>(*host_delays_), shape_delays_);
    auto weights =
        CreateSpan<std::complex<InputType>, 3>(*host_weights_, shape_weights_);

    for (unsigned channel = 0; channel < nr_channels_; channel++) {
      const double frequency = subband_frequency_ - 0.5 * subband_bandwidth_ +
                               channel * (subband_bandwidth_ / nr_channels_);
      const double delay = delays(0, 0);
      const double phi = -2.0 * delay * frequency * M_PI;
      std::complex<InputType> weight(InputType(cos(phi)), InputType(sin(phi)));
      xt::view(weights, channel, xt::all(), xt::all()).fill(weight);
    }
  }

  void runTestReference() {
    auto input = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_input_), shape_input_);
    auto output = CreateSpan(static_cast<std::complex<float> *>(*host_output_),
                             shape_output_);
    auto delays =
        CreateSpan(static_cast<double *>(*host_delays_), shape_delays_);

    const std::complex<float> input_value(float(sqrt(3.0)),
                                          2.0f); // to square a sqrt(3) later
    input.fill(std::complex<InputType>(input_value));

    output.fill(std::complex<float>(poison_, poison_));

    delays.fill(-0.5);

    computeWeights();

    const std::complex<double> cos_sin_phi(
        cos(M_PI / 6.0),  // 0.5*sqrt(3.0) (~ 0.86602540378443864676)
        sin(M_PI / 6.0)); // 0.5

    // ref_value.real() is a nice 3*NR_STATIONS; imag() ends up at the just
    // acceptable ref output nr of 3*sqrt(3)*NR_STATIONS
    std::complex<float> ref_value = std::complex<float>(
        cos_sin_phi *
        std::complex<double>(input_value.real(), input_value.imag()) *
        (double)nr_stations_);

    runKernel(ref_value);
  }

private:
  void runKernel(const std::complex<float> &ref_value) {
    cu::DeviceMemory dev_weights(host_weights_->size());
    stream_.memcpyHtoDAsync(dev_weights, *host_weights_, host_weights_->size());

    cu::DeviceMemory dev_output(host_output_->size());

    cu::DeviceMemory dev_input_(host_input_->size());
    stream_.memcpyHtoDAsync(dev_input_, *host_input_, host_input_->size());

    BlockID block_id;

    kernel_->setWeights(dev_weights);
    kernel_->enqueue(block_id, dev_input_, dev_output);

    stream_.memcpyDtoHAsync(*host_output_, dev_output, host_output_->size());

    stream_.synchronize();

    const float epsilon = 1e-3;

    auto output = CreateSpan(static_cast<std::complex<float> *>(*host_output_),
                             shape_output_);
    for (size_t idx = 0; idx < output.size(); ++idx) {
      fpEquals(ref_value, output.data()[idx], epsilon);
    }
  }

  unsigned nr_stations_;
  unsigned nr_channels_;
  unsigned nr_samples_per_channel_;
  unsigned nr_tabs_;
  unsigned nr_polarizations_;
  double subband_frequency_;
  double subband_bandwidth_;
  const float poison_ = 42.0f;

  std::unique_ptr<KernelFactory<BeamFormerWMMAKernel>> factory_;
  std::unique_ptr<BeamFormerWMMAKernel> kernel_;

  std::array<size_t, 4> shape_input_;
  std::array<size_t, 4> shape_output_;
  std::array<size_t, 2> shape_delays_;
  std::array<size_t, 3> shape_weights_;

  std::unique_ptr<cu::HostMemory> host_input_;
  std::unique_ptr<cu::HostMemory> host_output_;
  std::unique_ptr<cu::HostMemory> host_delays_;
  std::unique_ptr<cu::HostMemory> host_weights_;
};

using TestTypes = std::tuple<half, float>;
TEMPLATE_LIST_TEST_CASE_METHOD(TestImpl, "BeamFormerWMMAKernel",
                               "[correctness]", TestTypes) {
  if (TestImpl<TestType>::skip_) {
    return;
  }

  const unsigned kNrStations = 16;
  const unsigned kNrChannels = 64;
  const unsigned kNrSamplesPerChannel = 64;
  const unsigned kNrTabs = 64;
  const unsigned kNrPolarizations = 2;

  TestImpl<TestType>::init(kNrStations, kNrChannels, kNrSamplesPerChannel,
                           kNrTabs, kNrPolarizations);

  // ***********************************************************
  // Test all, but cause reasonably "simple" reference output.
  // Set an unrealistic sb freq and bw, such that the ref output becomes a
  // simple, non-zero nr and the same for all channels. Make sure the mult of
  // (any channelFrequency, delay) ends up at an -(integer+(1/12)). The phi
  // will then be a 2N+pi/6, whose cos and sin are then "simple", non-zero.
  // The phase shift to (complex) mult the samples with is ( cos(phi),
  // sin(phi) ), possibly times some weight correction. Then sum over all
  // stations, i.e. all equal, so * NR_STATIONS.
  SECTION("Simple reference output") { TestImpl<TestType>::runTestReference(); }
}
