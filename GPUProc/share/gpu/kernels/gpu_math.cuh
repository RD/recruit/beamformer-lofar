#ifndef LOFAR_GPUPROC_CUDA_GPU_MATH_CUH
#define LOFAR_GPUPROC_CUDA_GPU_MATH_CUH

#include <cuda_fp16.h>

// \file
// Functions and operators for CUDA-specific types.
// This file contains functions and operators for CUDA-specific types, like
// float4. Only a minimal set of operators is provided, the ones that are
// currently needed. It can be extended when needed. We do \e not plan to
// provide a complete set of C++-operators for all the different CUDA types.

struct __device_builtin__ __builtin_align__(8) half4 { half x, y, z, w; };

inline __device__ float4 operator+(float4 a, float4 b) {
  return make_float4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
}

inline __device__ float4 operator-(float4 a, float4 b) {
  return make_float4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
}

inline __device__ float4 operator*(float4 a, float4 b) {
  return make_float4(a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w);
}

inline __device__ float4 operator/(float4 a, float4 b) {
  return make_float4(a.x / b.x, a.y / b.y, a.z / b.z, a.w / b.w);
}

inline __device__ float4 &operator+=(float4 &a, float4 b) {
  a.x += b.x;
  a.y += b.y;
  a.z += b.z;
  a.w += b.w;
  return a;
}

inline __device__ float4 &operator-=(float4 &a, float4 b) {
  a.x -= b.x;
  a.y -= b.y;
  a.z -= b.z;
  a.w -= b.w;
  return a;
}

inline __device__ float4 &operator*=(float4 &a, float4 b) {
  a.x *= b.x;
  a.y *= b.y;
  a.z *= b.z;
  a.w *= b.w;
  return a;
}

inline __device__ float4 &operator/=(float4 &a, float4 b) {
  a.x /= b.x;
  a.y /= b.y;
  a.z /= b.z;
  a.w /= b.w;
  return a;
}

inline __device__ half4 &operator+=(half4 &a, half4 b) {
  a.x += b.x;
  a.y += b.y;
  a.z += b.z;
  a.w += b.w;
  return a;
}

inline __device__ float2 &operator+=(float2 &a, float2 b) {
  a.x += b.x;
  a.y += b.y;
  return a;
}

inline __device__ half2 operator*(half a, half2 b) {
  return make_half2(a * b.x, a * b.y);
}

// to distinguish complex float/double from other uses of float2/double2
typedef half2 hcomplex;
typedef float2 fcomplex;
typedef double2 dcomplex;

typedef char2 char_complex;
typedef short2 short_complex;

// Operator overloads for complex values
//
// Do not implement a type with these, as it must be non-POD.
// This introduces redundant member inits in the constructor,
// causing races when declaring variables in shared memory.
inline __device__ fcomplex operator+(fcomplex a, fcomplex b) {
  return make_float2(a.x + b.x, a.y + b.y);
}

inline __device__ dcomplex operator+(dcomplex a, dcomplex b) {
  return make_double2(a.x + b.x, a.y + b.y);
}

inline __device__ fcomplex operator-(fcomplex a, fcomplex b) {
  return make_float2(a.x - b.x, a.y - b.y);
}

inline __device__ dcomplex operator-(dcomplex a, dcomplex b) {
  return make_double2(a.x - b.x, a.y - b.y);
}

inline __device__ fcomplex operator*(fcomplex a, fcomplex b) {
  return make_float2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

inline __device__ dcomplex operator*(dcomplex a, dcomplex b) {
  return make_double2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

inline __device__ fcomplex operator*(fcomplex a, float b) {
  return make_float2(a.x * b, a.y * b);
}

inline __device__ dcomplex operator*(dcomplex a, double b) {
  return make_double2(a.x * b, a.y * b);
}

inline __device__ fcomplex operator*(float a, fcomplex b) {
  return make_float2(a * b.x, a * b.y);
}

inline __device__ dcomplex operator*(double a, dcomplex b) {
  return make_double2(a * b.x, a * b.y);
}

inline __device__ dcomplex dphaseShift(double frequency, double delay) {
  // Convert the fraction of sample duration (delayAtBegin/delayAfterEnd) to
  // fractions of a circle. Because we `undo' the delay, we need to rotate BACK.
  //
  // This needs to be done in double precision, because phi may become
  // large after we multiply a small delay and a very large freq,
  // Then we need to compute a good sin(phi) and cos(phi).
  double phi =
      -2.0 * delay * frequency; // -2.0 * M_PI: M_PI below in sincospi()

  dcomplex rv;
  sincospi(phi, &rv.y, &rv.x); // store (cos(), sin())
  return rv;
}

// Math definitions needed for NVRTC compilation
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#ifndef M_PI_F
#define M_PI_F 3.1415927f
#endif

inline __device__ fcomplex fphaseShift(float frequency, float delay) {
  // Convert the fraction of sample duration (delayAtBegin/delayAfterEnd) to
  // fractions of a circle. Because we `undo' the delay, we need to rotate BACK.
  float phi = -2.0f * delay * frequency; // -2.0f, since M_PI_F is handled below

  // Reduce phi modulo 2 to reduce large argument errors
  phi = remainderf(phi, 2.0f);

  fcomplex rv;
  __sincosf(phi * M_PI_F, &rv.y, &rv.x);

  return rv; // rv.x = cosf(phi), rv.y = sinf(phi)
}

// Functions implemented with inline PTX to force the
// the compiler to use the desired instructions.

// multiply–accumulate: a += b * c
inline __device__ void mac(float &a, float b, float c) {
  asm("fma.rn.f32 %0,%1,%2,%3;" : "=f"(a) : "f"(b), "f"(c), "f"(a));
}

// Complex multiply–accumulate: a += b * c
inline __device__ void cmac(fcomplex &a, fcomplex b, fcomplex c) {
  asm("fma.rn.f32 %0,%1,%2,%3;" : "=f"(a.x) : "f"(b.x), "f"(c.x), "f"(a.x));
  asm("fma.rn.f32 %0,%1,%2,%3;" : "=f"(a.y) : "f"(b.x), "f"(c.y), "f"(a.y));
  asm("fma.rn.f32 %0,%1,%2,%3;" : "=f"(a.x) : "f"(-b.y), "f"(c.y), "f"(a.x));
  asm("fma.rn.f32 %0,%1,%2,%3;" : "=f"(a.y) : "f"(b.y), "f"(c.x), "f"(a.y));
}

inline __device__ void cmac(fcomplex &a, hcomplex b, hcomplex c) {
  a.x += __half2float(b.x * c.x - b.y * c.y);
  a.y += __half2float(b.x * c.y + b.y * c.x);
}

#endif
