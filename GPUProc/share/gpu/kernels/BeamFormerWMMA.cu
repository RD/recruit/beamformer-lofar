#include <mma.h>

#include "gpu_math.cuh"

using namespace nvcuda;

#if NR_INPUT_BITS == 16
using TdeviceInput = hcomplex;
using TdeviceOutput = fcomplex;
using TsharedInput = half;
using TsharedOutput = float; // same type as accumulator
using TcoresFrag = half;
using TcoresAcc = float; // half is also supported
#elif NR_INPUT_BITS == 32
using TdeviceInput = fcomplex;
using TdeviceOutput = fcomplex;
using TsharedInput = float;
using TsharedOutput = float;
using TcoresFrag = wmma::precision::tf32;
using TcoresAcc = float;
#else
#error unsupported NR_INPUT_BITS
#endif

// number of tiles in row/column per block
#define BLOCK_ROW_TILES (WARP_ROW_TILES * BLOCK_ROW_WARPS) // 4
#define BLOCK_COL_TILES (WARP_COL_TILES * BLOCK_COL_WARPS) // 4
// total size of the tiles in row/column per block
#ifndef M_TILES
#define M_TILES (WARP_ROW_TILES * BLOCK_ROW_WARPS * WMMA_M) // 4*16=64
#endif
#ifndef N_TILES
#define N_TILES (WARP_COL_TILES * BLOCK_COL_WARPS * WMMA_N) // 4*16=64
#endif
// warps and threads per block
#define WARPS_PER_BLOCK (BLOCK_ROW_WARPS * BLOCK_COL_WARPS) // 8
#define THREADS_PER_BLOCK (WARP_SIZE * WARPS_PER_BLOCK)     // 256

#ifndef NR_CHANNELS
#error NR_CHANNELS not defined
#endif
#ifndef NR_BEAMS
#error NR_BEAMS not defined
#endif
#ifndef NR_STATIONS
#error NR_STATIONS not defined
#endif
#ifndef NR_SAMPLES
#error NR_SAMPLES not defined
#endif

// weights[channel][beam][station]
typedef TdeviceInput (*weights_t)[NR_CHANNELS][NR_BEAMS][NR_STATIONS];
// samples[channel][polarization][station][sample]
typedef TdeviceInput (
    *samples_t)[NR_CHANNELS][NR_POLARIZATIONS][NR_STATIONS][NR_SAMPLES];
// total[channel][polarization][beam][sample]
typedef TdeviceOutput (
    *total_t)[NR_CHANNELS][NR_POLARIZATIONS][NR_BEAMS][NR_SAMPLES];

typedef TsharedInput *sh_weights_t;
typedef TsharedInput *sh_samples_t;
typedef TsharedOutput *sh_total_t;

extern "C" {

// clang-format off
__global__ void wmma_gemm(TdeviceInput *weightsPtr, TdeviceInput *samplesPtr, TdeviceOutput *totalPtr) {

  weights_t weights = reinterpret_cast<weights_t>(weightsPtr);
  samples_t samples = reinterpret_cast<samples_t>(samplesPtr);
  total_t total = reinterpret_cast<total_t>(totalPtr);

  const int channel = blockIdx.z / NR_POLARIZATIONS;
  const int polarization = blockIdx.z % NR_POLARIZATIONS;

  // 4x2 warp tile
  const int warpRow = threadIdx.y; // assuming block size (32,4,2) ==> 0,1,2,3
  const int warpCol = threadIdx.z; // assuming block size (32,4,2) ==> 0,1

  // Declare the fragments
  wmma::fragment<wmma::matrix_a, WMMA_M, WMMA_N, WMMA_K, TcoresFrag, wmma::row_major> a_frag;
  wmma::fragment<wmma::matrix_b, WMMA_M, WMMA_N, WMMA_K, TcoresFrag, wmma::row_major> b_frag;
  // multiplication parts ac, bd, ad, bc in  (a + ib) * (c + id) = ac - bd + i(ad + bc)
  wmma::fragment<wmma::accumulator, WMMA_M, WMMA_N, WMMA_K, TcoresAcc> acc_frag_re[WARP_ROW_TILES][WARP_COL_TILES]; // 2x1
  wmma::fragment<wmma::accumulator, WMMA_M, WMMA_N, WMMA_K, TcoresAcc> acc_frag_im[WARP_ROW_TILES][WARP_COL_TILES]; // 2x1

  // Initialize the output to zero
#pragma unroll
  for (int i = 0; i < WARP_ROW_TILES; i++) { // 0,1
#pragma unroll
    for (int j = 0; j < WARP_COL_TILES; j++) { // 0
      wmma::fill_fragment(acc_frag_re[i][j], 0.0f);
      wmma::fill_fragment(acc_frag_im[i][j], 0.0f);
    }
  }

  // M_TILES=64, M_TILES=64, WMMA_K=16
  __shared__ TsharedInput sh_weights_re[M_TILES * WMMA_K]; // 64x16
  __shared__ TsharedInput sh_weights_im[M_TILES * WMMA_K]; // 64x16
  __shared__ TsharedInput sh_samples_re[WMMA_K * N_TILES]; // 16x64
  __shared__ TsharedInput sh_samples_im[WMMA_K * N_TILES]; // 16x64
  __shared__ TsharedOutput sh_total_re[M_TILES * N_TILES]; // 64x64
  __shared__ TsharedOutput sh_total_im[M_TILES * N_TILES]; // 64x64
  
  typedef TsharedInput (*sh_weights_wmma_t)[WARP_ROW_TILES][WMMA_M][WMMA_K];
  sh_weights_wmma_t sh_weights_wmma_re = reinterpret_cast<sh_weights_wmma_t>(sh_weights_re);
  sh_weights_wmma_t sh_weights_wmma_im = reinterpret_cast<sh_weights_wmma_t>(sh_weights_im);
  typedef TsharedInput (*sh_samples_wmma_t)[BLOCK_COL_WARPS][WARP_COL_TILES][WMMA_N];
  sh_samples_wmma_t sh_samples_wmma_re = reinterpret_cast<sh_samples_wmma_t>(sh_samples_re);
  sh_samples_wmma_t sh_samples_wmma_im = reinterpret_cast<sh_samples_wmma_t>(sh_samples_im);
  typedef TsharedOutput (*sh_total_wmma_t)[WARP_ROW_TILES][WMMA_M][BLOCK_COL_WARPS][WARP_COL_TILES][WMMA_N];
  sh_total_wmma_t sh_total_wmma_re = reinterpret_cast<sh_total_wmma_t>(sh_total_re);
  sh_total_wmma_t sh_total_wmma_im = reinterpret_cast<sh_total_wmma_t>(sh_total_im);

  // 0,..,255 on a (32,4,2) sized thread block
  const int t = threadIdx.x + blockDim.x * (threadIdx.y + blockDim.y * threadIdx.z);

  // Loop over k (input data)
  for (int k = 0; k < NR_STATIONS; k += WMMA_K) { // 0,16,32,...

    // Create weights chunks
    {
      // t = 0,..,255, M_TILES=64, WMMA_K=16
#pragma unroll
      for (int i = t; i < M_TILES * WMMA_K; i += THREADS_PER_BLOCK) {
        int tx = i / WMMA_K; // 0,..,15
        int ty = i % WMMA_K; // 0,..,15
        int dx = M_TILES * blockIdx.x; // 0,64,128,...
        int dy = k;                    // 0,15,31,...
        int beam = dx + tx;
        int station = dy + ty;
        TdeviceInput weightValue = (*weights)[channel][beam][station];
        sh_weights_re[i] = weightValue.x;
        sh_weights_im[i] = weightValue.y;
      }
    }

    // Create sample chunks
    {
      // t = 0,..,255, M_TILES=64, WMMA_K=16
#pragma unroll
      for (int i = t; i < WMMA_K * N_TILES; i += THREADS_PER_BLOCK) {
        int tx = i / N_TILES; // 0,..,3
        int ty = i % N_TILES; // 0,..,63
        // samples chunk 16x64 data tile
        int sx = k;                     // 0,15,31,..
        int sy = N_TILES * blockIdx.y;  // 0,64,128,..
        int station = sx + tx;
        int sample = sy + ty;
        TdeviceInput sampleValue = (*samples)[channel][polarization][station][sample];
        sh_samples_re[i] = sampleValue.x;
        sh_samples_im[i] = sampleValue.y;
      }
    }

    __syncthreads();

    // complex GEMM
    {

#pragma unroll
      for (int i = 0; i < WARP_ROW_TILES; i++) { // 0
#pragma unroll
        for (int j = 0; j < WARP_COL_TILES; j++) { // 0,1
          // Load the input and perform the complex matrix multiplication
          // multiplication parts ac, bd, ad, bc in  (a + ib) * (c + id) = ac - bd + i(ad + bc)
          wmma::load_matrix_sync(a_frag, &sh_weights_wmma_re[warpRow][i][0][0], WMMA_K); // a
          wmma::load_matrix_sync(b_frag, &sh_samples_wmma_re[0][warpCol][j][0], N_TILES); // c
          wmma::mma_sync(acc_frag_re[i][j], a_frag, b_frag, acc_frag_re[i][j]); // ac
          wmma::load_matrix_sync(a_frag, &sh_weights_wmma_im[warpRow][i][0][0], WMMA_K); // b
          wmma::mma_sync(acc_frag_im[i][j], a_frag, b_frag, acc_frag_im[i][j]); // bc

#pragma unroll
          for (int e = 0; e < a_frag.num_elements; e++) { // 0,..,15
            a_frag.x[e] *= -1.0f; // -b (minus sign here!)
          }

          wmma::load_matrix_sync(b_frag, &sh_samples_wmma_im[0][warpCol][j][0], N_TILES); // d
          wmma::mma_sync(acc_frag_re[i][j], a_frag, b_frag, acc_frag_re[i][j]); // -db
          wmma::load_matrix_sync(a_frag, &sh_weights_wmma_re[warpRow][i][0][0], WMMA_K); // a
          wmma::mma_sync(acc_frag_im[i][j], a_frag, b_frag, acc_frag_im[i][j]); // ad
        }
      }
    }

    __syncthreads();

  } // for k

  // Store the output
#pragma unroll
  for (int i = 0; i < WARP_ROW_TILES; i++) { // 0,1
#pragma unroll
    for (int j = 0; j < WARP_COL_TILES; j++) { // 0
      wmma::store_matrix_sync(&sh_total_wmma_re[warpRow][i][0][warpCol][j][0],
                              acc_frag_re[i][j], N_TILES, wmma::mem_row_major);
      wmma::store_matrix_sync(&sh_total_wmma_im[warpRow][i][0][warpCol][j][0],
                              acc_frag_im[i][j], N_TILES, wmma::mem_row_major);
    }
  }

  __syncthreads();

  {
    // t = 0,..,255, M_TILES=64, N_TILES=64
#pragma unroll
    for (int i = t; i < M_TILES * N_TILES; i += THREADS_PER_BLOCK) {
      int tx = i / N_TILES; // 0,..,3
      int ty = i % N_TILES; // 0,..,63
      int tlx = M_TILES * blockIdx.x; // 0,64,128,..
      int tly = N_TILES * blockIdx.y; // 0,64,128,..
      int beam = tlx + tx;
      int sample = tly + ty;
      TdeviceOutput totalValue;
      totalValue.x = sh_total_re[i];
      totalValue.y = sh_total_im[i];
      (*total)[channel][polarization][beam][sample] = totalValue;
    }
  }
}

} // extern "C"
