#include <cooperative_groups.h>

#include "reduction.cuh"

namespace cg = cooperative_groups;

#if !(NR_CHANNELS >= 1)
#error Precondition violated: NR_CHANNELS >= 1
#endif

#if !(NR_COHERENT_STOKES == 1 || NR_COHERENT_STOKES == 4)
#error Precondition violated: NR_COHERENT_STOKES == 1 || NR_COHERENT_STOKES == 4
#endif

#if !(NR_SAMPLES_PER_CHANNEL > 0)
#error Precondition violated: NR_SAMPLES_PER_CHANNEL > 0
#endif

#if !(NR_TABS >= 1)
#error Precondition violated: NR_TABS >= 1
#endif

#if (NR_QUANTIZE_BITS == 16)
#define UINT unsigned short int
#define INT signed short int
#elif (NR_QUANTIZE_BITS == 8)
#define UINT unsigned char
#define INT signed char
#else
#error Precondition violated: invalid value for NR_QUANTIZE_BITS
#endif

// A shared memory buffer is used to cache the input data
// for a single tab, stokes parameter and channel:
// size = 12288 * sizeof(float) / 1024 = 48 Kb
// This size matches the amount of shared memory available
// on CUDA 3.0 capable devices onwards and should therefore
// not be exceeded.
// In case NR_SAMPLES_PER_CHANNEL > BATCH_SIZE, the input
// is processed in batches.
#define MAX_BATCH_SIZE 6144
#define BATCH_SIZE                                                             \
  NR_SAMPLES_PER_CHANNEL < MAX_BATCH_SIZE ? NR_SAMPLES_PER_CHANNEL             \
                                          : MAX_BATCH_SIZE

// 4D input array of stokes values. Each sample contains 1 or 4 stokes
// parameters. For each tab, there are NR_COHERENT_STOKES timeseries of channels
typedef float (*InputDataType)[NR_TABS][NR_COHERENT_STOKES][NR_CHANNELS]
                              [NR_SAMPLES_PER_CHANNEL];

// The output consists of three arrays stored in one buffer:
//  -  scales[NR_TABS][NR_COHERENT_STOKES][NR_CHANNELS]
//  - offsets[NR_TABS][NR_COHERENT_STOKES][NR_CHANNELS]
//  -  values[NR_TABS][NR_COHERENT_STOKES][NR_SAMPLES_PER_CHANNEL][NR_CHANNELS]
// The scales and offsets are floating-point numbers:
typedef float FloatArray[NR_TABS][NR_COHERENT_STOKES][NR_CHANNELS];
// The values are signed integer or unsigned integers with NR_QUANTIZE_BITS
// bits:
typedef INT IntArray[NR_TABS][NR_COHERENT_STOKES][NR_SAMPLES_PER_CHANNEL]
                    [NR_CHANNELS];
// when writing unsigned integers, cast the array to an unsigned integer
// pointer.

/*!
 * Copy n elements from src to dst, with the possibility
 * to specify input and output strides.
 */
template <typename T>
inline __device__ void copy_data(cg::thread_group group, T *dst, T *src,
                                 size_t n, size_t istride = 1,
                                 size_t ostride = 1) {
  unsigned lane = group.thread_rank();

  for (unsigned i = lane; i < n; i += group.size()) {
    if (i < n) {
      dst[i * (ostride - 1) + i] = src[i * (istride - 1) + i];
    }
  }

  group.sync();
}

__device__ void compute_stats(cg::thread_group group, float *d_data,
                              float *s_data, size_t batch_size, size_t n,
                              float *mean, float *std) {
  unsigned current_nr_samples = batch_size;

  float sum = 0.0f;
  float sq_sum = 0.0f;

  // Compute partial sum and squared sum for batch_size samples at once
  for (unsigned i = 0; i < n; i += batch_size) {
    current_nr_samples = n - i < batch_size ? n - i : batch_size;

    group.sync();

    // Load samples from device memory to shared memory
    copy_data<float>(group, s_data, d_data + i, current_nr_samples);

    // Determine middle of the s_data buffer
    unsigned middle = (current_nr_samples + 1) / 2;

    // Perform first step in reduction:
    //  - store partial sums in s_data[0:middle]
    //  - store partial squared sums in s_data[middle:n]
    for (unsigned lane = group.thread_rank(); lane < middle;
         lane += group.size()) {
      float sum = s_data[lane] + s_data[lane + middle];
      s_data[lane] = sum;
      s_data[lane + middle] = sum * sum;
    }

    group.sync();

    // Perform remainder of reduction:
    //  - compute sum in s_data[0]
    //  - computes squared sum in s_data[middle]
    sum += reduce_sum(group, s_data, middle, group.size());
    sq_sum += reduce_sum(group, s_data + middle, middle, group.size());
  }

  // Compute mean
  *mean = sum / n;

  // Compute variance
  float variance = sq_sum / n - *mean * *mean;

  // Compute standard deviation
  *std = sqrtf(variance);
}

/*!
 * Bring the value of x in the domain [a:b]
 */
__device__ int clamp(int x, int a, int b) { return max(a, min(b, x)); }

/* clang-format off
 * Digitizes the 32-bit floating-point input data to scaled and offset integers (i.e. 16-bit or 8-bit)
 * Read one block of samples (one TAB, one channel) for either Complex Voltages (XXYY) or
 * Coherent/Incoherent Stokes.
 *
 * For Coherent and Incoherent Stokes the Stokes I values (sum of two polarizations) will not
 * have a zero mean. Hence, the scale and offset is determined from the mean and the standard deviation.
 * Stokes Q, U and V are differences between two polarizations and will have a zero mean. Still, the
 * scale and offset are also determined from the mean and standard deviation.
 * For Complex Voltages, each of the four values of XXYY (real and imaginary of X and Y) will also have zero mean.
 *
 * The user can specify (smin, smax) as scale = (smax - smin) * std(block)
 * For non-zero mean: offset = mean(block) + smin * std(block)
 * For zero mean: offset = smin * std(block)
 *
 * The kernel maps blocks of data to thread blocks in round-robin fashion.
 *
 * Threads in a thread block are mapped to samples based on their rank within the thread block.
 *
 * \param[out] output
 *             pointer to a buffer for output data. This buffer will be populated with the digitized
 *             data (signed integers for zero-mean, unsigned integers for non-zero mean), scales
 *             and offsets. The number of bits for the signed and unsigned integers is compile-time
 *             constant (\c NR_QUANTIZE_BITS). The scales and offsets are 32-bit floating-point numbers.
 * \param[in]  input
 *             4D output array of stokes values. Each sample contains 1 or 4
 *             stokes paramters. For each tab, there are \c NR_COHERENT_STOKES
 *             time series of channels. The dimensions are: \c NR_TABS, \c
 *             NR_COHERENT_STOKES,
 *             <tt>(NR_SAMPLES_PER_CHANNEL)</tt>, \c NR_CHANNELS.
 * \param[in]  smax
 *             digitize/map raw data from [-smin*std(block), smax*std(block]
 *             outliers are clipped
 * \param[in]  smin
 *             digitize/map raw data from [-smin*std(block), smax*std(block]
 *             outliers are clipped
 *
 * Pre-processor input symbols (some are tied to the execution configuration)
 * Symbol                  | Valid Values  | Description
 * ----------------------- | ------------- | -----------
 * NR_CHANNELS             | >= 1          | number of frequency channels per subband
 * NR_COHERENT_STOKES      | 1 or 4        | number of stokes paramters to create
 * COMPLEX_VOLTAGES        | 1 or 0        | whether we compute complex voltages or coherent stokes
 * NR_SAMPLES_PER_CHANNEL  | >= 4          | number of input samples per channel
 * NR_TABS                 | >= 1          | number of tabs to create
 * NR_QUANTIZE_BITS        | 8, 16         | number of bits used for the quantization
 * STOKES_I_POSITIVE       | 1 or 0        | if 1, and if smin<0, override and set smin=0 for stokes I
 *
 * Every block of data will require sizeof(INT) + 2*sizeof(float),
 * therefore NR_SAMPLES_PER_CHANNEL should be at least 5 (in case of 16-bit ouput) to
 * make sure that the size of the digitized output is smaller than the size of the input.
 *
 * clang-format on */
extern "C" __launch_bounds__(NR_THREADS_PER_BLOCK, 2) __global__
    void quantizeOutput(size_t output, const InputDataType input, float smax,
                        float smin) {

  // Pointers to output
  FloatArray *scale_ptr = (FloatArray *)output;
  FloatArray *offset_ptr =
      (FloatArray *)((size_t)scale_ptr + sizeof(FloatArray));
  IntArray *value_ptr = (IntArray *)((size_t)offset_ptr + sizeof(FloatArray));

  __shared__ float s_data[BATCH_SIZE];

  // Setup thread group
  cg::thread_block block = cg::this_thread_block();
  int lane = block.thread_rank();

  for (unsigned i = blockIdx.x;
       i < (NR_TABS * NR_COHERENT_STOKES * NR_CHANNELS); i += gridDim.x) {
    unsigned tab_idx = i / (NR_COHERENT_STOKES * NR_CHANNELS);
    unsigned block_idx = i % (NR_COHERENT_STOKES * NR_CHANNELS);
    unsigned stokes_idx = block_idx / NR_CHANNELS;
    unsigned channel_idx = block_idx % NR_CHANNELS;

    if (tab_idx >= NR_TABS || stokes_idx >= NR_COHERENT_STOKES) {
      return;
    }

    // Determine the type of quantization to apply
    bool stokes_quv = (stokes_idx > 0);
    bool zero_mean = (COMPLEX_VOLTAGES || stokes_quv);

    // Get pointer to first sample for current tab, stokes and channel
    float *d_input = &((*input)[tab_idx][stokes_idx][channel_idx][0]);

    // Compute mean and standard deviation
    float mean = 0;
    float std = 0;
    compute_stats(block, d_input, s_data, BATCH_SIZE, NR_SAMPLES_PER_CHANNEL,
                  &mean, &std);

    // Set mean to zero for zero-mean data
    if (zero_mean) {
      mean = 0.0f;
    }

    // In Stokes IQUV mode, I (unsigned, 0..inf) and QUV (signed, -inf..inf)
    // Make sure that the full range is used, if smin<0 therefore:
    //  - for Stokes I, map the values to (0:std*smax)
    //  - for Stokes QUV use the range (std*smin:std:smax)
#if (STOKES_I_POSITIVE == 1)
    bool unsigned_stokes_i = (!COMPLEX_VOLTAGES && !stokes_quv && smin < 0.0f);
    if (unsigned_stokes_i) {
      smin = 0.0f;
    }
#endif

    for (unsigned sample_idx = lane; sample_idx < NR_SAMPLES_PER_CHANNEL;
         sample_idx += block.size()) {
      // Compute scale and offset
      int xmin = 0;
      int xmax = (1 << NR_QUANTIZE_BITS) - 1;
#if (STOKES_I_POSITIVE == 1)
      float xoffset = (unsigned_stokes_i ? 0.0f : mean + smin * std);
#else
      float xoffset = mean + smin * std;
#endif
      float xscale = ((smax - smin) * std) / (xmax + 1);

      if (sample_idx < NR_SAMPLES_PER_CHANNEL) {
        // Get adress of sample (cast to either unsigned or signed integer
        // pointer below)
        void *d_output =
            &((*value_ptr)[tab_idx][stokes_idx][sample_idx][channel_idx]);

        // Convert to int
        float x = (*input)[tab_idx][stokes_idx][channel_idx][sample_idx];
        int x_int =
            clamp(__int2float_rd(((x - xoffset) / xscale) + 0.5f), xmin, xmax);

        // Shift value
        if (zero_mean) {
          xmax = 1 << (NR_QUANTIZE_BITS - 1);
          x_int -= xmax;
          xoffset += xmax * xscale;
        }

        // Store value
        if (zero_mean) {
          *((INT *)d_output) = (INT)x_int;
        } else {
          *((UINT *)d_output) = (UINT)x_int;
        }
      }

      // Store scale and offset for current block
      if (sample_idx == 0) {
        (*scale_ptr)[tab_idx][stokes_idx][channel_idx] = xscale;
        (*offset_ptr)[tab_idx][stokes_idx][channel_idx] = xoffset;
      }

    } // end for sample_idx

    block.sync();

  } // end for i
}
