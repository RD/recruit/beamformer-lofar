#include "gpu_math.cuh"

#define DELAY_INDEX(s) (delayIndices[s])

// # Typedefs used to map input data on arrays
typedef double (*DelaysType)[NR_DELAYS][NR_TABS];
#if NR_OUTPUT_BITS == 32
typedef float2 (*WeightsType)[NR_CHANNELS][NR_TABS][NR_OUTPUT_STATIONS];
#elif NR_OUTPUT_BITS == 16
typedef half2 (*WeightsType)[NR_CHANNELS][NR_TABS][NR_OUTPUT_STATIONS];
#endif // if NR_OUTPUT_BITS

extern "C" __global__ void beamFormerWeights(
    const void *weightsPtr, const void *delaysPtr,
    const unsigned
        *delayIndices, // lookup index for stations to use in delaysPtr
    double subbandFrequency) {
  WeightsType weights = (WeightsType)weightsPtr;

  unsigned channel = threadIdx.x;
  unsigned tab = blockIdx.x;
  unsigned station = blockIdx.y;

  DelaysType delays = (DelaysType)delaysPtr;

#if NR_CHANNELS == 1
  double frequency = subbandFrequency;
#else
  double frequency = subbandFrequency - 0.5 * SUBBAND_BANDWIDTH +
                     channel * (SUBBAND_BANDWIDTH / NR_CHANNELS);
#endif

  if (station < NR_OUTPUT_STATIONS && tab < NR_TABS) {
    double delay = (*delays)[DELAY_INDEX(station)][tab];

#if SINGLE_PRECISION
    fcomplex weight = fphaseShift(frequency, delay);
#else
    dcomplex weight = dphaseShift(frequency, delay);
#endif
#if NR_OUTPUT_BITS == 32
    (*weights)[channel][tab][station] = make_float2(weight.x, weight.y);
#elif NR_OUTPUT_BITS == 16
    (*weights)[channel][tab][station] = make_half2(weight.x, weight.y);
#endif
  }
}
