#include "gpu_math.cuh"

#if !(NR_INPUT_STATIONS == NR_TABS)
#error Precondition violated: NR_INPUT_STATIONS == NR_TABS
#endif

#define STATION_INDEX(s) (stationIndices[s])

// # Typedefs used to map input data on arrays
#if NR_INPUT_BITS == 32
typedef float2 (
    *BandPassCorrectedType)[NR_INPUT_STATIONS][NR_CHANNELS]
                           [NR_SAMPLES_PER_CHANNEL][NR_POLARIZATIONS];
#elif NR_INPUT_BITS == 16
typedef half2 (
    *BandPassCorrectedType)[NR_INPUT_STATIONS][NR_CHANNELS]
                           [NR_SAMPLES_PER_CHANNEL][NR_POLARIZATIONS];
#endif // if NR_INPUT_BITS
typedef float2 (*ComplexVoltagesType)[NR_CHANNELS][NR_SAMPLES_PER_CHANNEL]
                                     [NR_TABS][NR_POLARIZATIONS];

/* clang-format off
 * The flyseye kernel performs transpose of the data, nothing more.
 *
 * \param[out] complexVoltagesPtr      4D output array of beams. For each channel a number of Tied Array Beams time serires is created for two polarizations
 * \param[in]  samplesPtr              3D input array of samples. A time series for each station and channel pair. Each sample contains the 2 polarizations X, Y, each of complex float type.
 *
 * Pre-processor input symbols (some are tied to the execution configuration)
 * Symbol                  | Valid Values            | Description
 * ----------------------- | ----------------------- | -----------
 * NR_INPUT_STATIONS       | >= 1                    | number of antenna fields in the input
 * NR_SAMPLES_PER_CHANNEL  | >= 1                    | number of input samples per channel
 * NR_CHANNELS             | >= 1                    | number of frequency channels per subband
 * NR_TABS                 | >= 1                    | number of Tied Array Beams (old name: pencil beams) to create
 *
 * Note that this kernel assumes  NR_POLARIZATIONS == 2
 *
 * Execution configuration:
 * - LocalWorkSize = (NR_POLARIZATIONS, NR_TABS) Note that for full utilization
 * NR_TABS * NR_CHANNELS % 16 = 0. Also note that NR_CHANNELS should be set to 1
 * per block (i.e. a 2D block). To process N channels with M samples per
 * channel, launch N*M blocks.
 * clang-format on */
extern "C" __global__ void
flysEye(void *complexVoltagesPtr, const void *samplesPtr,
        const unsigned
            *stationIndices // lookup index for stations to use in samplesPtr
) {
  ComplexVoltagesType complexVoltages = (ComplexVoltagesType)complexVoltagesPtr;
  BandPassCorrectedType samples = (BandPassCorrectedType)samplesPtr;

  unsigned pol = threadIdx.x;
  unsigned tab = threadIdx.y;
  unsigned time = blockIdx.x;
  unsigned channel = blockIdx.y;

  if (tab < NR_TABS) {
    float2 sample;
#if NR_INPUT_BITS == 16
    half2 hsample = (*samples)[STATION_INDEX(tab)][channel][time][pol];
    sample = make_float2(hsample.x, hsample.y);
#elif NR_INPUT_BITS == 32
    sample = (*samples)[STATION_INDEX(tab)][channel][time][pol];
#endif
    (*complexVoltages)[channel][time][tab][pol] = sample;
  }
}
