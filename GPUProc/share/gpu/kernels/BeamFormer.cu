#include "gpu_math.cuh"

#define STATION_INDEX(s) (stationIndices[s])
#define DELAY_INDEX(s) (delayIndices[s])

#ifndef NR_INPUT_STATIONS
#define NR_INPUT_STATIONS NR_STATIONS
#endif
#ifndef NR_OUTPUT_STATIONS
#define NR_OUTPUT_STATIONS NR_STATIONS
#endif

// # Typedefs used to map input data on arrays
#if NR_DELAYS > 0
typedef double (*DelaysType)[NR_DELAYS][NR_TABS];
#else
#if NR_INPUT_BITS == 32
typedef float2 (*WeightsType)[NR_CHANNELS][NR_TABS][NR_OUTPUT_STATIONS];
#elif NR_INPUT_BITS == 16
typedef half2 (*WeightsType)[NR_CHANNELS][NR_TABS][NR_OUTPUT_STATIONS];
#endif
#endif
#if NR_INPUT_BITS == 32
typedef float2 (
    *BandPassCorrectedType)[NR_INPUT_STATIONS][NR_CHANNELS]
                           [NR_SAMPLES_PER_CHANNEL][NR_POLARIZATIONS];
#elif NR_INPUT_BITS == 16
typedef half2 (
    *BandPassCorrectedType)[NR_INPUT_STATIONS][NR_CHANNELS]
                           [NR_SAMPLES_PER_CHANNEL][NR_POLARIZATIONS];
#endif // if NR_INPUT_BITS
typedef float2 (*ComplexVoltagesType)[NR_CHANNELS][NR_SAMPLES_PER_CHANNEL]
                                     [NR_TABS][NR_POLARIZATIONS];

template <typename T>
__device__ void
loadSamples(unsigned channel, unsigned time, unsigned first_station,
            const unsigned *stationIndices, BandPassCorrectedType samples,
            T local_samples[NR_STATIONS_PER_PASS][NR_TIME_PER_PASS]) {
  const unsigned last_station =
      min(NR_STATIONS_PER_PASS, NR_OUTPUT_STATIONS - first_station);
  for (unsigned i = threadIdx.x + NR_POLARIZATIONS * threadIdx.y;
       i < last_station * NR_TIME_PER_PASS;
       i +=
       blockDim.y * NR_POLARIZATIONS) // Use blockdim steps: memmory access done
                                      // by more threads then we might have tabs
  {
    const unsigned t = i % NR_TIME_PER_PASS;
    const unsigned s = i / NR_TIME_PER_PASS;

    if (NR_SAMPLES_PER_CHANNEL % NR_TIME_PER_PASS == 0 ||
        time + t < NR_SAMPLES_PER_CHANNEL) {
      for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
        local_samples[0][i][pol] = (*samples)[STATION_INDEX(first_station + s)]
                                             [channel][time + t][pol];
      }
    }
  }
}

/* clang-format off
 * The beamformer kernel performs a complex weighted multiply-add of each sample
 * of the provided input
 *
 * \param[out] complexVoltagesPtr      4D output array of beams. For each channel a number of Tied Array Beams time serires is created for two polarizations
 * \param[in]  samplesPtr              3D input array of samples. A time series for each station and channel pair. Each sample contains the 2 polarizations X, Y, each of complex float type.
 * \param[in]  delaysOrWeightsPtr      NR_DELAYS >= 0: 3D input array of complex valued delays to be applied to the correctData samples. There is a delay for each Sub-Array Pointing, station, and Tied Array Beam triplet.
 *                                     NR_DELAYS == 0: 3D input array of complex valued weights to be applied to the correctData samples. There is a delay for each Sub-Array Pointing, channel, and station triplet.
 * \param[in]  delayIndices            1D input array of which stations to use out of delaysPtr, if a subset of the stations need to be beam formed
 * \param[in]  subbandFrequency        central frequency of the subband
 *
 * Pre-processor input symbols (some are tied to the execution configuration)
 * Symbol                  | Valid Values            | Description
 * ----------------------- | ----------------------- | -----------
 * NR_INPUT_STATIONS       | >= 1                    | number of antenna fields in the input
 * NR_OUTPUT_STATIONS      | >= 1                    | number of antenna fields selected for beamforming
 * NR_DELAYS               | >= 0                    | number of delays = number of antenna fields in the observation
 * NR_SAMPLES_PER_CHANNEL  | >= 1                    | number of input samples per channel
 * NR_CHANNELS             | >= 1                    | number of frequency channels per subband
 * NR_TABS                 | >= 1                    | number of Tied Array Beams (old name: pencil beams) to create
 * SUBBAND_BANDWIDTH       | double, multiple of NR_CHANNELS | Bandwidth of a subband in Hz
 * NR_STATIONS_PER_PASS    | >= 1                    | number of stations to beamform in a single pass over the input data
 *
 * Note that this kernel assumes  NR_POLARIZATIONS == 2
 *
 * In case NR_DELAYS >= 0, weights are computed (based on subbandFrequency and delays)
 * In case NR_DELAYS == 0, weights are read
 *
 * Execution configuration:
 * - LocalWorkSize = (NR_POLARIZATIONS, NR_TABS) Note that for full utilization
 * NR_TABS * NR_CHANNELS % 16 = 0. Also note that NR_CHANNELS should be set to 1
 * per block (i.e. a 2D block). To process N channels, launch N blocks.
 * clang-format on */
extern "C" __global__ void
beamFormer(void *complexVoltagesPtr, const void *samplesPtr,
           const unsigned *
               stationIndices, // lookup index for stations to use in samplesPtr
           const void *delaysOrWeightsPtr,
           const unsigned
               *delayIndices, // lookup index for stations to use in delaysPtr
           double subbandFrequency) {
  ComplexVoltagesType complexVoltages = (ComplexVoltagesType)complexVoltagesPtr;
  BandPassCorrectedType samples = (BandPassCorrectedType)samplesPtr;

  unsigned pol = threadIdx.x;
  unsigned tab = blockIdx.y * blockDim.y + threadIdx.y;
  unsigned channel = blockIdx.x;

#if NR_DELAYS > 0
  DelaysType delays = (DelaysType)delaysOrWeightsPtr;
#else
  WeightsType allWeights = (WeightsType)delaysOrWeightsPtr;
#endif

  // This struct is in shared memory because it is used by all threads in the
  // block
  __shared__ struct {
#if NR_INPUT_BITS == 32
    float2 samples[NR_STATIONS_PER_PASS][NR_TIME_PER_PASS][NR_POLARIZATIONS];
#elif NR_INPUT_BITS == 16
    half2 samples[NR_STATIONS_PER_PASS][NR_TIME_PER_PASS][NR_POLARIZATIONS];
#endif
  } _local;

#if NR_CHANNELS == 1
  double frequency = subbandFrequency;
#elif NR_DELAYS > 0
  double frequency = subbandFrequency - 0.5 * SUBBAND_BANDWIDTH +
                     channel * (SUBBAND_BANDWIDTH / NR_CHANNELS);
#endif

#pragma unroll
  for (unsigned first_station =
           0; // Step over data with NR_STATIONS_PER_PASS stride
       first_station < NR_OUTPUT_STATIONS;
       first_station +=
       NR_STATIONS_PER_PASS) { // this for loop spans the whole file
#if NR_INPUT_BITS == 32
    fcomplex weights[NR_STATIONS_PER_PASS];
#elif NR_INPUT_BITS == 16
    hcomplex weights[NR_STATIONS_PER_PASS];
#endif

    // Compute or load weights
#pragma unroll
    for (unsigned i = 0; i < NR_STATIONS_PER_PASS; i++) {
      // Number of station might be larger then 32:
      // We then do multiple passes to span all stations
      if (first_station + i < NR_OUTPUT_STATIONS && tab < NR_TABS) {
#if NR_DELAYS > 0
        double delay = (*delays)[DELAY_INDEX(first_station + i)][tab];
        dcomplex weight = dphaseShift(frequency, delay);
#if NR_INPUT_BITS == 32
        weights[i] = make_float2(weight.x, weight.y);
#elif NR_INPUT_BITS == 16
        weights[i] = make_half2(weight.x, weight.y);
#endif
#else
        weights[i] = (*allWeights)[channel][tab][first_station + i];
#endif
      }
    }

    // Loop over all the samples in time. Perform the addition for
    // NR_TIME_PER_PASS time steps.
    for (unsigned time = 0; time < NR_SAMPLES_PER_CHANNEL;
         time += NR_TIME_PER_PASS) {
      loadSamples(channel, time, first_station, stationIndices, samples,
                  _local.samples);
      __syncthreads();

      // Some threads are scheduled to fill the wave, so check for actual work.
      // Note that we can't simply call 'continue' for idle threads, as they
      // still need to participate in the sync_threads below
      if (tab < NR_TABS) {
        const unsigned last_time =
            NR_SAMPLES_PER_CHANNEL % NR_TIME_PER_PASS == 0
                ? NR_TIME_PER_PASS
                : min(NR_TIME_PER_PASS, NR_SAMPLES_PER_CHANNEL - time);
        for (unsigned t = 0; t < last_time; t++) {
          fcomplex sum =
              first_station == 0
                  ? // The first run the sum should be zero, otherwise we need
                    // to take the sum of the previous run
                  make_float2(0, 0)
                  : (*complexVoltages)[channel][time + t][tab][pol];

          // Calculate the weighted complex sum of the samples
#pragma unroll
          for (unsigned j = 0; j < NR_STATIONS_PER_PASS; j++) {
            if (first_station + (j + 1) <=
                NR_OUTPUT_STATIONS) { // Remember that the number of stations
                                      // might not be a multiple of 32. Skip if
                                      // station does not exist
#if NR_INPUT_BITS == 32
              fcomplex sample = _local.samples[j][t][pol];
              sum = sum + weights[j] * sample;
#elif NR_INPUT_BITS == 16
              hcomplex sample = _local.samples[j][t][pol];
              cmac(sum, weights[j], sample);
#endif
            }
          }

          // Write data to global mem
          (*complexVoltages)[channel][time + t][tab][pol] = sum;
        }
      }

      __syncthreads();
    }
  }
}
