#include <cooperative_groups.h>
namespace cg = cooperative_groups;

#include "gpu_math.cuh"

#if !(NR_TABS >= 1)
#error Precondition violated: NR_TABS >= 1
#endif

#if !(NR_TAPS == 16)
#error Precondition violated: NR_TAPS == 16
#endif

#if !(NR_SAMPLES_PER_CHANNEL > 0 && NR_SAMPLES_PER_CHANNEL % NR_TAPS == 0)
#error Precondition violated: NR_SAMPLES_PER_CHANNEL > 0 && NR_SAMPLES_PER_CHANNEL % NR_TAPS == 0
#endif

#if NR_BITS_PER_SAMPLE == 32
typedef float SampleType;
typedef float WeightType;
typedef float2 ComplexSampleType;
#elif NR_BITS_PER_SAMPLE == 16
typedef half SampleType;
typedef half WeightType;
typedef half2 ComplexSampleType;
#endif

#if !(NR_CHANNELS > 0 && NR_CHANNELS % 16 == 0)
#error Precondition violated: NR_CHANNELS > 0 && NR_CHANNELS % 16 == 0
#endif

#if !(NR_POLARIZATIONS == 2)
#error Precondition violated: NR_POLARIZATIONS == 2
#endif

#if !(COMPLEX == 2)
#error Precondition violated: COMPLEX == 2
#endif

#if ENABLE_FFT
#include <cufftdx.hpp>
using namespace cufftdx;
using ForwardFFT =
    decltype(Thread() + Size<NR_CHANNELS>() +
             Direction<fft_direction::forward>() + Type<fft_type::c2c>() +
             Precision<float>() + SM<__CUDA_ARCH__>());
#endif

typedef ComplexSampleType (
    *SampledDataType)[NR_TABS][NR_POLARIZATIONS][NR_SAMPLES_PER_CHANNEL]
                     [NR_CHANNELS];
typedef ComplexSampleType (
    *HistoryDataType)[NR_TABS][NR_TAPS - 1][NR_CHANNELS][NR_POLARIZATIONS];
typedef ComplexSampleType (
    *FilteredDataType)[NR_TABS][NR_POLARIZATIONS][NR_SAMPLES_PER_CHANNEL]
                      [NR_CHANNELS];
typedef const WeightType (*WeightsType)[NR_CHANNELS][NR_TAPS];

__device__ inline void loadWeights(cg::thread_block block,
                                   const WeightsType weightsData,
                                   WeightType *weights) {
  const unsigned channel = block.thread_rank();

  if (channel < NR_CHANNELS) {
    for (unsigned i = 0; i < NR_TAPS; i++) {
      weights[i] = (*weightsData)[channel][i];
    }
  }
}

__device__ inline void loadDelayLines(cg::thread_block block,
                                      const HistoryDataType historyData,
                                      ComplexSampleType *delayLines,
                                      unsigned tab, unsigned pol) {
  const unsigned channel = block.thread_rank();

  if (channel < NR_CHANNELS) {
    for (unsigned i = 0; i < NR_TAPS - 1; i++) {
      delayLines[i] = (*historyData)[tab][i][channel][pol];
    }
  }
}

__device__ inline void computeSums(cg::thread_block block,
                                   const SampledDataType sampledData,
                                   const WeightType *weights,
                                   ComplexSampleType *delayLines,
                                   ComplexSampleType *sums, unsigned tab,
                                   unsigned pol, unsigned time) {
  const unsigned channel = block.thread_rank();
  unsigned timeIdx = time;

  if (channel < NR_CHANNELS) {
    delayLines[NR_TAPS - 1] = (*sampledData)[tab][pol][timeIdx][channel];

#pragma unroll
    for (unsigned delayIdx = 0; delayIdx < NR_TAPS; delayIdx++) {
      sums[delayIdx] = weights[NR_TAPS - 1] * delayLines[delayIdx];

      timeIdx = time + delayIdx + 1;
      if (delayIdx < NR_TAPS - 1) {
        delayLines[delayIdx] = (*sampledData)[tab][pol][timeIdx][channel];
      }

#pragma unroll
      for (unsigned i = 0; i < NR_TAPS - 1; i++) {
        sums[delayIdx] += weights[NR_TAPS - (2 + i)] *
                          delayLines[(delayIdx + i + 1) % NR_TAPS];
      }
    }
  }
}

#if ENABLE_FFT
__device__ inline void forwardFFT(cg::thread_block block,
                                  FilteredDataType filteredData,
                                  const ComplexSampleType *sums,
                                  float2 shared_data[NR_TAPS][NR_CHANNELS],
                                  unsigned tab, unsigned pol, unsigned time) {
  using FFT = ForwardFFT;

  const unsigned tid = block.thread_rank();

  block.sync();

  if (tid < cufftdx::size_of<FFT>::value) {
    for (unsigned delayIdx = 0; delayIdx < NR_TAPS; delayIdx++) {
      shared_data[delayIdx][tid] = sums[delayIdx];
    }
  }

  if (tid < NR_TAPS) {
    FFT::value_type *thread_data =
        reinterpret_cast<FFT::value_type *>(&shared_data[tid][0]);

    FFT().execute(thread_data);

    for (unsigned i = 0; i < cufftdx::size_of<FFT>::value; i++) {
      (*filteredData)[tab][pol][time + tid][i] = shared_data[tid][i];
    }
  }
}
#endif

__device__ inline void storeFilteredData(cg::thread_block block,
                                         FilteredDataType filteredData,
                                         const ComplexSampleType *sums,
                                         unsigned tab, unsigned pol,
                                         unsigned time) {
  const unsigned channel = block.thread_rank();

  if (channel < NR_CHANNELS) {
    for (unsigned delayIdx = 0; delayIdx < NR_TAPS; delayIdx++) {
      (*filteredData)[tab][pol][time + delayIdx][channel] = sums[delayIdx];
    }
  }
}

__device__ inline void storeHistoryData(cg::thread_block block,
                                        SampledDataType sampledData,
                                        HistoryDataType historyData,
                                        unsigned tab, unsigned pol) {
  const unsigned channel = block.thread_rank();

  if (channel < NR_CHANNELS) {
    for (unsigned tap = 0; tap < NR_TAPS - 1; tap++) {
      const unsigned timeIdx = NR_SAMPLES_PER_CHANNEL - (NR_TAPS - 1) + tap;
      (*historyData)[tab][tap][channel][pol] =
          (*sampledData)[tab][pol][timeIdx][channel];
    }
  }
}

/* clang-format off
 * Applies the Finite Input Response filter defined by the weightsPtr array
 * to the sampledDataPtr array. Output is written into the filteredDataPtr
 * array. The filter works on complex numbers. The weights are real values only.
 *
 * Input values are first converted to (complex) float/half.
 * The kernel also reorders the polarization dimension and expects the weights
 * per channel in reverse order. If an FFT is applied afterwards, the weights
 * of the odd channels are often supplied negated to get the resulting channels
 * in increasing order of frequency.
 *
 * \param[out] filteredDataPtr         4D output array of floats/halfs
 * \param[in]  sampledDataPtr          4D input array of signed chars or shorts
 * \param[in]  weightsPtr              2D per-channel FIR filter coefficient array of floats/halfs
 * \param[in]  historyDataPtr          5D input array of history input samples needed to initialize the FIR filter
 *
 * Pre-processor input symbols (some are tied to the execution configuration)
 * Symbol                  | Valid Values                | Description
 * ----------------------- | --------------------------- | -----------
 * NR_TABS                 | >= 1                        | number of tight array beams (tabs)
 * NR_TAPS                 | 16                          | number of FIR filtering coefficients
 * NR_SAMPLES_PER_CHANNEL  | multiple of NR_TAPS and > 0 | number of input samples per channel
 * NR_BITS_PER_SAMPLE      | 16 or 32                    | number of bits of floating point value type of sampledDataPtr
 * NR_CHANNELS             | multiple of 16 and > 0      | number of frequency channels per subband
 * NR_POLARIZATIONS        | 2                           | number of polarizations
 * COMPLEX                 | 2                           | size of complex in number of floats/halfs
 * ENABLE_FFT              | 0 or 1                      | enable FFT after FIR filtering
 *
 * Execution configuration:
 *     + Inner dim: the channel the thread processes
 *     + Outer dim: the polarization,tab the thread processes
 * - Work group size: (NR_CHANNELS)
 * - Global size: (NR_POLARIZATIONS, NR_TABS)
 * clang-format on */
extern "C" {
__global__ void FIR_filter(void *filteredDataPtr, const void *sampledDataPtr,
                           const void *weightsPtr, void *historyDataPtr) {
  SampledDataType sampledData = (SampledDataType)sampledDataPtr;
  FilteredDataType filteredData = (FilteredDataType)filteredDataPtr;
  WeightsType weightsData = (WeightsType)weightsPtr;
  HistoryDataType historyData = (HistoryDataType)historyDataPtr;

  const unsigned pol = blockIdx.x;
  const unsigned tab = blockIdx.y;

  cg::thread_block block = cg::this_thread_block();

  WeightType weights[NR_TAPS];
  loadWeights(block, weightsData, weights);

  ComplexSampleType delayLines[NR_TAPS];
  loadDelayLines(block, historyData, delayLines, tab, pol);

  ComplexSampleType sums[NR_TAPS];

  storeHistoryData(block, sampledData, historyData, tab, pol);

  for (unsigned time = 0; time < NR_SAMPLES_PER_CHANNEL; time += NR_TAPS) {
    computeSums(block, sampledData, weights, delayLines, sums, tab, pol, time);

#if ENABLE_FFT
    __shared__ float2 shared_data[NR_TAPS][NR_CHANNELS];
    forwardFFT(block, filteredData, sums, shared_data, tab, pol, time);
#else
    storeFilteredData(block, filteredData, sums, tab, pol, time);
#endif
  } // end for time
}
}
