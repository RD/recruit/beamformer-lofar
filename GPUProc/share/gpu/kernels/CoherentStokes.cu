#include "gpu_math.cuh"

#if !(TIME_INTEGRATION_FACTOR >= 1)
#error Precondition violated: TIME_INTEGRATION_FACTOR >= 1
#endif

#if TIME_INTEGRATION_FACTOR > 32 && (TIME_INTEGRATION_FACTOR % 32) != 0
#error Precondition violated: TIME_INTEGRATION_FACTOR % 32 == 0
#endif

#if !(NR_CHANNELS >= 1)
#error Precondition violated: NR_CHANNELS >= 1
#endif

#if !(NR_COHERENT_STOKES == 1 || NR_COHERENT_STOKES == 4)
#error Precondition violated: NR_COHERENT_STOKES == 1 || NR_COHERENT_STOKES == 4
#endif

#if !(COMPLEX_VOLTAGES == 0 || NR_COHERENT_STOKES == 4)
#error Precondition violated: COMPLEX_VOLTAGES == 0 || NR_COHERENT_STOKES == 4
#endif

#if !(NR_POLARIZATIONS == 2)
#error Precondition violated: NR_POLARIZATIONS == 2
#endif

#if !(NR_SAMPLES_PER_CHANNEL > 0 &&                                            \
      NR_SAMPLES_PER_CHANNEL % TIME_INTEGRATION_FACTOR == 0)
#error Precondition violated: NR_SAMPLES_PER_CHANNEL > 0 && NR_SAMPLES_PER_CHANNEL % TIME_INTEGRATION_FACTOR == 0
#endif

#if !(NR_TABS >= 1)
#error Precondition violated: NR_TABS >= 1
#endif

#if (NR_BANDPASS_WEIGHTS > 0)
#if !(NR_BANDPASS_WEIGHTS > 1 && NR_BANDPASS_WEIGHTS < NR_CHANNELS)
#error Bandpass weights not in 1...NR_CHANNELS
#endif
#endif /* NR_BANDPASS_WEIGHTS */

#if NR_BITS_PER_SAMPLE == 32
typedef float SampleType;
typedef float2 ComplexSampleType;
typedef float4 QuadSampleType;
#elif NR_BITS_PER_SAMPLE == 16
typedef half SampleType;
typedef half2 ComplexSampleType;
typedef half4 QuadSampleType;
#endif
typedef float WeightType;

// 4D input array of complex samples. For each tab and polarization there are
// time lines with data for each channel
typedef ComplexSampleType (*InputDataType)[NR_TABS][NR_POLARIZATIONS]
                                          [NR_SAMPLES_PER_CHANNEL][NR_CHANNELS];

// 4D output array of stokes values. Each sample contains 1 or 4 stokes
// parameters. For each tab, there are NR_COHERENT_STOKES timeseries of channels
#if defined(OUTPUT_ORDER_SAMPLES_CHANNELS)
typedef SampleType (*OutputDataType)[NR_TABS][NR_COHERENT_STOKES]
                                    [NR_SAMPLES_PER_CHANNEL /
                                     TIME_INTEGRATION_FACTOR][NR_CHANNELS];
#elif defined(OUTPUT_ORDER_CHANNELS_SAMPLES)
typedef SampleType (
    *OutputDataType)[NR_TABS][NR_COHERENT_STOKES][NR_CHANNELS]
                    [NR_SAMPLES_PER_CHANNEL / TIME_INTEGRATION_FACTOR];
#else
#error Unknown layout for OutputDataType
#endif

inline __device__ QuadSampleType transform_input(const ComplexSampleType &x,
                                                 const ComplexSampleType &y) {
  QuadSampleType result;
#if COMPLEX_VOLTAGES
  result.x = x.x;
  result.y = x.y;
  result.z = y.x;
  result.w = y.y;
#else
  const SampleType powerX = x.x * x.x + x.y * x.y;
  const SampleType powerY = y.x * y.x + y.y * y.y;
  result.x = powerX + powerY;

#if NR_COHERENT_STOKES == 4
  result.y = powerX - powerY;
  const SampleType halfStokesU = x.x * y.x + x.y * y.y;
  const SampleType halfStokesV = x.y * y.x - x.x * y.y;
  result.z = (SampleType)2.0f * halfStokesU;
  result.w = (SampleType)2.0f * halfStokesV;
#endif
#endif
  return result;
}

__inline__ __device__ QuadSampleType warp_reduce_sum(QuadSampleType &value) {
  for (int i = 32 / 2; i > 0; i /= 2) {
    value.x += __shfl_xor_sync(0Xffffffff, value.x, i, 32);
    value.y += __shfl_xor_sync(0Xffffffff, value.y, i, 32);
    value.z += __shfl_xor_sync(0Xffffffff, value.z, i, 32);
    value.w += __shfl_xor_sync(0Xffffffff, value.w, i, 32);
  }
  return value;
}

/* clang-format off
 * Computes the Stokes I or IQUV, or outputs the 4 complex voltages (XrXiYrYi).
 * http://www.astron.nl/~romein/papers/EuroPar-11/EuroPar-11.pdf 
 * In case of Stokes:
 * \code
 * I = X * conj(X) + Y * conj(Y)
 * Q = X * conj(X) - Y * conj(Y)
 * U = 2 * real(X * con(Y))
 * V = 2 * imag(X * con(Y))
 * \endcode
 * This reduces to (validated on paper by Wouter and John):
 * \code
 * Px = real(X) * real(X) + imag(X) * imag(X)
 * Py = real(Y) * real(Y) + imag(Y) * imag(Y)
 * I = Px + Py
 * Q = Px - Py
 * U = 2 * (real(X) * real(Y) + imag(X) * imag(Y))
 * V = 2 * (imag(X) * real(Y) - real(X) * imag(Y))
 * \endcode
 * 
 * The kernel's first parallel dimension is in time; the second dimensions
 * is on the channels; the third on the tabs.
 *
 * The kernel has two paths:
 *  1) for time integration factors smaller than the warp size (32 threads)
 *   - every thread processes all the samples for a single channel and tab
 *   - thrads are assigned to samples in round-robin fashion
 *  2) for time integration factors that are a multiple of the warp size.
 *   - threads in a warp each process one sample and apply a reduction to
 *     achieve 32x time integration. The results are written to device
 *     memory directly.
 *   - when time integration is > 32, atomic operations are used to apply
 *     the final time integration in device memory
 *   - every thread processes one channel and NR_TABS_PER_THREAD tabs
 *
 * \param[out] output
 *             4D output array of stokes values. Each sample contains 1 or 4
 *             stokes paramters. For each tab, there are \c NR_COHERENT_STOKES
 *             time series of channels. The dimensions are: \c NR_TABS, \c
 *             NR_COHERENT_STOKES,
 *             <tt>(NR_SAMPLES_PER_CHANNEL / TIME_INTEGRATION_FACTOR)</tt>, \c
 *             NR_CHANNELS.
 * \param[in]  input
 *             4D input array of complex samples. For each tab and polarization
 *             there are time lines with data for each channel. The dimensions
 *             are: \c NR_TABS, \c NR_POLARIZATIONS, \c NR_SAMPLES_PER_CHANNEL,
 *             \c NR_CHANNELS
 * \param[in]  timeIntegrationFactor
 *             immediate value that indicates in how many sub-ranges the sample
 *             range (time) is split for independent processing. Must be >= 1 and:
 *             NR_SAMPLES_PER_CHANNEL / timeParallelFactor must be a multiple of
 *             timeIntegrationFactor.
 * \param[in]  bandPassFactorsPtr  pointer to bandpass correction data of
 *                                 ::BandPassFactorsType, a 1D array [NR_BANDPASS_WEIGHTS] of
 *                                 float/half, containing bandpass correction factors
 *
 * Pre-processor input symbols (some are tied to the execution configuration)
 * Symbol                  | Valid Values  | Description
 * ----------------------- | ------------- | -----------
 * TIME_INTEGRATION_FACTOR | >= 1          | amount of samples to integrate to a single output sample
 * NR_CHANNELS             | >= 1          | number of frequency channels per subband
 * NR_COHERENT_STOKES      | 1 or 4        | number of stokes paramters to create
 * COMPLEX_VOLTAGES        | 1 or 0        | whether we compute complex voltages or coherent stokes
 * NR_POLARIZATIONS        | 2             | number of polarizations
 * NR_SAMPLES_PER_CHANNEL  | multiple of TIME_INTEGRATION_FACTOR | number of input samples per channel
 * NR_BITS_PER_SAMPLE      | 16 or 32      | number of bits of floating point value type of input/output
 * NR_TABS                 | >= 1          | number of tabs to create
 * NR_BANDPASS_WEIGHTS | > 1 < NR_CHANNELS | length of bandPassFactorsPtr, repeats over the NR_CHANNELS of channels, if this is >0, apply bandpass correction
 * 
 * Execution configuration:
 * - LocalWorkSize = 1 dimensional; 1 warp (32 threads).
 * - GlobalWorkSize = 3 dimensional; depends on the size of \c
 *                    NR_SAMPLES_PER_CHANNEL, \c NR_CHANNELS,\c NR_TABS.
 * clang-format on */
extern "C" __global__ void
coherentStokes(OutputDataType output, const InputDataType input,
               const WeightType *bandPassFactorsPtr) {
#if TIME_INTEGRATION_FACTOR <= 32
  const unsigned int channel = blockIdx.y;
  const unsigned int tab = blockIdx.z;
  const unsigned int thread_idx_x = blockIdx.x * blockDim.x + threadIdx.x;
  const unsigned int sample_start = thread_idx_x * TIME_INTEGRATION_FACTOR;
  const unsigned int nr_time_parallel_threads = blockDim.x * gridDim.x;

#if (NR_BANDPASS_WEIGHTS > 0)
  const WeightType weight = bandPassFactorsPtr[channel % NR_BANDPASS_WEIGHTS];
#endif

  for (unsigned int sample = sample_start; sample < NR_SAMPLES_PER_CHANNEL;
       sample += TIME_INTEGRATION_FACTOR * nr_time_parallel_threads) {
    const unsigned int sample_out = sample / TIME_INTEGRATION_FACTOR;

    QuadSampleType stokes;
    stokes.x = 0;
    stokes.y = 0;
    stokes.z = 0;
    stokes.w = 0;

    for (unsigned int i = 0; i < TIME_INTEGRATION_FACTOR; i++) {
      const unsigned int sample_in = sample + i;
      ComplexSampleType X = (*input)[tab][0][sample_in][channel];
      ComplexSampleType Y = (*input)[tab][1][sample_in][channel];
#if (NR_BANDPASS_WEIGHTS > 0)
      X.x *= weight;
      X.y *= weight;
      Y.x *= weight;
      Y.y *= weight;
#endif
      stokes += transform_input(X, Y);
    }

    for (unsigned int i = 0; i < NR_COHERENT_STOKES; i++) {
      const float stoke = ((SampleType *)(&stokes.x))[i];
#if OUTPUT_ORDER_SAMPLES_CHANNELS
      SampleType *output_ptr = &(*output)[tab][i][sample_out][channel];
#elif OUTPUT_ORDER_CHANNELS_SAMPLES
      SampleType *output_ptr = &(*output)[tab][i][channel][sample_out];
#endif
      *output_ptr = stoke;
    }
  }
#else
  const unsigned int sample_block = blockIdx.x;
  const unsigned int channel = blockIdx.y;
  const unsigned int tab = blockIdx.z * NR_TABS_PER_THREAD;

  const unsigned int sample_in = sample_block * 32 + threadIdx.x;
  const unsigned int nr_blocks_per_sample = TIME_INTEGRATION_FACTOR / 32;
  const unsigned int sample_out = sample_block / nr_blocks_per_sample;

#if (NR_BANDPASS_WEIGHTS > 0)
  const float weight = bandPassFactorsPtr[channel % NR_BANDPASS_WEIGHTS];
#endif

  ComplexSampleType X[NR_TABS_PER_THREAD];
  ComplexSampleType Y[NR_TABS_PER_THREAD];

  for (unsigned int i = 0; i < NR_TABS_PER_THREAD; i++) {
    if ((tab + i) < NR_TABS) {
      X[i] = (*input)[tab + i][0][sample_in][channel];
      Y[i] = (*input)[tab + i][1][sample_in][channel];
#if (NR_BANDPASS_WEIGHTS > 0)
      X[i].x *= weight;
      X[i].y *= weight;
      Y[i].x *= weight;
      Y[i].y *= weight;
#endif
    } else {
      X[i].x = 0;
      X[i].y = 0;
      Y[i].x = 0;
      Y[i].y = 0;
    }
  }

  for (unsigned int i = 0; i < NR_TABS_PER_THREAD; i++) {
    if ((tab + i) < NR_TABS) {
      QuadSampleType stokes = transform_input(X[i], Y[i]);

      stokes = warp_reduce_sum(stokes);

      if (threadIdx.x < NR_COHERENT_STOKES) {
        const SampleType stoke = ((SampleType *)(&stokes.x))[threadIdx.x];
#if OUTPUT_ORDER_SAMPLES_CHANNELS
        SampleType *output_ptr =
            &(*output)[tab + i][threadIdx.x][sample_out][channel];
#elif OUTPUT_ORDER_CHANNELS_SAMPLES
        SampleType *output_ptr =
            &(*output)[tab + i][threadIdx.x][channel][sample_out];
#endif
        if (nr_blocks_per_sample > 1) {
          atomicAdd(output_ptr, stoke);
        } else {
          *output_ptr = stoke;
        }
      }
    }
  }
#endif
}
