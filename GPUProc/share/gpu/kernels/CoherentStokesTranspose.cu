#include <cooperative_groups.h>
namespace cg = cooperative_groups;

#include "gpu_math.cuh"

#if !(NR_SAMPLES_PER_CHANNEL >= 1)
#error Precondition violated: NR_SAMPLES_PER_CHANNEL >= 1
#endif

#if !(NR_TABS >= 1)
#error Precondition violated: NR_TABS >= 1
#endif

#if !(NR_CHANNELS >= 16)
#error Precondition violated: NR_CHANNELS >= 16
#endif

#if NR_BITS_PER_SAMPLE == 32
typedef float SampleType;
typedef float2 ComplexSampleType;
#else
typedef half SampleType;
typedef half2 ComplexSampleType;
#endif

#if ENABLE_FFT
#include <cufftdx.hpp>
using namespace cufftdx;
using FFT =
    decltype(Block() + Size<NR_CHANNELS>() +
             Direction<fft_direction::inverse>() + Type<fft_type::c2c>() +
             Precision<float>() + SM<__CUDA_ARCH__>());
__constant__ dim3 fft_block_dim = FFT::block_dim;
#endif

typedef ComplexSampleType (
    *OutputDataType)[NR_TABS][NR_POLARIZATIONS][NR_SAMPLES_PER_CHANNEL]
                    [NR_CHANNELS];

typedef ComplexSampleType (*InputDataType)[NR_CHANNELS][NR_SAMPLES_PER_CHANNEL]
                                          [NR_TABS][NR_POLARIZATIONS];

#if ENABLE_FFT
__device__ inline void inverseFFT(cg::thread_block block,
                                  OutputDataType outputData,
                                  float2 *shared_data, unsigned tab,
                                  unsigned pol, unsigned sample) {
  const unsigned tid = block.thread_rank();

  FFT::value_type thread_data[FFT::storage_size];

  if (tid < (cufftdx::size_of<FFT>::value / FFT::elements_per_thread)) {
    FFT::value_type *shared_data_ptr =
        reinterpret_cast<FFT::value_type *>(&shared_data[0]);

    constexpr unsigned int stride = FFT::stride;
    for (unsigned int i = 0; i < FFT::elements_per_thread; i++) {
      const unsigned int index = tid + i * stride;
      if (index < cufftdx::size_of<FFT>::value) {
        thread_data[i] = shared_data_ptr[index];
      }
    }

    FFT().execute(thread_data, &(shared_data[0]));

    for (unsigned int i = 0; i < FFT::elements_per_thread; i++) {
      const unsigned int index = tid + i * stride;
      if (index < cufftdx::size_of<FFT>::value) {
        const float2 element = make_float2(thread_data[i].x, thread_data[i].y);
        const signed factor = 1 - 2 * (index % 2);
#if NR_BITS_PER_SAMPLE == 16
        (*outputData)[tab][pol][sample][index] =
            __float22half2_rn(element * factor);
#else
        (*outputData)[tab][pol][sample][index] = element * factor;
#endif
      }
    }

    block.sync();
  }
}
#endif

/* clang-format off
 * Performs data transposition from the output of the beamformer kernel to
 * a data order suitable for an inverse FFT.
 * Parallelisation is performed over the TABs and number of samples (time).
 *
 *
 * \param[out] OutputDataType      4D output array of samples. For each TAB and pol, a spectrum per time step of complex floats.
 * \param[in]  InputDataType       3D input array of samples (last dim (pol) is implicit). For each channel, the TABs per time step of two complex floats.
 *
 * Pre-processor input symbols (some are tied to the execution configuration)
 * Symbol                  | Valid Values            | Description
 * ----------------------- | ----------------------- | -----------
 * NR_SAMPLES_PER_CHANNEL  | >= 1                    | number of input samples per channel
 * NR_CHANNELS             | multiple of 16 and > 0  | number of frequency channels per subband
 * NR_TABS                 | >= 1                    | number of Tied Array Beams to create, multiple 16 is optimal
 * NR_BITS_PER_SAMPLE      | 16 or 32                | number of bits of floating point value type of input/output
 * ENABLE_FFT              | 0 or 1                  | enable iFFT + FFT-shift after transpose
 *
 * Note that this kernel assumes  NR_POLARIZATIONS == 2
 *
 * Execution configuration:
 * - LocalWorkSize = 2 dimensional; (16, 16, 1) is in use.
 * - GlobalWorkSize = 3 dimensional:
 *   + inner dim (x): nr (( params.nrTABs + 16 - 1) / 16) * 16 
 *   + middle dim (y): nr samples ( /16)
 *   + outer dim (z): number of channels (/1)
 * clang-format on */
extern "C" __global__ void coherentStokesTranspose(void *OutputDataPtr,
                                                   const void *InputDataPtr) {
  OutputDataType outputData = (OutputDataType)OutputDataPtr;
  InputDataType inputData = (InputDataType)InputDataPtr;

  cg::thread_block block = cg::this_thread_block();

  const unsigned tid = block.thread_rank();
  const unsigned nr_threads = block.size();

  const unsigned tab = blockIdx.x;
  const unsigned sample = blockIdx.y;

  __shared__ ComplexSampleType shared_data[NR_POLARIZATIONS][NR_CHANNELS];

  for (unsigned channel = tid; channel < NR_CHANNELS; channel += nr_threads) {
    for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
      if (channel < NR_CHANNELS) {
        shared_data[pol][channel] = (*inputData)[channel][sample][tab][pol];
      }
    }
  }

  block.sync();

  for (unsigned pol = 0; pol < NR_POLARIZATIONS; pol++) {
#if ENABLE_FFT
    inverseFFT(block, outputData, &(shared_data[pol][0]), tab, pol, sample);
#else
    for (unsigned channel = tid; channel < NR_CHANNELS; channel += nr_threads) {
      if (channel < NR_CHANNELS) {
        (*outputData)[tab][pol][sample][channel] = shared_data[pol][channel];
      }
    }
#endif
  }
}
