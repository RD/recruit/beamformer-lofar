# Detect list of kernel sources to install. CMake cannot detect changes in this
# list, but for kernel sources it doesn't need to.
file(
  GLOB _kernel_sources
  RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"
  "gpu/kernels/*.cu" "gpu/kernels/*.cuh"
)
set(PACKAGE_NAME ${PROJECT_NAME})
foreach(_src_name ${_kernel_sources})
  set(_dest_name ${_src_name})
  get_filename_component(_abs_name ${_src_name} ABSOLUTE)
  get_filename_component(_dest_path ${_dest_name} PATH)
  file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/share/${_dest_path})
  file(CREATE_LINK ${_abs_name} ${CMAKE_BINARY_DIR}/share/${_dest_name}
       SYMBOLIC
  )
  install(FILES ${_src_name} DESTINATION share/${_dest_path})
endforeach()
