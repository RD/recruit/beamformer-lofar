#include <complex>
#include <iostream>
#include <memory>

#include <cuda_fp16.h>
#include <cudawrappers/cu.hpp>
#include <cxxopts.hpp>

#include <CoInterface/BlockID.h>
#include <CoInterface/Span.h>
#include <GPUProc/Kernels/BeamFormerCCGKernel.h>

#include "BenchmarkUtil.h"

namespace {
cxxopts::Options setupCommandLineParser(const char *argv[]) {
  cxxopts::Options options(argv[0], "Benchmark for BeamFormerCCGKernel");

  const unsigned kNrStations = 64;
  const unsigned kNrChannels = 256;
  const unsigned kNrSamplesPerChannel = 768;
  const unsigned kNrPolarizations = 2;
  const unsigned kNrTabs = 64;
  const unsigned kNrInputBits = 32;
  const unsigned kDeviceId = 0;

  options.add_options()(
      "nr_stations", "Number of stations",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrStations)))(
      "nr_channels", "Number of channels",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrChannels)))(
      "nr_samples", "Number of samples per channel",
      cxxopts::value<unsigned>()->default_value(std::to_string(
          kNrSamplesPerChannel)))("nr_polarizations", "Number of polarizations",
                                  cxxopts::value<unsigned>()->default_value(
                                      std::to_string(kNrPolarizations)))(
      "nr_tabs", "Number of TABs",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrTabs)))(
      "nr_input_bits", "Number of input bits",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrInputBits)))(
      "device_id", "Device ID",
      cxxopts::value<unsigned>()->default_value(std::to_string(kDeviceId)))(
      "h,help", "Print help");

  return options;
}

cxxopts::ParseResult getCommandLineOptions(int argc, const char *argv[]) {
  cxxopts::Options options = setupCommandLineParser(argv);

  try {
    cxxopts::ParseResult result = options.parse(argc, argv);

    if (result.count("help")) {
      std::cout << options.help() << std::endl;
      exit(EXIT_SUCCESS);
    }

    return result;

  } catch (const cxxopts::exceptions::exception &e) {
    std::cerr << "Error parsing command-line options: " << e.what()
              << std::endl;
    exit(EXIT_FAILURE);
  }
}
} // namespace

using namespace LOFAR::Cobalt;

class Settings : public BenchmarkFixture::SettingsBase {
public:
  Settings(cxxopts::ParseResult &result) {
    nr_stations = result["nr_stations"].as<unsigned>();
    nr_channels = result["nr_channels"].as<unsigned>();
    nr_samples_per_channel = result["nr_samples"].as<unsigned>();
    nr_polarizations = result["nr_polarizations"].as<unsigned>();
    nr_tabs = result["nr_tabs"].as<unsigned>();
    nr_input_bits = result["nr_input_bits"].as<unsigned>();
    device_id = result["device_id"].as<unsigned>();
    nr_polarizations = result["nr_polarizations"].as<unsigned>();
  }

  unsigned nr_stations;
  unsigned nr_channels;
  unsigned nr_samples_per_channel;
  unsigned nr_polarizations;
  unsigned nr_tabs;
  unsigned nr_input_bits;
  const double subband_bandwidth = 200e3; // Hz
  const double subband_frequency = 1.5e8; // Hz

  friend std::ostream &operator<<(std::ostream &os, const Settings &settings) {
    os << ">>> Settings" << std::endl;
    os << "\tnr_stations:            " << settings.nr_stations << std::endl;
    os << "\tnr_channels:            " << settings.nr_channels << std::endl;
    os << "\tnr_samples_per_channel: " << settings.nr_samples_per_channel
       << std::endl;
    os << "\tnr_polarizations:       " << settings.nr_polarizations
       << std::endl;
    os << "\tnr_tabs:                " << settings.nr_tabs << std::endl;
    os << "\tnr_input_bits:          " << settings.nr_input_bits << std::endl;
    os << "\tdevice_id:              " << settings.device_id << std::endl;
    return os;
  }
};

template <typename InputType> class Benchmark : public BenchmarkFixture {
public:
  Benchmark(Settings &settings)
      : settings_(settings), BenchmarkFixture(settings) {
    shape_input_ = {settings.nr_stations, settings.nr_channels,
                    settings.nr_samples_per_channel, settings.nr_polarizations};
    shape_output_ = {settings.nr_channels, settings.nr_samples_per_channel,
                     settings.nr_tabs, settings.nr_polarizations};
    shape_delays_ = {settings.nr_stations, settings.nr_tabs};
    shape_weights_ = {settings.nr_tabs, settings.nr_channels,
                      settings.nr_stations};

    const size_t nr_input_bits = sizeof(InputType) * 8;

    BeamFormerCCGKernel::Parameters parameters(
        settings.nr_stations,            // nrStations
        settings.nr_channels,            // nrChannels
        settings.nr_samples_per_channel, // nrSamplesPerChannal
        settings.nr_tabs,                // nrTABs
        settings.nr_polarizations,       // nrPolarizations
        nr_input_bits                    // nrInputBits
    );

    host_input_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerCCGKernel::INPUT_DATA)));
    host_output_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerCCGKernel::OUTPUT_DATA)));
    const size_t sizeof_delays =
        std::accumulate(shape_delays_.begin(), shape_delays_.end(), 1,
                        std::multiplies<size_t>());
    host_delays_.reset(new cu::HostMemory(sizeof_delays));
    host_weights_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerCCGKernel::BEAM_FORMER_WEIGHTS)));

    dev_input_.reset(new cu::DeviceMemory(host_input_->size()));
    dev_output_.reset(new cu::DeviceMemory(host_output_->size()));
    dev_weights_.reset(new cu::DeviceMemory(host_weights_->size()));

    kernel_ = std::make_unique<BeamFormerCCGKernel>(device_, execute_stream_,
                                                    parameters);

    auto input = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_input_), shape_input_);
    auto weights = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_weights_), shape_weights_);

    input.fill(std::complex<InputType>(1, 1));
    weights.fill(0.1);
    htod_stream_.memcpyHtoDAsync(*dev_weights_, *host_weights_,
                                 host_weights_->size());
    htod_stream_.synchronize();

    kernel_->setWeights(*dev_weights_);
  }

private:
  virtual Kernel &getKernel() { return *kernel_; }

  virtual void launchKernel() override {
    BlockID block;
    kernel_->enqueue(block, *dev_input_, *dev_output_);
  }

  Settings &settings_;

  std::unique_ptr<KernelFactory<BeamFormerCCGKernel>> factory_;
  std::unique_ptr<BeamFormerCCGKernel> kernel_;

  std::array<size_t, 4> shape_input_;
  std::array<size_t, 4> shape_output_;
  std::array<size_t, 2> shape_delays_;
  std::array<size_t, 3> shape_weights_;

  std::unique_ptr<cu::HostMemory> host_input_;
  std::unique_ptr<cu::HostMemory> host_output_;
  std::unique_ptr<cu::HostMemory> host_delays_;
  std::unique_ptr<cu::HostMemory> host_weights_;

  std::unique_ptr<cu::DeviceMemory> dev_input_;
  std::unique_ptr<cu::DeviceMemory> dev_output_;
  std::unique_ptr<cu::DeviceMemory> dev_weights_;
};

int main(int argc, const char *argv[]) {
  // Parse command-line arguments
  cxxopts::ParseResult result = getCommandLineOptions(argc, argv);
  Settings settings(result);
  std::cout << settings;

  // Disable the default performance statistics
  PerformanceCounter::disablePrint();

  // Run benchmark
  if (settings.nr_input_bits == 16) {
    Benchmark<half> benchmark(settings);
    benchmark.runBenchmark();
  } else if (settings.nr_input_bits == 32) {
    Benchmark<float> benchmark(settings);
    benchmark.runBenchmark();
  }

  return EXIT_SUCCESS;
}