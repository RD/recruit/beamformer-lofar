#include <array>
#include <cmath>
#include <complex>
#include <iostream>
#include <memory>
#include <vector>

#include <cuda_fp16.h>
#include <cudawrappers/cu.hpp>
#include <cudawrappers/nvtx.hpp>
#include <cxxopts.hpp>
#include <xtensor/xtensor.hpp>
#include <xtensor/xview.hpp>

#if defined(HAVE_PMT)
#include <pmt.h>
#endif

#include <CoInterface/BlockID.h>
#include <CoInterface/Span.h>
#include <GPUProc/Kernels/BeamFormerCCGKernel.h>
#include <GPUProc/Kernels/BeamFormerKernel.h>
#include <GPUProc/Kernels/BeamFormerWMMAKernel.h>
#include <GPUProc/Kernels/BeamFormerWeightsKernel.h>
#include <GPUProc/Kernels/CoherentStokesKernel.h>
#include <GPUProc/Kernels/CoherentStokesTransposeKernel.h>
#include <GPUProc/Kernels/FIR_FilterKernel.h>
#include <GPUProc/Kernels/QuantizeOutputKernel.h>

namespace {
cxxopts::Options setupCommandLineParser(const char *argv[]) {
  cxxopts::Options options(argv[0], "Beamformer pipeline benchmark");

  const unsigned kNrStreams = 1;
  const unsigned kNrSubbands = 1;
  const unsigned kNrStations = 64;
  const unsigned kNrInputChannels = 256;
  const unsigned kNrOutputChannels = 16;
  const unsigned kNrSamplesPerChannel = 768;
  const unsigned kNrTabs = 64;
  const unsigned kNrInputBits = 16;
  const unsigned kNrStokes = 4;
  const unsigned kTimeIntegrationFactor = 32;
  const unsigned kQuantizationBits = 8;
  const std::string kBeamFormer = "ccg";
  const unsigned kDeviceId = 0;
  const unsigned kNrBlocks = 10;

  options.add_options()(
      "nr_streams", "Number of streams",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrStreams)))(
      "nr_subbands", "Number of subbands",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrSubbands)))(
      "nr_stations", "Number of stations",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrStations)))(
      "nr_input_channels", "Number of channels per subband",
      cxxopts::value<unsigned>()->default_value(std::to_string(
          kNrInputChannels)))("nr_output_channels", "Number of output channels",
                              cxxopts::value<unsigned>()->default_value(
                                  std::to_string(kNrOutputChannels)))(
      "nr_samples", "Number of samples per channel",
      cxxopts::value<unsigned>()->default_value(
          std::to_string(kNrSamplesPerChannel)))(
      "nr_tabs", "Number of TABs",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrTabs)))(
      "nr_input_bits", "Number of input bits",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrInputBits)))(
      "nr_stokes", "Number of Stokes",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrStokes)))(
      "time_integration_factor", "Time integration factor",
      cxxopts::value<unsigned>()->default_value(std::to_string(
          kTimeIntegrationFactor)))("nr_quantize_bits",
                                    "Number of bits in quantization",
                                    cxxopts::value<unsigned>()->default_value(
                                        std::to_string(kQuantizationBits)))(
      "beamformer", "Beamformer backend (reference, wmma or ccg)",
      cxxopts::value<std::string>()->default_value(kBeamFormer))(
      "nr_blocks", "Number of blocks",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrBlocks)))(
      "device_id", "Device ID",
      cxxopts::value<unsigned>()->default_value(std::to_string(kDeviceId)))(
      "h,help", "Print help");

  return options;
}

cxxopts::ParseResult getCommandLineOptions(int argc, const char *argv[]) {
  cxxopts::Options options = setupCommandLineParser(argv);

  try {
    cxxopts::ParseResult result = options.parse(argc, argv);

    if (result.count("help")) {
      std::cout << options.help() << std::endl;
      exit(EXIT_SUCCESS);
    }

    return result;

  } catch (const cxxopts::exceptions::exception &e) {
    std::cerr << "Error parsing command-line options: " << e.what()
              << std::endl;
    exit(EXIT_FAILURE);
  }
}

double blockDuration() {
  const unsigned int block_size = 196608;
  const unsigned int clock_mhz = 200;
  const unsigned int clock_hz = clock_mhz * 1000000;
  const unsigned int subband_width = 1.0 * clock_hz / 1024;
  const double sample_duration = 1.0 / subband_width;
  return block_size * sample_duration;
}

std::string sizeString(size_t bytes) {
  const char *units[] = {"byte", "KB", "MB", "GB"};
  unsigned int unit_index = 0;
  float size = static_cast<float>(bytes);

  while (size >= 1024 && unit_index < 3) {
    size /= 1024;
    unit_index++;
  }

  std::ostringstream oss;
  oss << std::fixed << std::setprecision(2) << size << " " << units[unit_index];
  return oss.str();
}

void printProgress(float percentage, int width = 60, char fill = '|',
                   char empty = ' ') {
  // Clamp the percentage to be between 0 and 1
  if (percentage < 0.0) {
    percentage = 0.0;
  }
  if (percentage > 1.0) {
    percentage = 1.0;
  }

  const int filled = static_cast<int>(percentage * width);
  const int remaining = width - filled;

  std::cout << "\r" << std::setw(3) << static_cast<int>(percentage * 100)
            << "% [" << std::string(filled, fill)
            << std::string(remaining, empty) << "]" << std::flush;
}

} // namespace

using namespace LOFAR::Cobalt;

class Settings {
public:
  Settings(cxxopts::ParseResult &result) {
    nr_streams = result["nr_streams"].as<unsigned>();
    nr_subbands = result["nr_subbands"].as<unsigned>();
    nr_stations = result["nr_stations"].as<unsigned>();
    nr_input_channels = result["nr_input_channels"].as<unsigned>();
    nr_output_channels = result["nr_output_channels"].as<unsigned>();
    nr_samples_per_channel = result["nr_samples"].as<unsigned>();
    nr_tabs = result["nr_tabs"].as<unsigned>();
    nr_input_bits = result["nr_input_bits"].as<unsigned>();
    nr_stokes = result["nr_stokes"].as<unsigned>();
    time_integration_factor = result["time_integration_factor"].as<unsigned>();
    nr_quantize_bits = result["nr_quantize_bits"].as<unsigned>();
    beamformer = result["beamformer"].as<std::string>();
    nr_blocks = result["nr_blocks"].as<unsigned>();
    device_id = result["device_id"].as<unsigned>();
  }

  unsigned nr_streams;
  unsigned nr_subbands;
  unsigned nr_stations;
  unsigned nr_input_channels;
  unsigned nr_output_channels;
  unsigned nr_samples_per_channel;
  unsigned nr_tabs;
  unsigned nr_input_bits;
  const unsigned nr_output_bits = 32;
  unsigned nr_stokes;
  unsigned time_integration_factor;
  unsigned nr_quantize_bits;
  std::string beamformer;
  unsigned nr_blocks;
  unsigned device_id;
  const unsigned nr_polarizations = 2;
  const double subband_bandwidth = 200e3; // Hz
  const double subband_frequency = 1.5e8; // Hz

  friend std::ostream &operator<<(std::ostream &os, const Settings &settings) {
    os << ">>> Settings" << std::endl;
    os << "\tnr_streams:              " << settings.nr_streams << std::endl;
    os << "\tnr_subbands:             " << settings.nr_subbands << std::endl;
    os << "\tnr_stations:             " << settings.nr_stations << std::endl;
    os << "\tnr_input_channels:       " << settings.nr_input_channels
       << std::endl;
    os << "\tnr_output_channels:      " << settings.nr_output_channels
       << std::endl;
    os << "\tnr_samples_per_channel:  " << settings.nr_samples_per_channel
       << std::endl;
    os << "\tnr_tabs:                 " << settings.nr_tabs << std::endl;
    os << "\tnr_input_bits:           " << settings.nr_input_bits << std::endl;
    os << "\tnr_stokes:               " << settings.nr_stokes << std::endl;
    os << "\ttime_integration_factor: " << settings.time_integration_factor
       << std::endl;
    os << "\tnr_quantize_bits:        " << settings.nr_quantize_bits
       << std::endl;
    os << "\tnr_blocks:               " << settings.nr_blocks << std::endl;
    os << "\tdevice_id:               " << settings.device_id << std::endl;
    return os;
  }
};

template <typename InputType> class Benchmark {
public:
  Benchmark(const Settings &settings, cu::Context &context, cu::Device &device)
      : settings_(settings), context_(context), device_(device) {
    initializeParameters();
    selectBeamformer();
    initializeKernels();
    allocateHostMemory();
    initializeData();
    allocateDeviceMemory();
    reportDataSizes();
  }

  void initializeParameters() {
    const size_t block_size =
        settings_.nr_samples_per_channel * settings_.nr_input_channels;

    // BeamFormer: same dimensions as input
    const size_t nr_channels_bf = settings_.nr_input_channels;
    const size_t nr_samples_bf = settings_.nr_samples_per_channel;

    // CoherenStokesTranspose: same as beamformer
    const size_t nr_channels_cst = settings_.nr_input_channels;
    const size_t nr_samples_cst = settings_.nr_samples_per_channel;

    // FIR_Filter: fewer channels, more samples
    const size_t nr_channel_fir = settings_.nr_output_channels;
    const size_t nr_samples_fir = block_size / nr_channel_fir;

    // CoherentStokes: size before downsampling
    const size_t nr_channels_cs = settings_.nr_output_channels;
    const size_t nr_samples_cs = nr_samples_fir;

    // QuantizeOutput
    const size_t nr_channels_quantized = settings_.nr_output_channels;
    const size_t nr_samples_quantized =
        nr_samples_cs / settings_.time_integration_factor;

    std::vector<unsigned> station_indices(settings_.nr_stations);
    std::iota(station_indices.begin(), station_indices.end(), 0);
    std::vector<unsigned> delay_indices(settings_.nr_stations);
    std::iota(delay_indices.begin(), delay_indices.end(), 0);

    bfw_params_ = std::make_unique<BeamFormerWeightsKernel::Parameters>(
        station_indices, delay_indices,
        station_indices.size(),      // nrDelays
        settings_.nr_input_channels, // nrChannels
        settings_.nr_tabs,           // nrTABs
        settings_.nr_output_bits,    // nrOutputBits
        settings_.subband_bandwidth, // subbandBandwidth
        true                         // singlePrecision
    );

    bf_reference_params_ = std::make_unique<BeamFormerKernel::Parameters>(
        station_indices, delay_indices,
        station_indices.size(),      // nrDealays
        nr_channels_bf,              // nrChannels
        nr_samples_bf,               // nrSamplesPerChannel
        settings_.nr_tabs,           // nrTABs
        settings_.nr_input_bits,     // nrInputBits
        settings_.subband_bandwidth, // subbandBandwidth
        false                        // computeWeights
    );

    bf_ccg_params_ = std::make_unique<BeamFormerCCGKernel::Parameters>(
        settings_.nr_stations,      // nrStations
        nr_channels_bf,             // nrChannels
        nr_samples_bf,              // nrSamplesPerChannel
        settings_.nr_tabs,          // nrTABs
        settings_.nr_polarizations, // nrPolarizations
        settings_.nr_input_bits     // nrInputBits
    );

    bf_wmma_params_ = std::make_unique<BeamFormerWMMAKernel::Parameters>(
        settings_.nr_stations,      // nrStations
        nr_channels_bf,             // nrChannels
        nr_samples_bf,              // nrSamplesPerChannel
        settings_.nr_tabs,          // nrTABs
        settings_.nr_polarizations, // nrPolarizations
        settings_.nr_input_bits     // nrInputBits
    );

    cst_params_ = std::make_unique<CoherentStokesTransposeKernel::Parameters>(
        nr_channels_cst,          // nrChannels
        nr_samples_cst,           // nrSamplesPerChannel
        settings_.nr_tabs,        // nrTABs
        settings_.nr_output_bits, // nrBitsPerSample
        true                      // enableFFT
    );

    fir_params_ = std::make_unique<FIR_FilterKernel::Parameters>(
        settings_.nr_tabs,        // nrTABs
        settings_.nr_output_bits, // nrBitsPerSample
        nr_channel_fir,           // nrChannels
        nr_samples_fir,           // nrSamplesPerChannel
        1.0f,                     // scaleFactor
        true                      // enableFFT
    );

    cs_params_ = std::make_unique<CoherentStokesKernel::Parameters>(
        nr_channels_cs,                    // nrChannels
        nr_samples_cs,                     // nrSamplesPerChannel
        settings_.nr_tabs,                 // nrTABs
        settings_.nr_stokes,               // nrStokes
        settings_.nr_output_bits,          // nrBitsPerSample
        false,                             // outputComplexVoltages
        settings_.time_integration_factor, // timeIntegrationFactor
        false,                             // quantizeOutput
        0                                  // nrChannelsDelayComp
    );

    quantization_params_ = std::make_unique<QuantizeOutputKernel::Parameters>(
        nr_channels_quantized,      // nrChannels
        nr_samples_quantized,       // nrSamplesPerChannel
        settings_.nr_tabs,          // nrTABs
        settings_.nr_stokes,        // nrStokes
        false,                      // outputComplexVoltages
        settings_.nr_quantize_bits, // nrQuantizeBits
        5,                          // quantizeScaleMax
        -5,                         // quantizeScaleMin
        false                       // sIpositive
    );
  }

  void selectBeamformer() {
    beamformer_reference_ = !settings_.beamformer.compare("reference");
    beamformer_ccg_ = !settings_.beamformer.compare("ccg");
    beamformer_wmma_ = !settings_.beamformer.compare("wmma");
  }

  void initializeKernels() {
    execute_stream_.resize(settings_.nr_streams);

    bfw_factory_ = std::make_unique<KernelFactory<BeamFormerWeightsKernel>>(
        device_, *bfw_params_);

    if (beamformer_reference_) {
      beamformer_reference_factory_ =
          std::make_unique<KernelFactory<BeamFormerKernel>>(
              device_, *bf_reference_params_);
    } else if (beamformer_wmma_) {
      beamformer_wmma_factory_ =
          std::make_unique<KernelFactory<BeamFormerWMMAKernel>>(
              device_, *bf_wmma_params_);
    }

    cst_factory_ =
        std::make_unique<KernelFactory<CoherentStokesTransposeKernel>>(
            device_, *cst_params_);

    if (settings_.nr_output_channels > 1) {
      firfilter_factory_ = std::make_unique<KernelFactory<FIR_FilterKernel>>(
          device_, *fir_params_);
    }

    cs_factory_ = std::make_unique<KernelFactory<CoherentStokesKernel>>(
        device_, *cs_params_);

    quantization_factory_ =
        std::make_unique<KernelFactory<QuantizeOutputKernel>>(
            device_, *quantization_params_);

    for (int stream_index = 0; stream_index < settings_.nr_streams;
         stream_index++) {
      cu::Stream &execute_stream = execute_stream_[stream_index];
      bfw_kernel_.emplace_back(bfw_factory_->create(context_, execute_stream));
      if (beamformer_reference_) {
        beamformer_reference_kernel_.emplace_back(
            beamformer_reference_factory_->create(context_, execute_stream));
      } else if (beamformer_ccg_) {
        beamformer_ccg_kernel_.push_back(std::make_unique<BeamFormerCCGKernel>(
            device_, execute_stream, *bf_ccg_params_));
      } else if (beamformer_wmma_) {
        beamformer_wmma_kernel_.emplace_back(
            beamformer_wmma_factory_->create(context_, execute_stream));
      }
      cst_kernel_.emplace_back(cst_factory_->create(context_, execute_stream));

      if (settings_.nr_output_channels > 1) {
        firfilter_kernel_.emplace_back(
            firfilter_factory_->create(context_, execute_stream));
      }
      cs_kernel_.emplace_back(cs_factory_->create(context_, execute_stream));
      quantization_kernel_.emplace_back(
          quantization_factory_->create(context_, execute_stream));
    }
  }

  void allocateHostMemory() {
    host_delays_ = std::make_unique<cu::HostMemory>(
        bfw_params_->bufferSize(BeamFormerWeightsKernel::BEAM_FORMER_DELAYS));
    host_input_ = std::make_unique<cu::HostMemory>(
        kDoubleBuffering *
        bf_reference_params_->bufferSize(BeamFormerKernel::INPUT_DATA));
    host_output_ = std::make_unique<cu::HostMemory>(
        kDoubleBuffering *
        quantization_params_->bufferSize(QuantizeOutputKernel::OUTPUT_DATA));
  }

  void allocateDeviceMemory() {
    dev_delays_ = std::make_unique<cu::DeviceMemory>(
        bfw_params_->bufferSize(BeamFormerWeightsKernel::BEAM_FORMER_DELAYS));
    dev_weights_ = std::make_unique<cu::DeviceMemory>(
        bfw_params_->bufferSize(BeamFormerWeightsKernel::BEAM_FORMER_WEIGHTS));

    size_t sizeof_temp = std::max(
        {bf_reference_params_->bufferSize(BeamFormerKernel::OUTPUT_DATA),
         cst_params_->bufferSize(CoherentStokesTransposeKernel::OUTPUT_DATA),
         cs_params_->bufferSize(CoherentStokesKernel::OUTPUT_DATA)

        });

    if (settings_.nr_output_channels > 1) {
      sizeof_temp = std::max(
          sizeof_temp, fir_params_->bufferSize(FIR_FilterKernel::OUTPUT_DATA));
    }

    dev_temp1_ = std::make_unique<cu::DeviceMemory>(sizeof_temp);
    dev_temp2_ = std::make_unique<cu::DeviceMemory>(sizeof_temp);

    for (int i = 0; i < kDoubleBuffering; i++) {
      dev_input_.emplace_back(host_input_->size() / kDoubleBuffering);
      dev_output_.emplace_back(host_output_->size() / kDoubleBuffering);
    }
  }

  void initializeData() {
    if (beamformer_reference_) {
      shape_input_ = {
          kDoubleBuffering, settings_.nr_stations, settings_.nr_input_channels,
          settings_.nr_samples_per_channel, settings_.nr_polarizations};
    } else {
      shape_input_ = {kDoubleBuffering, settings_.nr_input_channels,
                      settings_.nr_polarizations, settings_.nr_stations,
                      settings_.nr_samples_per_channel};
    }
    auto input = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_input_), shape_input_);
    input.fill(std::complex<InputType>(1, 1));

    shape_delays_ = {settings_.nr_stations, settings_.nr_tabs};
    auto delays =
        CreateSpan(static_cast<double *>(*host_delays_), shape_delays_);
    delays.fill(1.0);

    shape_output_ = {kDoubleBuffering, host_output_->size() / kDoubleBuffering};
    auto output = CreateSpan(static_cast<char *>(*host_output_), shape_output_);
    output.fill(0.0);
  }

  void reportDataSizes() {
    std::cout << std::endl;
    std::cout << ">>> Size of static memory: " << std::endl;
    std::cout << "\tdelays:  " << sizeString(dev_delays_->size()) << std::endl;
    std::cout << "\tweights: " << sizeString(dev_weights_->size()) << std::endl;
    std::cout << "\ttemp1: " << sizeString(dev_temp1_->size()) << std::endl;
    std::cout << "\ttemp2: " << sizeString(dev_temp2_->size()) << std::endl;
    const size_t sizeof_memory_static = dev_delays_->size() +
                                        dev_weights_->size() +
                                        dev_temp1_->size() + dev_temp2_->size();
    std::cout << "\ttotal: " << sizeString(sizeof_memory_static) << std::endl;
    std::cout << ">>> Size of memory per block: " << std::endl;
    std::cout << "\tinput:   " << sizeString(dev_input_[0].size()) << std::endl;
    std::cout << "\toutput:  " << sizeString(dev_output_[0].size())
              << std::endl;
    const size_t sizeof_memory_block =
        dev_input_.size() * dev_input_[0].size() +
        dev_output_.size() * dev_output_[0].size();
    const size_t sizeof_memory_total =
        sizeof_memory_static + sizeof_memory_block;
    std::cout << ">>> Size of memory total: " << sizeString(sizeof_memory_total)
              << std::endl;
    std::cout << std::endl;
  }

  void enqueue() {
    auto input = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_input_), shape_input_);

    auto output = CreateSpan(static_cast<char *>(*host_output_), shape_output_);

    auto delays =
        CreateSpan(static_cast<double *>(*host_delays_), shape_delays_);

    BlockID block;
    block.block = 0;
    for (int subband_index = 0; subband_index < settings_.nr_subbands;
         subband_index++) {
      const int stream_index = subband_index % settings_.nr_streams;

      // Set beamformer weights
      if (beamformer_reference_) {
        beamformer_reference_kernel_[stream_index]->setWeights(*dev_weights_);
      } else if (beamformer_wmma_) {
        beamformer_wmma_kernel_[stream_index]->setWeights(*dev_weights_);
      } else if (beamformer_ccg_) {
        beamformer_ccg_kernel_[stream_index]->setWeights(*dev_weights_);
      }
    }

    for (int block_id = 0; block_id < settings_.nr_blocks; block_id++) {
      for (int subband_index = 0; subband_index < settings_.nr_subbands;
           subband_index++) {
        const int stream_index = subband_index % settings_.nr_streams;
        cu::Stream &execute_stream = execute_stream_[stream_index];

        const int buffer_id = block_id % 2;

        // Wait for previous output copy to finish to prevent
        // the output from being overwritten.
        if (block_id > 1) {
          execute_stream.wait(
              output_copy_end_events_(subband_index, block_id - 2));
        }

        // Copy delays
        execute_stream.memcpyHtoDAsync(*dev_delays_, delays.data(),
                                       dev_delays_->size());

        // Launch beamformer weights kernel
        bfw_kernel_[stream_index]->enqueue(block, *dev_delays_, *dev_weights_);

        // Copy input for first block
        if (block_id == 0) {
          htod_stream_.record(
              input_copy_start_events_(subband_index, block_id));
          htod_stream_.memcpyHtoDAsync(dev_input_[buffer_id],
                                       &input(buffer_id, 0, 0, 0),
                                       dev_input_[buffer_id].size());
          htod_stream_.record(input_copy_end_events_(subband_index, block_id));
        }

        // Copy input for the next block (if any)
        if (block_id < (settings_.nr_blocks - 1)) {
          // Wait for previous execution to finished to prevent
          // its input from being overwritten.
          if (block_id > 1) {
            htod_stream_.wait(execute_end_events_(subband_index, block_id - 2));
          }
          htod_stream_.record(
              input_copy_start_events_(subband_index, block_id + 1));
          htod_stream_.memcpyHtoDAsync(dev_input_[buffer_id ^ 1],
                                       &input(buffer_id ^ 1, 0, 0, 0),
                                       dev_input_[buffer_id].size());
          htod_stream_.record(
              input_copy_end_events_(subband_index, block_id + 1));
        }

        execute_stream.wait(input_copy_end_events_(subband_index, block_id));
        execute_stream.record(execute_start_events_(subband_index, block_id));
        block.block = block_id;

        // Launch beamformer kernel: input -> temp1
        if (beamformer_reference_) {
          beamformer_reference_kernel_[stream_index]->enqueue(
              block, dev_input_[buffer_id], *dev_temp1_);
        } else if (beamformer_wmma_) {
          beamformer_wmma_kernel_[stream_index]->enqueue(
              block, dev_input_[buffer_id], *dev_temp1_);
        } else if (beamformer_ccg_) {
          beamformer_ccg_kernel_[stream_index]->enqueue(
              block, dev_input_[buffer_id], *dev_temp1_);
        }

        // Launch coherentstokes transpose kernel: temp1 -> temp2
        cst_kernel_[stream_index]->enqueue(block, *dev_temp1_, *dev_temp2_);

        // Launch FIR filter kernel: temp2 -> temp1
        if (settings_.nr_output_channels > 1) {
          firfilter_kernel_[stream_index]->enqueue(block, *dev_temp2_,
                                                   *dev_temp1_);

          // Swap input/output for subsequent kernels
          std::swap(dev_temp1_, dev_temp2_);
        }

        // Launch coherentstokes kernel
        cs_kernel_[stream_index]->enqueue(block, *dev_temp1_, *dev_temp2_);

        // Launch quantization kernel: temp -> output
        quantization_kernel_[stream_index]->enqueue(block, *dev_temp2_,
                                                    dev_output_[buffer_id]);
        execute_stream.record(execute_end_events_(subband_index, block_id));

        // Copy output
        dtoh_stream_.wait(execute_end_events_(subband_index, block_id));
        dtoh_stream_.record(output_copy_start_events_(subband_index, block_id));
        dtoh_stream_.memcpyDtoHAsync(&output(buffer_id, 0),
                                     dev_output_[buffer_id],
                                     dev_output_[buffer_id].size());
        dtoh_stream_.record(output_copy_end_events_(subband_index, block_id));
      }
    }
  }

  void run() {
    auto input = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_input_), shape_input_);

    auto delays =
        CreateSpan(static_cast<double *>(*host_delays_), shape_delays_);

    auto output = CreateSpan(static_cast<char *>(*host_output_), shape_output_);

    // Initialize PMT
#if defined(HAVE_PMT)
    std::unique_ptr<pmt::PMT> powermeter =
        pmt::Create("nvidia", settings_.device_id);
    pmt::State start_state = powermeter->Read();
#endif

    input_copy_end_events_ =
        xt::xtensor<cu::Event, 2>({settings_.nr_subbands, settings_.nr_blocks});
    input_copy_start_events_ =
        xt::xtensor<cu::Event, 2>({settings_.nr_subbands, settings_.nr_blocks});
    output_copy_start_events_ =
        xt::xtensor<cu::Event, 2>({settings_.nr_subbands, settings_.nr_blocks});
    output_copy_end_events_ =
        xt::xtensor<cu::Event, 2>({settings_.nr_subbands, settings_.nr_blocks});
    execute_start_events_ =
        xt::xtensor<cu::Event, 2>({settings_.nr_subbands, settings_.nr_blocks});
    execute_end_events_ =
        xt::xtensor<cu::Event, 2>({settings_.nr_subbands, settings_.nr_blocks});

    nvtx::Marker marker("pipeline");
    marker.start();

    // Enqueue all subbands and blocks
    enqueue();

    std::cout << ">>> Processing blocks: " << std::endl;
    for (int block_id = 0; block_id < settings_.nr_blocks; block_id++) {
      printProgress(static_cast<float>(block_id) / settings_.nr_blocks);
      execute_end_events_(0, block_id).synchronize();
    }
    printProgress(1);
    std::cout << std::endl << std::endl;

    // Wait for GPU to finish
    cu::Event &pipeline_start_event = input_copy_start_events_(0, 0);
    cu::Event &pipeline_end_event =
        execute_end_events_(settings_.nr_subbands - 1, settings_.nr_blocks - 1);
    pipeline_end_event.synchronize();
    marker.end();

    // Report performance
    std::cout << ">>> Performance" << std::endl;
    const float runtime_pipeline =
        pipeline_end_event.elapsedTime(pipeline_start_event); // ms
    std::cout << "\tpipeline runtime: " << runtime_pipeline << " ms";

#if defined(HAVE_PMT)
    pmt::State end_state = powermeter->Read();
    const float joules = pmt::PMT::joules(start_state, end_state);
    const float watts = pmt::PMT::watts(start_state, end_state);
    std::cout << ", " << joules << " J, " << watts << " W (average)";
    powermeter.reset();
#endif

    std::cout << std::endl;

    // Report how many subbands would yield up to 99% load
    const size_t nr_blocks = settings_.nr_subbands * settings_.nr_blocks;
    const float runtime_block_s =
        (runtime_pipeline / nr_blocks) * 1e-3; // in seconds
    const int nr_subbands_realtime =
        static_cast<int>(std::floor(0.99 * blockDuration() / runtime_block_s));
    std::cout << "\tsubbands per GPU at realtime: " << nr_subbands_realtime
              << std::endl;

    // Report the average bandwidth and gpu utilization
    float runtime_execute = 0;
    float runtime_input_copy = 0;
    float runtime_output_copy = 0;
    for (int block_id = 0; block_id < settings_.nr_blocks; block_id++) {
      for (int subband_index = 0; subband_index < settings_.nr_subbands;
           subband_index++) {
        runtime_input_copy +=
            input_copy_end_events_(subband_index, block_id)
                .elapsedTime(input_copy_start_events_(subband_index, block_id));
        runtime_execute +=
            execute_end_events_(subband_index, block_id)
                .elapsedTime(execute_start_events_(subband_index, block_id));
        runtime_output_copy += output_copy_end_events_(subband_index, block_id)
                                   .elapsedTime(output_copy_start_events_(
                                       subband_index, block_id));
      }
    }
    const size_t total_input_bytes = nr_blocks * dev_input_[0].size();
    const size_t total_output_bytes = nr_blocks * dev_output_[0].size();
    const float runtime_pipeline_s = runtime_pipeline * 1e-3;
    std::cout << "\tgpu copy in: "
              << sizeString(total_input_bytes / runtime_input_copy * 1e3)
              << "/s" << std::endl;
    std::cout << "\tgpu copy out: "
              << sizeString(total_output_bytes / runtime_output_copy * 1e3)
              << "/s" << std::endl;
    std::cout << "\tgpu utilization: "
              << runtime_execute / runtime_pipeline_s * 1e-1 << " %"
              << std::endl;

    // Report pipeline datarate
    std::cout << "\tinput datarate: "
              << sizeString(8 * total_input_bytes / runtime_pipeline_s)
              << "it/s" << std::endl;
    std::cout << "\toutput datarate: "
              << sizeString(8 * total_output_bytes / runtime_pipeline_s)
              << "it/s" << std::endl;

    // Kernel runtime is printed upon Kernel destruction
    std::cout << std::endl;
    std::cout << "\tkernel runtime:" << std::endl;
  }

private:
  const unsigned kDoubleBuffering = 2;
  const Settings &settings_;

  cu::Context &context_;
  cu::Device &device_;

  cu::Stream htod_stream_;
  std::vector<cu::Stream> execute_stream_;
  cu::Stream dtoh_stream_;

  xt::xtensor<cu::Event, 2> input_copy_start_events_;
  xt::xtensor<cu::Event, 2> input_copy_end_events_;
  xt::xtensor<cu::Event, 2> output_copy_start_events_;
  xt::xtensor<cu::Event, 2> output_copy_end_events_;
  xt::xtensor<cu::Event, 2> execute_start_events_;
  xt::xtensor<cu::Event, 2> execute_end_events_;

  std::unique_ptr<BeamFormerWeightsKernel::Parameters> bfw_params_;
  std::unique_ptr<BeamFormerKernel::Parameters> bf_reference_params_;
  std::unique_ptr<BeamFormerCCGKernel::Parameters> bf_ccg_params_;
  std::unique_ptr<BeamFormerWMMAKernel::Parameters> bf_wmma_params_;
  std::unique_ptr<CoherentStokesTransposeKernel::Parameters> cst_params_;
  std::unique_ptr<FIR_FilterKernel::Parameters> fir_params_;
  std::unique_ptr<CoherentStokesKernel::Parameters> cs_params_;
  std::unique_ptr<QuantizeOutputKernel::Parameters> quantization_params_;

  std::unique_ptr<KernelFactory<BeamFormerWeightsKernel>> bfw_factory_;
  std::unique_ptr<KernelFactory<BeamFormerKernel>>
      beamformer_reference_factory_;
  std::unique_ptr<KernelFactory<BeamFormerWMMAKernel>> beamformer_wmma_factory_;
  std::unique_ptr<KernelFactory<FIR_FilterKernel>> firfilter_factory_;
  std::unique_ptr<KernelFactory<CoherentStokesTransposeKernel>> cst_factory_;
  std::unique_ptr<KernelFactory<CoherentStokesKernel>> cs_factory_;
  std::unique_ptr<KernelFactory<QuantizeOutputKernel>> quantization_factory_;

  std::vector<std::unique_ptr<BeamFormerWeightsKernel>> bfw_kernel_;
  std::vector<std::unique_ptr<BeamFormerKernel>> beamformer_reference_kernel_;
  std::vector<std::unique_ptr<BeamFormerWMMAKernel>> beamformer_wmma_kernel_;
  std::vector<std::unique_ptr<BeamFormerCCGKernel>> beamformer_ccg_kernel_;
  std::vector<std::unique_ptr<FIR_FilterKernel>> firfilter_kernel_;
  std::vector<std::unique_ptr<CoherentStokesTransposeKernel>> cst_kernel_;
  std::vector<std::unique_ptr<CoherentStokesKernel>> cs_kernel_;
  std::vector<std::unique_ptr<QuantizeOutputKernel>> quantization_kernel_;

  bool beamformer_reference_;
  bool beamformer_ccg_;
  bool beamformer_wmma_;

  std::array<size_t, 5> shape_input_;
  std::array<size_t, 2> shape_delays_;
  std::array<size_t, 2> shape_output_;

  std::unique_ptr<cu::HostMemory> host_delays_;
  std::unique_ptr<cu::HostMemory> host_input_;
  std::unique_ptr<cu::HostMemory> host_output_;

  std::unique_ptr<cu::DeviceMemory> dev_delays_;
  std::unique_ptr<cu::DeviceMemory> dev_weights_;
  std::unique_ptr<cu::DeviceMemory> dev_temp1_;
  std::unique_ptr<cu::DeviceMemory> dev_temp2_;
  std::vector<cu::DeviceMemory> dev_input_;
  std::vector<cu::DeviceMemory> dev_output_;
};

int main(int argc, const char *argv[]) {
  // Parse command-line arguments
  cxxopts::ParseResult result = getCommandLineOptions(argc, argv);
  Settings settings(result);
  std::cout << settings;

  // Initialize CUDA environemt
  cu::init();
  cu::Device device(settings.device_id);
  cu::Context context(0, device);

  std::cout << "\tgpu_name:                " << device.getName() << std::endl;
  std::cout << std::endl;

  // Run benchmark
  if (settings.nr_input_bits == 16) {
    Benchmark<half> benchmark(settings, context, device);
    benchmark.run();
  } else if (settings.nr_input_bits == 32) {
    Benchmark<float> benchmark(settings, context, device);
    benchmark.run();
  }

  return EXIT_SUCCESS;
}