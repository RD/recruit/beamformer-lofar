#include <complex>
#include <iostream>
#include <memory>

#include <cuda_fp16.h>
#include <cudawrappers/cu.hpp>
#include <cxxopts.hpp>

#include <CoInterface/BlockID.h>
#include <CoInterface/Span.h>
#include <GPUProc/Kernels/QuantizeOutputKernel.h>

#include "BenchmarkUtil.h"

namespace {
cxxopts::Options setupCommandLineParser(const char *argv[]) {
  cxxopts::Options options(argv[0], "Benchmark for BeamFormerKernel");

  const unsigned kNrChannels = 1;
  const unsigned kNrSamplesPerChannel = 196608;
  const unsigned kNrTabs = 128;
  const unsigned kNrStokes = 4;
  const unsigned kNrQuantizeBits = 8;
  const unsigned kDeviceId = 0;

  options.add_options()(
      "nr_channels", "Number of channels",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrChannels)))(
      "nr_samples", "Number of samples per channel",
      cxxopts::value<unsigned>()->default_value(
          std::to_string(kNrSamplesPerChannel)))(
      "nr_tabs", "Number of TABs",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrTabs)))(
      "nr_stokes", "Number of Stokes",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrStokes)))(
      "nr_quantize_bits", "Number of bits in quantization",
      cxxopts::value<unsigned>()->default_value(
          std::to_string(kNrQuantizeBits)))(
      "device_id", "Device ID",
      cxxopts::value<unsigned>()->default_value(std::to_string(kDeviceId)))(
      "h,help", "Print help");

  return options;
}

cxxopts::ParseResult getCommandLineOptions(int argc, const char *argv[]) {
  cxxopts::Options options = setupCommandLineParser(argv);

  try {
    cxxopts::ParseResult result = options.parse(argc, argv);

    if (result.count("help")) {
      std::cout << options.help() << std::endl;
      exit(EXIT_SUCCESS);
    }

    return result;

  } catch (const cxxopts::exceptions::exception &e) {
    std::cerr << "Error parsing command-line options: " << e.what()
              << std::endl;
    exit(EXIT_FAILURE);
  }
}
} // namespace

using namespace LOFAR::Cobalt;

class Settings : public BenchmarkFixture::SettingsBase {
public:
  Settings(cxxopts::ParseResult &result) {
    nr_channels = result["nr_channels"].as<unsigned>();
    nr_samples_per_channel = result["nr_samples"].as<unsigned>();
    nr_tabs = result["nr_tabs"].as<unsigned>();
    nr_stokes = result["nr_stokes"].as<unsigned>();
    nr_quantize_bits = result["nr_quantize_bits"].as<unsigned>();
    device_id = result["device_id"].as<unsigned>();
  }

  const unsigned int nr_polarizations = 2;

  unsigned nr_channels;
  unsigned nr_samples_per_channel;
  unsigned nr_tabs;
  unsigned nr_stokes;
  unsigned nr_quantize_bits;

  friend std::ostream &operator<<(std::ostream &os, const Settings &settings) {
    os << ">>> Settings" << std::endl;
    os << "\tnr_channels:             " << settings.nr_channels << std::endl;
    os << "\tnr_samples_per_channel:  " << settings.nr_samples_per_channel
       << std::endl;
    os << "\tnr_tabs:                 " << settings.nr_tabs << std::endl;
    os << "\tnr_stokes:               " << settings.nr_stokes << std::endl;
    os << "\tnr_quantize_bits:        " << settings.nr_quantize_bits
       << std::endl;
    os << "\tdevice_id:               " << settings.device_id << std::endl;
    return os;
  }
};

class Benchmark : public BenchmarkFixture {
public:
  Benchmark(Settings &settings)
      : settings_(settings), BenchmarkFixture(settings) {
    shape_input_ = {settings.nr_tabs, settings.nr_stokes, settings.nr_channels,
                    settings.nr_samples_per_channel};

    QuantizeOutputKernel::Parameters parameters(
        settings.nr_channels,            // nrChannels
        settings.nr_samples_per_channel, // nrSamplesPerChannel
        settings.nr_tabs,                // nrTABs
        settings.nr_stokes,              // nrStokes
        kComplexVoltages,                // outputComplexVoltages
        settings.nr_quantize_bits,       // nrQuantizeBits
        kQuantizeScale,                  // quantizeScaleMax
        -kQuantizeScale,                 // quantizeScaleMin
        kStokesIPositive                 // sIpositive
    );

    host_input_.reset(new cu::HostMemory(
        parameters.bufferSize(QuantizeOutputKernel::INPUT_DATA)));
    host_output_.reset(new cu::HostMemory(
        parameters.bufferSize(QuantizeOutputKernel::OUTPUT_DATA)));

    dev_input_.reset(new cu::DeviceMemory(host_input_->size()));
    dev_output_.reset(new cu::DeviceMemory(host_output_->size()));

    factory_.reset(
        new KernelFactory<QuantizeOutputKernel>(device_, parameters));

    kernel_.reset(factory_->create(context_, execute_stream_));

    auto input = CreateSpan(static_cast<float *>(*host_input_), shape_input_);

    input.fill(1);

    htod_stream_.synchronize();
  }

private:
  const bool kComplexVoltages = false;
  const float kQuantizeScale = 2;
  const bool kStokesIPositive = false;

  virtual Kernel &getKernel() { return *kernel_; }

  virtual void launchKernel() override {
    BlockID block;
    kernel_->enqueue(block, *dev_input_, *dev_output_);
  }

  Settings &settings_;

  std::unique_ptr<KernelFactory<QuantizeOutputKernel>> factory_;
  std::unique_ptr<QuantizeOutputKernel> kernel_;

  std::array<size_t, 4> shape_input_;

  std::unique_ptr<cu::HostMemory> host_input_;
  std::unique_ptr<cu::HostMemory> host_output_;

  std::unique_ptr<cu::DeviceMemory> dev_input_;
  std::unique_ptr<cu::DeviceMemory> dev_output_;
};

int main(int argc, const char *argv[]) {
  // Parse command-line arguments
  cxxopts::ParseResult result = getCommandLineOptions(argc, argv);
  Settings settings(result);
  std::cout << settings;

  // Disable the default performance statistics
  PerformanceCounter::disablePrint();

  // Run benchmark
  Benchmark benchmark(settings);
  benchmark.runBenchmark();

  return EXIT_SUCCESS;
}