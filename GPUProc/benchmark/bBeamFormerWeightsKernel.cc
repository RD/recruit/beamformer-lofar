#include <complex>
#include <iostream>
#include <memory>

#include <cuda_fp16.h>
#include <cudawrappers/cu.hpp>
#include <cxxopts.hpp>

#include <CoInterface/BlockID.h>
#include <CoInterface/Span.h>
#include <GPUProc/Kernels/BeamFormerWeightsKernel.h>

#include "BenchmarkUtil.h"

namespace {
cxxopts::Options setupCommandLineParser(const char *argv[]) {
  cxxopts::Options options(argv[0], "Benchmark for BeamFormerKernel");

  const unsigned kNrStations = 64;
  const unsigned kNrChannels = 256;
  const unsigned kNrSamplesPerChannel = 768;
  const unsigned kNrTabs = 64;
  const unsigned kNrInputBits = 32;
  const bool kSinglePrecision = true;
  const unsigned kDeviceId = 0;

  options.add_options()(
      "nr_stations", "Number of stations",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrStations)))(
      "nr_channels", "Number of channels",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrChannels)))(
      "nr_samples", "Number of samples per channel",
      cxxopts::value<unsigned>()->default_value(
          std::to_string(kNrSamplesPerChannel)))(
      "nr_tabs", "Number of TABs",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrTabs)))(
      "nr_input_bits", "Number of input bits",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrInputBits)))(
      "single_precision", "Compute weights in single-precision",
      cxxopts::value<bool>()->default_value(std::to_string(kSinglePrecision)))(
      "device_id", "Device ID",
      cxxopts::value<unsigned>()->default_value(std::to_string(kDeviceId)))(
      "h,help", "Print help");

  return options;
}

cxxopts::ParseResult getCommandLineOptions(int argc, const char *argv[]) {
  cxxopts::Options options = setupCommandLineParser(argv);

  try {
    cxxopts::ParseResult result = options.parse(argc, argv);

    if (result.count("help")) {
      std::cout << options.help() << std::endl;
      exit(EXIT_SUCCESS);
    }

    return result;

  } catch (const cxxopts::exceptions::exception &e) {
    std::cerr << "Error parsing command-line options: " << e.what()
              << std::endl;
    exit(EXIT_FAILURE);
  }
}
} // namespace

using namespace LOFAR::Cobalt;

class Settings : public BenchmarkFixture::SettingsBase {
public:
  Settings(cxxopts::ParseResult &result) {
    nr_stations = result["nr_stations"].as<unsigned>();
    nr_channels = result["nr_channels"].as<unsigned>();
    nr_samples_per_channel = result["nr_samples"].as<unsigned>();
    nr_tabs = result["nr_tabs"].as<unsigned>();
    nr_input_bits = result["nr_input_bits"].as<unsigned>();
    single_precision = result["single_precision"].as<bool>();
    device_id = result["device_id"].as<unsigned>();
  }

  unsigned nr_stations;
  unsigned nr_channels;
  unsigned nr_samples_per_channel;
  unsigned nr_tabs;
  unsigned nr_input_bits;
  bool single_precision;
  const double subband_bandwidth = 200e3; // Hz
  const double subband_frequency = 1.5e8; // Hz

  friend std::ostream &operator<<(std::ostream &os, const Settings &settings) {
    os << ">>> Settings" << std::endl;
    os << "\tnr_stations:            " << settings.nr_stations << std::endl;
    os << "\tnr_channels:            " << settings.nr_channels << std::endl;
    os << "\tnr_samples_per_channel: " << settings.nr_samples_per_channel
       << std::endl;
    os << "\tnr_tabs:                " << settings.nr_tabs << std::endl;
    os << "\tnr_input_bits:          " << settings.nr_input_bits << std::endl;
    os << "\tsingle_precision:       " << settings.single_precision
       << std::endl;
    os << "\tdevice_id:              " << settings.device_id << std::endl;
    return os;
  }
};

template <typename OutputType> class Benchmark : public BenchmarkFixture {
public:
  Benchmark(Settings &settings)
      : settings_(settings), BenchmarkFixture(settings) {
    shape_delays_ = {settings.nr_stations, settings.nr_tabs};
    shape_weights_ = {settings.nr_tabs, settings.nr_channels,
                      settings.nr_stations};

    std::vector<unsigned> station_indices(settings.nr_stations);
    std::iota(station_indices.begin(), station_indices.end(), 0);
    std::vector<unsigned> delay_indices(settings.nr_stations);
    std::iota(delay_indices.begin(), delay_indices.end(), 0);

    const size_t nr_output_bits = sizeof(OutputType) * 8;

    BeamFormerWeightsKernel::Parameters parameters(
        station_indices, delay_indices,
        station_indices.size(),     // nrDelays
        settings.nr_channels,       // nrChannels
        settings.nr_tabs,           // nrTABs
        nr_output_bits,             // nrInputBits
        settings.subband_bandwidth, // subbandBandwidth
        settings.single_precision   // singlePrecision
    );

    host_delays_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerWeightsKernel::BEAM_FORMER_DELAYS)));
    host_weights_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerWeightsKernel::BEAM_FORMER_WEIGHTS)));

    dev_delays_.reset(new cu::DeviceMemory(host_delays_->size()));
    dev_weights_.reset(new cu::DeviceMemory(host_weights_->size()));

    factory_.reset(
        new KernelFactory<BeamFormerWeightsKernel>(device_, parameters));

    kernel_.reset(factory_->create(context_, execute_stream_));

    auto delays = CreateSpan(static_cast<std::complex<float> *>(*host_delays_),
                             shape_delays_);

    delays.fill(0.5);
  }

private:
  virtual Kernel &getKernel() { return *kernel_; }

  virtual void launchKernel() override {
    BlockID block;
    kernel_->setSubbandFrequency(settings_.subband_frequency);
    kernel_->enqueue(block, *dev_delays_, *dev_weights_);
  }

  Settings &settings_;

  std::unique_ptr<KernelFactory<BeamFormerWeightsKernel>> factory_;
  std::unique_ptr<BeamFormerWeightsKernel> kernel_;

  std::array<size_t, 2> shape_delays_;
  std::array<size_t, 3> shape_weights_;

  std::unique_ptr<cu::HostMemory> host_delays_;
  std::unique_ptr<cu::HostMemory> host_weights_;

  std::unique_ptr<cu::DeviceMemory> dev_delays_;
  std::unique_ptr<cu::DeviceMemory> dev_weights_;
};

int main(int argc, const char *argv[]) {
  // Parse command-line arguments
  cxxopts::ParseResult result = getCommandLineOptions(argc, argv);
  Settings settings(result);
  std::cout << settings;

  // Disable the default performance statistics
  PerformanceCounter::disablePrint();

  // Run benchmark
  if (settings.nr_input_bits == 16) {
    Benchmark<half> benchmark(settings);
    benchmark.runBenchmark();
  } else if (settings.nr_input_bits == 32) {
    Benchmark<float> benchmark(settings);
    benchmark.runBenchmark();
  }

  return EXIT_SUCCESS;
}