#include <complex>
#include <iostream>
#include <memory>

#include <cuda_fp16.h>
#include <cudawrappers/cu.hpp>
#include <cxxopts.hpp>

#include <CoInterface/BlockID.h>
#include <CoInterface/Span.h>
#include <GPUProc/Kernels/BeamFormerKernel.h>

#include "BenchmarkUtil.h"

namespace {
cxxopts::Options setupCommandLineParser(const char *argv[]) {
  cxxopts::Options options(argv[0], "Benchmark for BeamFormerKernel");

  const unsigned kNrStations = 64;
  const unsigned kNrChannels = 256;
  const unsigned kNrSamplesPerChannel = 768;
  const unsigned kNrTabs = 64;
  const unsigned kNrInputBits = 32;
  const bool kComputeWeights = false;
  const unsigned kDeviceId = 0;

  options.add_options()(
      "nr_stations", "Number of stations",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrStations)))(
      "nr_channels", "Number of channels",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrChannels)))(
      "nr_samples", "Number of samples per channel",
      cxxopts::value<unsigned>()->default_value(
          std::to_string(kNrSamplesPerChannel)))(
      "nr_tabs", "Number of TABs",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrTabs)))(
      "nr_input_bits", "Number of input bits",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrInputBits)))(
      "compute_weights", "Compute weights (if false, read them from memory)",
      cxxopts::value<bool>()->default_value(std::to_string(kComputeWeights)))(
      "device_id", "Device ID",
      cxxopts::value<unsigned>()->default_value(std::to_string(kDeviceId)))(
      "h,help", "Print help");

  return options;
}

cxxopts::ParseResult getCommandLineOptions(int argc, const char *argv[]) {
  cxxopts::Options options = setupCommandLineParser(argv);

  try {
    cxxopts::ParseResult result = options.parse(argc, argv);

    if (result.count("help")) {
      std::cout << options.help() << std::endl;
      exit(EXIT_SUCCESS);
    }

    return result;

  } catch (const cxxopts::exceptions::exception &e) {
    std::cerr << "Error parsing command-line options: " << e.what()
              << std::endl;
    exit(EXIT_FAILURE);
  }
}
} // namespace

using namespace LOFAR::Cobalt;

class Settings : public BenchmarkFixture::SettingsBase {
public:
  Settings(cxxopts::ParseResult &result) {
    nr_stations = result["nr_stations"].as<unsigned>();
    nr_channels = result["nr_channels"].as<unsigned>();
    nr_samples_per_channel = result["nr_samples"].as<unsigned>();
    nr_tabs = result["nr_tabs"].as<unsigned>();
    nr_input_bits = result["nr_input_bits"].as<unsigned>();
    compute_weights = result["compute_weights"].as<bool>();
    device_id = result["device_id"].as<unsigned>();
  }

  unsigned nr_stations;
  unsigned nr_channels;
  unsigned nr_samples_per_channel;
  unsigned nr_tabs;
  unsigned nr_input_bits;
  bool compute_weights;
  const double subband_bandwidth = 200e3; // Hz
  const double subband_frequency = 1.5e8; // Hz

  friend std::ostream &operator<<(std::ostream &os, const Settings &settings) {
    os << ">>> Settings" << std::endl;
    os << "\tnr_stations:            " << settings.nr_stations << std::endl;
    os << "\tnr_channels:            " << settings.nr_channels << std::endl;
    os << "\tnr_samples_per_channel: " << settings.nr_samples_per_channel
       << std::endl;
    os << "\tnr_tabs:                " << settings.nr_tabs << std::endl;
    os << "\tcompute_weights:        " << settings.compute_weights << std::endl;
    os << "\tnr_input_bits:          " << settings.nr_input_bits << std::endl;
    os << "\tdevice_id:              " << settings.device_id << std::endl;
    return os;
  }
};

template <typename InputType> class Benchmark : public BenchmarkFixture {
public:
  Benchmark(Settings &settings)
      : settings_(settings), BenchmarkFixture(settings) {
    shape_input_ = {settings.nr_stations, settings.nr_channels,
                    settings.nr_samples_per_channel, settings.nr_polarizations};
    shape_output_ = {settings.nr_channels, settings.nr_samples_per_channel,
                     settings.nr_tabs, settings.nr_polarizations};
    shape_delays_ = {settings.nr_stations, settings.nr_tabs};
    shape_weights_ = {settings.nr_tabs, settings.nr_channels,
                      settings.nr_stations};

    std::vector<unsigned> station_indices(settings.nr_stations);
    std::iota(station_indices.begin(), station_indices.end(), 0);
    std::vector<unsigned> delay_indices(settings.nr_stations);
    std::iota(delay_indices.begin(), delay_indices.end(), 0);

    const size_t nr_input_bits = sizeof(InputType) * 8;

    BeamFormerKernel::Parameters parameters(
        station_indices, delay_indices,
        station_indices.size(),          // nrDelays
        settings.nr_channels,            // nrChannels
        settings.nr_samples_per_channel, // nrSamplesPerChannal
        settings.nr_tabs,                // nrTABs
        nr_input_bits,                   // nrInputBits
        settings.subband_bandwidth,      // subbandBandwidth
        settings.compute_weights         // computeWeights
    );

    host_input_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerKernel::INPUT_DATA)));
    host_output_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerKernel::OUTPUT_DATA)));
    host_delays_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerKernel::BEAM_FORMER_DELAYS)));
    host_weights_.reset(new cu::HostMemory(
        parameters.bufferSize(BeamFormerKernel::BEAM_FORMER_WEIGHTS)));

    dev_input_.reset(new cu::DeviceMemory(host_input_->size()));
    dev_output_.reset(new cu::DeviceMemory(host_output_->size()));
    if (settings.compute_weights) {
      dev_delays_.reset(new cu::DeviceMemory(host_delays_->size()));
    } else {
      dev_weights_.reset(new cu::DeviceMemory(host_weights_->size()));
    }

    factory_.reset(new KernelFactory<BeamFormerKernel>(device_, parameters));

    kernel_.reset(factory_->create(context_, execute_stream_));

    auto input = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_input_), shape_input_);
    auto delays = CreateSpan(static_cast<std::complex<float> *>(*host_delays_),
                             shape_delays_);
    auto weights = CreateSpan(
        static_cast<std::complex<InputType> *>(*host_weights_), shape_weights_);

    input.fill(std::complex<InputType>(1, 1));
    delays.fill(0.5);
    weights.fill(0.1);

    if (settings.compute_weights) {
      htod_stream_.memcpyHtoDAsync(*dev_delays_, *host_delays_,
                                   host_delays_->size());
    } else {
      htod_stream_.memcpyHtoDAsync(*dev_weights_, *host_weights_,
                                   host_weights_->size());
    }
    htod_stream_.synchronize();
  }

private:
  virtual Kernel &getKernel() { return *kernel_; }

  virtual void launchKernel() override {
    BlockID block;
    if (settings_.compute_weights) {
      kernel_->setWeights(*dev_delays_);
    } else {
      kernel_->setWeights(*dev_weights_);
    }
    kernel_->setSubbandFrequency(settings_.subband_frequency);
    kernel_->enqueue(block, *dev_input_, *dev_output_);
  }

  Settings &settings_;

  std::unique_ptr<KernelFactory<BeamFormerKernel>> factory_;
  std::unique_ptr<BeamFormerKernel> kernel_;

  std::array<size_t, 4> shape_input_;
  std::array<size_t, 4> shape_output_;
  std::array<size_t, 2> shape_delays_;
  std::array<size_t, 3> shape_weights_;

  std::unique_ptr<cu::HostMemory> host_input_;
  std::unique_ptr<cu::HostMemory> host_output_;
  std::unique_ptr<cu::HostMemory> host_delays_;
  std::unique_ptr<cu::HostMemory> host_weights_;

  std::unique_ptr<cu::DeviceMemory> dev_input_;
  std::unique_ptr<cu::DeviceMemory> dev_output_;
  std::unique_ptr<cu::DeviceMemory> dev_delays_;
  std::unique_ptr<cu::DeviceMemory> dev_weights_;
};

int main(int argc, const char *argv[]) {
  // Parse command-line arguments
  cxxopts::ParseResult result = getCommandLineOptions(argc, argv);
  Settings settings(result);
  std::cout << settings;

  // Disable the default performance statistics
  PerformanceCounter::disablePrint();

  // Run benchmark
  if (settings.nr_input_bits == 16) {
    Benchmark<half> benchmark(settings);
    benchmark.runBenchmark();
  } else if (settings.nr_input_bits == 32) {
    Benchmark<float> benchmark(settings);
    benchmark.runBenchmark();
  }

  return EXIT_SUCCESS;
}