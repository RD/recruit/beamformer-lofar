#ifndef LOFAR_GPUPROC_BENCHMARKUTIL_H
#define LOFAR_GPUPROC_BENCHMARKUTIL_H

#if defined(HAVE_PMT)
#include <pmt.h>
#endif

#include <cudawrappers/cu.hpp>

namespace LOFAR::Cobalt {

// Class to initialize CUDA
class BenchmarkFixtureBase {
public:
  BenchmarkFixtureBase() { cu::init(); }
};

// Class to initialize common cu:: members
class BenchmarkFixture : public BenchmarkFixtureBase {
public:
  typedef struct {
    unsigned device_id;
    unsigned nr_polarizations = 2;
  } SettingsBase;

  BenchmarkFixture(SettingsBase settings);

  void runBenchmark();

protected:
  cu::Device device_;
  cu::Context context_;
  cu::Stream htod_stream_;
  cu::Stream dtoh_stream_;
  cu::Stream execute_stream_;

#if defined(HAVE_PMT)
  std::unique_ptr<pmt::PMT> powermeter_;
#endif

private:
  virtual Kernel &getKernel() = 0;
  virtual void launchKernel() = 0;
};

} // namespace LOFAR::Cobalt

#endif
