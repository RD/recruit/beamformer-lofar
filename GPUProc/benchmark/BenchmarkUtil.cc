#include <iostream>
#include <memory>
#include <thread>

#include <GPUProc/Kernels/Kernel.h>

#include "BenchmarkUtil.h"

using namespace LOFAR::Cobalt;

BenchmarkFixture::BenchmarkFixture(SettingsBase settings)
    : device_(settings.device_id), context_(0, device_), htod_stream_(),
      dtoh_stream_(), execute_stream_()
#if defined(HAVE_PMT)
      ,
      powermeter_(pmt::Create("nvidia", settings.device_id))
#endif
{
}

void BenchmarkFixture::runBenchmark() {
#if defined(HAVE_PMT)
  const int benchmark_duration = 2000; // ms
#else
  const int benchmark_duration = 50; // ms
#endif

  std::thread thread([&] {
    context_.setCurrent();

    auto start = std::chrono::high_resolution_clock::now();

    int i = 0;
    int milliseconds = 0;
    for (; milliseconds < benchmark_duration; i++) {
      launchKernel();
      execute_stream_.synchronize();
      auto end = std::chrono::high_resolution_clock::now();
      auto duration =
          std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
      milliseconds = duration.count();
    };
    std::cout << "\texecuted " << i << " iterations in " << milliseconds
              << " milliseconds" << std::endl;
  });

  Kernel &kernel = getKernel();
  double runtime = 0;

#if defined(HAVE_PMT)
  std::this_thread::sleep_for(
      std::chrono::milliseconds(benchmark_duration / 4));
  const pmt::State start_state = powermeter_->Read();
  std::this_thread::sleep_for(
      std::chrono::milliseconds(benchmark_duration / 2));
  const pmt::State end_state = powermeter_->Read();
#endif
  thread.join();
#if defined(HAVE_PMT)
  const double watts = pmt::PMT::watts(start_state, end_state);
  std::cout << "\tmean gpu power:      " << watts << " W" << std::endl;
  runtime = kernel.getStats().mean();
  const double joules = watts * (runtime / 1e3);
  std::cout << "\tkernel energy use:   " << joules << " J" << std::endl;
#endif

  runtime = getKernel().getStats().mean();
  std::cout << "\tmean kernel runtime: " << runtime << " milliseconds"
            << std::endl;
  std::cout << "\tperformance:         "
            << (kernel.getNrOperations() / runtime) * 1e-6 << " GOps/s"
            << std::endl;
  std::cout << "\tread bandwidth:      "
            << (kernel.getNrBytesRead() / runtime) * 1e-6 << " GB/s"
            << std::endl;
  std::cout << "\twrite bandwidth:     "
            << (kernel.getNrBytesWritten() / runtime) * 1e-6 << " GB/s"
            << std::endl;
  std::cout << std::endl;
}