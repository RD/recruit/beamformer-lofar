#include <complex>
#include <iostream>
#include <memory>

#include <cuda_fp16.h>
#include <cudawrappers/cu.hpp>
#include <cxxopts.hpp>

#include <CoInterface/BlockID.h>
#include <CoInterface/Span.h>
#include <GPUProc/Kernels/FIR_FilterKernel.h>

#include "BenchmarkUtil.h"

namespace {
cxxopts::Options setupCommandLineParser(const char *argv[]) {
  cxxopts::Options options(argv[0], "Benchmark for FIR_FilterKernel");

  const unsigned kNrStations = 64;
  const unsigned kNrChannels = 256;
  const unsigned kNrSamplesPerChannel = 768;
  const unsigned kNrTabs = 64;
  const unsigned kNrInputBits = 32;
  const bool kEnableFFT = false;
  const unsigned kDeviceId = 0;

  options.add_options()(
      "nr_stations", "Number of stations",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrStations)))(
      "nr_channels", "Number of channels",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrChannels)))(
      "nr_samples", "Number of samples per channel",
      cxxopts::value<unsigned>()->default_value(
          std::to_string(kNrSamplesPerChannel)))(
      "nr_tabs", "Number of TABs",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrTabs)))(
      "nr_input_bits", "Number of input bits",
      cxxopts::value<unsigned>()->default_value(std::to_string(kNrInputBits)))(
      "enable_fft", "Enable FFT after FIR",
      cxxopts::value<bool>()->default_value(std::to_string(kEnableFFT)))(
      "device_id", "Device ID",
      cxxopts::value<unsigned>()->default_value(std::to_string(kDeviceId)))(
      "h,help", "Print help");

  return options;
}

cxxopts::ParseResult getCommandLineOptions(int argc, const char *argv[]) {
  cxxopts::Options options = setupCommandLineParser(argv);

  try {
    cxxopts::ParseResult result = options.parse(argc, argv);

    if (result.count("help")) {
      std::cout << options.help() << std::endl;
      exit(EXIT_SUCCESS);
    }

    return result;

  } catch (const cxxopts::exceptions::exception &e) {
    std::cerr << "Error parsing command-line options: " << e.what()
              << std::endl;
    exit(EXIT_FAILURE);
  }
}
} // namespace

using namespace LOFAR::Cobalt;

class Settings : public BenchmarkFixture::SettingsBase {
public:
  Settings(cxxopts::ParseResult &result) {
    nr_channels = result["nr_channels"].as<unsigned>();
    nr_samples_per_channel = result["nr_samples"].as<unsigned>();
    nr_tabs = result["nr_tabs"].as<unsigned>();
    nr_input_bits = result["nr_input_bits"].as<unsigned>();
    enable_fft = result["enable_fft"].as<bool>();
    device_id = result["device_id"].as<unsigned>();
  }

  unsigned nr_channels;
  unsigned nr_samples_per_channel;
  unsigned nr_tabs;
  unsigned nr_input_bits;
  bool enable_fft;

  friend std::ostream &operator<<(std::ostream &os, const Settings &settings) {
    os << ">>> Settings" << std::endl;
    os << "\tnr_channels:            " << settings.nr_channels << std::endl;
    os << "\tnr_samples_per_channel: " << settings.nr_samples_per_channel
       << std::endl;
    os << "\tnr_tabs:                " << settings.nr_tabs << std::endl;
    os << "\tnr_input_bits:          " << settings.nr_input_bits << std::endl;
    os << "\tfft:                    " << settings.enable_fft << std::endl;
    os << "\tdevice_id:              " << settings.device_id << std::endl;
    return os;
  }
};

template <typename InputType> class Benchmark : public BenchmarkFixture {
public:
  Benchmark(Settings &settings)
      : settings_(settings), BenchmarkFixture(settings) {
    shape_input_ = {settings.nr_tabs, kNrPolarizations,
                    settings.nr_samples_per_channel, settings.nr_channels};
    shape_output_ = shape_input_;

    FIR_FilterKernel::Parameters parameters(
        settings.nr_tabs,                // nrTABs
        settings.nr_input_bits,          // nrBitsPerSample
        settings.nr_channels,            // nrChannels
        settings.nr_samples_per_channel, // nrSamplesPerChannel
        kScaleFactor,                    // scaleFactor
        settings.enable_fft              // enableFFT
    );

    host_input_.reset(new cu::HostMemory(
        parameters.bufferSize(FIR_FilterKernel::INPUT_DATA)));
    host_output_.reset(new cu::HostMemory(
        parameters.bufferSize(FIR_FilterKernel::OUTPUT_DATA)));
    device_input_.reset(new cu::DeviceMemory(
        parameters.bufferSize(FIR_FilterKernel::INPUT_DATA)));
    device_output_.reset(new cu::DeviceMemory(
        parameters.bufferSize(FIR_FilterKernel::OUTPUT_DATA)));

    factory_.reset(new KernelFactory<FIR_FilterKernel>(device_, parameters));

    kernel_.reset(factory_->create(context_, execute_stream_));
  }

private:
  virtual Kernel &getKernel() { return *kernel_; }

  virtual void launchKernel() override {
    BlockID block;

    kernel_->enqueue(block, *device_input_, *device_output_);
  }

  Settings &settings_;

  std::unique_ptr<KernelFactory<FIR_FilterKernel>> factory_;
  std::unique_ptr<FIR_FilterKernel> kernel_;

  const unsigned int kNrPolarizations = 2;
  const unsigned int kNrTaps = 16;
  const float kScaleFactor = 1.0f;

  std::array<size_t, 4> shape_input_;
  std::array<size_t, 4> shape_output_;

  std::unique_ptr<cu::HostMemory> host_input_;
  std::unique_ptr<cu::HostMemory> host_output_;

  std::unique_ptr<cu::DeviceMemory> device_input_;
  std::unique_ptr<cu::DeviceMemory> device_output_;
};

int main(int argc, const char *argv[]) {
  // Parse command-line arguments
  cxxopts::ParseResult result = getCommandLineOptions(argc, argv);
  Settings settings(result);
  std::cout << settings;

  // Disable the default performance statistics
  PerformanceCounter::disablePrint();

  // Run benchmark
  if (settings.nr_input_bits == 16) {
    Benchmark<half> benchmark(settings);
    benchmark.runBenchmark();
  } else if (settings.nr_input_bits == 32) {
    Benchmark<float> benchmark(settings);
    benchmark.runBenchmark();
  }

  return EXIT_SUCCESS;
}